<?php

/**
 * @author Danijel
 * @copyright 2016
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require($_SERVER['DOCUMENT_ROOT'] . '/admin/system/settings/config.php');

// before
/*if (isset($_SESSION['admin_user']))
{
    header('Location: /admin/interface/');
    die();
}*/

$XeniumAPI = new API('set_session');
$session_result = json_decode($XeniumAPI->get(), true);
if ($session_result['status'] == 'loggedIn' && $session_result['user_info']['user_id'])
{
    header('Location: /admin/interface/');
    die();
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/admin/theme/images/favicon.ico">

    <title>Xenium CMS</title>

    <!-- Tether CSS -->
    <link href="/admin/theme/css/tether.min.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="/admin/theme/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/admin/theme/css/signin.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="/admin/theme/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>



    <div class="navigation">
        <a href="http://xenium.org">
            <img alt="Xenium CMS" class="xenium-logo" src="/admin/theme/images/xenium-logo-dark.svg" />
        </a>
        <ul>
            <li>
                <a href="http://xenium.org">Home</a>
            </li>
            <li>
                <a href="http://xenium.network">Network</a>
            </li>
            <li>
                <a href="http://xenium.blog">Blog</a>
            </li>
            <li>
                <a href="http://xenium.docs">Documentation</a>
            </li>
            <li>
                <a href="http://xenium.io">About</a>
            </li>
        </ul>
    </div>

    <div class="container">
        <h2 class="title-signin">Sign in to continue</h2>
      <form class="form-signin" role="form" method="post">
        <div class="row">
            <div class="col-md-5">
                <a href="#" class="social-login" id="facebook-login"><i class="fa fa-facebook"></i> <span>Facebook login</span></a>
                <a href="#" class="social-login" id="google-login"><i class="fa fa-google"></i> <span>Google login</span></a>
                <a href="#" class="social-login" id="twitter-login"><i class="fa fa-twitter"></i> <span>Twitter login</span></a>
                <a href="#" class="social-login" id="linkedin-login"><i class="fa fa-linkedin"></i> <span>Linkedin login</span></a>
            </div>
            <div class="col-md-2 separator-holder">
                <div class="separator">
                </div>
                <div class="or">
                    OR
                </div>
            </div>
            <div class="col-md-5">
                <h3 class="form-signin-heading">Sign in with your<br /><strong>Xenium Network ID</strong></h3>
                <div class="login-form-container">
                    <input type="email" name="" id="email" class="form-control" placeholder="Email" required autofocus>
                    <input type="password" name="" id="password" class="form-control" placeholder="Password" required>
                    <button class="btn btn-lg btn-primary btn-login" type="submit"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                </div>
                <p>
                    <a href="#">Forgotten password?</a>
                </p>
                <p>
                    Don't have a <strong>Xenium Net ID</strong>?<br /><a href="#" data-toggle="modal" data-target="#RegistrationModal" onclick="updateRegistrationFields(); return false;">Register now</a>
                </p>
            </div>
        </div>
      </form>

    </div> <!-- /container -->

    <div id="particles-js"></div>

    <!-- Registration Modal -->
    <div class="modal fade" id="RegistrationModal" tabindex="-1" role="dialog" aria-labelledby="registrationModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="registrationModalLabel">New user registration</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="form-registration" role="form" method="post">
          <div class="modal-body">
              <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="registration-email">Enter your Email:</label>
                          <input type="email" name="" id="registration-email" class="form-control" placeholder="Email" required autofocus>
                      </div>
                      <div class="form-group">
                          <label for="registration-password">Enter your Password:</label>
                          <input type="password" name="" id="registration-password" class="form-control" placeholder="Password" required>
                      </div>
                      <div class="form-group">
                          <label for="registration-email">Re-Enter your Password:</label>
                          <input type="password" name="" id="registration-password-repeat" class="form-control" placeholder="Password Repeat" required>
                      </div>
                  </div>
                  <div class="col-md-6 registration-info">
                      <div>
                          <h6>By registering to the Xenium Network you get:</h6>
                          <ul>
                              <li>
                                  Free and easy project management
                              </li>
                              <li>
                                  Special offers from our partners
                              </li>
                              <li>
                                  Free updates and extra features
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="col-md-12 registration-checkboxes">
                      <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                        <input type="checkbox" class="custom-control-input" name="agree" value="1">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">I agree to the <a href="#">Terms and Conditions</a> of Xenium Network</span>
                      </label>
                  </div>
                  <div class="col-md-12 registration-checkboxes">
                      <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                        <input type="checkbox" class="custom-control-input" name="mailing" value="1" checked="true">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">I'd like to be notified via email about Xenium Network news, updates and special offers from our partners</span>
                      </label>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Register</button>
          </div>
          </form>
        </div>
      </div>
    </div>

    <div id="login-result"></div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="/admin/theme/js/tether.min.js"></script>
    <script src="/admin/theme/js/bootstrap.min.js"></script>
    <script src="/admin/theme/js/bootbox.min.js"></script>

    <script src="/admin/theme/js/jAPI.js"></script>
    <script src="/admin/theme/js/particles.min.js"></script>
    <script src="/admin/system/admin.login.actions/admin.login.actions.php"></script>

    <script type="text/javascript">

    function loginRequest(login_data)
    {
        return AdminLoginAction.loginRequest(login_data);
    }

    $( document ).ready(function()
    {
        loginRequest().done(function(result){
            $("#email").attr('name', result.email_field_name);
            $("#password").attr('name', result.pass_field_name);
            // set social login
            $("#facebook-login").attr('social-token', result.social_login_fields.facebook);
            $("#google-login").attr('social-token', result.social_login_fields.google);
            $("#twitter-login").attr('social-token', result.social_login_fields.twitter);
            $("#linkedin-login").attr('social-token', result.social_login_fields.linkedin);
        });
    });

    function login(login_data)
    {
        return AdminLoginAction.login(login_data);
    }

    $( ".form-signin" ).on( "submit", function( event ) {
        event.preventDefault();
        var login_data = $( this ).serialize();

        login(encodeURIComponent(login_data)).done(function(result){
            if (result.answer == "error"){
                bootbox.alert(result.message);
                $("#email").attr('name', result.field_names.email_field_name);
                $("#password").attr('name', result.field_names.pass_field_name);
                $("#password").val('');
                // set social login
                $("#facebook-login").attr('social-token', result.social_login_fields.facebook);
                $("#google-login").attr('social-token', result.social_login_fields.google);
                $("#twitter-login").attr('social-token', result.social_login_fields.twitter);
                $("#linkedin-login").attr('social-token', result.social_login_fields.linkedin);
            } else {
                window.location.reload();
            }
        });
    });

    function registrationRequest(){
        return AdminLoginAction.registrationRequest();
    }

    function updateRegistrationFields(){
        registrationRequest().done(function(result){
            $("#registration-email").attr('name', result.email_field_name);
            $("#registration-password").attr('name', result.pass_field_name);
            $("#registration-password-repeat").attr('name', result.pass_r_field_name);
            $("#registration-password").val('');
            $("#registration-password-repeat").val('');
        });
    }

    function registration(registration_data)
    {
        return AdminLoginAction.registration(registration_data);
    }

    $( ".form-registration" ).on( "submit", function( event ) {
        event.preventDefault();
        var registration_data = $( this ).serialize();

        registration(encodeURIComponent(registration_data)).done(function(result){
            if (result.answer == "error"){
                bootbox.alert(result.message);
                // login form
                $("#email").attr('name', result.field_names.email_field_name);
                $("#password").attr('name', result.field_names.pass_field_name);
                $("#password").val('');
                // registration form
                $("#registration-email").attr('name', result.registration_field_names.email_field_name);
                $("#registration-password").attr('name', result.registration_field_names.pass_field_name);
                $("#registration-password-repeat").attr('name', result.registration_field_names.pass_r_field_name);
                $("#registration-password").val('');
                $("#registration-password-repeat").val('');
                // set social login
                $("#facebook-login").attr('social-token', result.social_login_fields.facebook);
                $("#google-login").attr('social-token', result.social_login_fields.google);
                $("#twitter-login").attr('social-token', result.social_login_fields.twitter);
                $("#linkedin-login").attr('social-token', result.social_login_fields.linkedin);
            } else {
                //window.location.reload();
            }
        });
    });

    function socialLoginURL(social_token)
    {
        return AdminLoginAction.getSocialURL(social_token);
    }

    $( ".social-login" ).on( "click", function( event ) {
        var social_token = $(this).attr('social-token');

        var w = 600;
        var h = 500;
        var title = 'SocialLogin';

        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;

        var newWindow = window.open('https://api.xenium.network/', title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        socialLoginURL(social_token).done(function(result){
            //console.log(result);
            //popupCenter(result.url, 'SocialLogin', 500, 600);
            newWindow.location = result.url;
            if (window.focus) {
                newWindow.focus();
            }
        });
        return false;
    });

    particlesJS('particles-js',
      {
        "particles": {
          "number": {
            "value": 80,
            "density": {
              "enable": true,
              "value_area": 800
            }
          },
          "color": {
            "value": "#ffffff"
          },
          "shape": {
            "type": "circle",
            "stroke": {
              "width": 0,
              "color": "#000000"
            },
            "polygon": {
              "nb_sides": 5
            },
            "image": {
              "width": 100,
              "height": 100
            }
          },
          "opacity": {
            "value": 0.3,
            "random": false,
            "anim": {
              "enable": false,
              "speed": 1,
              "opacity_min": 0.01,
              "sync": false
            }
          },
          "size": {
            "value": 5,
            "random": true,
            "anim": {
              "enable": false,
              "speed": 40,
              "size_min": 0.1,
              "sync": false
            }
          },
          "line_linked": {
            "enable": true,
            "distance": 150,
            "color": "#ffffff",
            "opacity": 0.4,
            "width": 1
          },
          "move": {
            "enable": true,
            "speed": 0.5,
            "direction": "none",
            "random": false,
            "straight": false,
            "out_mode": "out",
            "attract": {
              "enable": false,
              "rotateX": 600,
              "rotateY": 1200
            }
          }
        },
        "interactivity": {
          "detect_on": "canvas",
          "events": {
            "onhover": {
              "enable": true,
              "mode": "repulse"
            },
            "onclick": {
              "enable": true,
              "mode": "push"
            },
            "resize": true
          },
          "modes": {
            "grab": {
              "distance": 400,
              "line_linked": {
                "opacity": 1
              }
            },
            "bubble": {
              "distance": 400,
              "size": 40,
              "duration": 2,
              "opacity": 8,
              "speed": 3
            },
            "repulse": {
              "distance": 200
            },
            "push": {
              "particles_nb": 4
            },
            "remove": {
              "particles_nb": 2
            }
          }
        },
        "retina_detect": true,
        "config_demo": {
          "hide_card": false,
          "background_color": "#b61924",
          "background_image": "",
          "background_position": "50% 50%",
          "background_repeat": "no-repeat",
          "background_size": "cover"
        }
      }
    );
    </script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/admin/theme/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
