<?php

/**
 * @author Danijel
 * @copyright 2014
 */

require($_SERVER['DOCUMENT_ROOT'] . '/admin/system/settings/config.php');

// before
/*if (!isset($_SESSION['admin_user']))
{
    header('Location: /admin/login/');
    die();
}*/

if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) && !isset($_SERVER['Uploading']) )
{
	die();
}

$XeniumAPI = new API('set_session');
$session_result = json_decode($XeniumAPI->get(), true);
if (!isset($session_result['status']) || $session_result['status'] == 'loggedOut' || !$session_result['user_info']['user_id'])
{
    header('Location: /admin/login/');
    die();
}

$user_info = $session_result['user_info'];
$my_projects = $user_info['projects'];

//$my_projects = $XeniumAPI->endpoint('get_projects')->get();
//echo $my_projects;

$project_id = Project::getAdminCurrent();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/admin/theme/images/favicon.ico">

    <title>Xenium CMS</title>

    <!-- Tether CSS -->
    <link href="/admin/theme/css/tether.min.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="/admin/theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="/admin/theme/css/bootstrap.vertical-tabs.min.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="/admin/theme/css/font-awesome.min.css" rel="stylesheet">

    <!-- Linea FonIcon CSS -->
    <link href="/admin/theme/fonts/linea/styles.css" rel="stylesheet">

    <!-- Tonicons CSS -->
    <link href="/admin/theme/fonts/tonicons/style.css" rel="stylesheet">

    <!-- Keen IO dashboard layout -->
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/keen-dashboards.css" />

    <!-- jQuery UI styles -->
    <link href="/admin/theme/css/jquery-ui.min.css" rel="stylesheet">

    <!-- Gridstack styles -->
    <link href="/admin/theme/css/gridstack.css" rel="stylesheet">
    <!--<link href="/admin/theme/css/gridstack-extra.css" rel="stylesheet">-->

    <!-- Select 2 -->
    <link href="/admin/theme/js/select2/select2.css" rel="stylesheet">
    <!--<link href="/admin/theme/js/select2/select2-bootstrap.css" rel="stylesheet">-->

    <!-- Font Icon Picker -->
    <!--<link rel="stylesheet" type="text/css" href="/admin/theme/css/jquery.fonticonpicker.min.css" />
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/jquery.fonticonpicker.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/admin/theme/fonts/fontello/css/fontello.css" />-->
    <link rel="stylesheet" type="text/css" href="/admin/theme/fonts/icomoon/style.css" />

    <!-- Bootstrap IconPicker -->
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/bootstrap-iconpicker.min.css" />

    <link rel="stylesheet" href="/admin/theme/icon-fonts/elusive-icons-2.0.0/css/elusive-icons.min.css"/>
    <link rel="stylesheet" href="/admin/theme/icon-fonts/font-awesome-4.2.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/admin/theme/icon-fonts/ionicons-1.5.2/css/ionicons.min.css"/>
    <link rel="stylesheet" href="/admin/theme/icon-fonts/map-icons-2.1.0/css/map-icons.min.css"/>
    <link rel="stylesheet" href="/admin/theme/icon-fonts/octicons-2.1.2/css/octicons.min.css"/>
    <link rel="stylesheet" href="/admin/theme/icon-fonts/typicons-2.0.6/css/typicons.min.css"/>
    <link rel="stylesheet" href="/admin/theme/icon-fonts/weather-icons-1.2.0/css/weather-icons.min.css"/>

    <!-- Color picker spectrum -->
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/spectrum.css" />

    <!-- Bootstrap slider -->
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/bootstrap-slider.css" />

    <!-- Perfect Scrollbar -->
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/perfect-scrollbar.min.css" />

    <!-- jsTree -->
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/jsTree.css" />

	<!-- jQuery Query Builder -->
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/query-builder.default.min.css" />

    <!-- Grid Editor -->
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/grideditor.css" />
	<link rel="stylesheet" type="text/css" href="/admin/theme/css/grideditor-font-awesome.css" />

	<!-- Noty -->
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/noty.css" />

    <!-- Bootstrap Modal -->
    <!--<link rel="stylesheet" type="text/css" href="/admin/theme/css/bootstrap-modal-bs3patch.css" />
    <link rel="stylesheet" type="text/css" href="/admin/theme/css/bootstrap-modal.css" />-->

    <!-- Custom styles for this template -->
    <link href="/admin/theme/css/custom.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <!--<body id="html_content">-->
  <body>

    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse" role="navigation">
        <div class="logo"><img src="/admin/theme/images/X - white.svg" /></div>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="main-menu" class="collapse navbar-collapse">
            <div class="navbar-nav">
                <a class="nav-item nav-link follow active" href="/admin/interface/"><i class="fa fa-home"></i> Dashboard <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link follow" href="/admin/interface/interface-users/"><i class="fa fa-users"></i> Users</a>
                <a class="nav-item nav-link follow" href="/admin/interface/interface-multimedia/"><i class="fa fa-image"></i> Multimedia</a>
                <a class="nav-item nav-link follow" href="/admin/interface/interface-settings/"><i class="fa fa-cog"></i> Settings</a>
            </div>
        </div>

      <!--<div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
      </div>-->
        <div class="navbar-collapse collapse">
          <!--<ul id="main-menu" class="nav navbar-nav navbar-left">
            <li class="active"><a id="home-button" href="/admin/interface/" class="follow"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a href="/admin/interface/users/" class="follow"><i class="fa fa-users"></i> Users</a></li>
            <li><a href="/admin/interface/multimedia/" class="follow"><i class="fa fa-image"></i> Multimedia</a></li>
            <li><a href="/admin/interface/settings/" class="follow"><i class="fa fa-cog"></i> Settings</a></li>
        </ul>-->
          <!--
          <form class="navbar-form navbar-left">
            <input type="text" class="form-control" placeholder="Search...">
          </form>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">Contents</a></li>
            </ul>-->

            <?php
                $projects = Project::getAll();

                if (!$project_id) $project_menu_id = 1; else $project_menu_id = $project_id;

                $my_projects_array = array();
                foreach ($my_projects['owned'] as $own_project)
                {
                    $my_projects_array[$own_project['id']] = $own_project;
                }
                foreach ($my_projects['access'] as $access_project)
                {
                    $my_projects_array[$access_project['project_id']] = $access_project;
                }

                /*foreach ($projects as $project)
                {
                    if (isset($my_projects_array[$project['id']]))
                    {
                        unset($my_projects_array[$project['id']]);
                    }

                    if ($project['id'] == $project_menu_id)
                    {
                        $project_info = $project;
                        break;
                    }
                }*/
				foreach ($projects as $project)
                {
                    if (isset($my_projects_array[$project['ref_id']]))
                    {
                        unset($my_projects_array[$project['ref_id']]);
                    }

                    if ($project['id'] == $project_menu_id)
                    {
                        $project_info = $project;
                        //break;
                    }
                }
            ?>
            <ul id="project-menu" class="nav navbar-nav navbar-right">
                <!--<li><span class="logged">Selected project:</span></li>-->
                <li>
                    <div class="btn-group">
                        <!--<button id="project-menu-more" type="button" class="btn btn-sm btn-primary"><span id="project-logo">X</span>Xenium</button>-->
                        <button id="project-menu-more" type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span id="project-logo"><?php echo strtoupper($project_info['project_name'][0]); ?></span><span class="selected-project-name"><?php echo $project_info['project_name']; ?></span>
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu" role="menu">
                            <?php foreach ($projects as $project){ ?>
                            <a href="/admin/interface/project-<?php echo $project['id']; ?>/interface-contents/" data-id="<?php echo $project['id']; ?>" data-initial="<?php echo strtoupper($project['project_name'][0]); ?>" class="dropdown-item project-select follow"><?php echo $project['project_name']; ?></a>
                            <?php } ?>

                            <?php if (count($my_projects_array)){ ?>
                            <div class="dropdown-divider"></div>
                            <h6 class="dropdown-header">Not installed</h6>
							<div class="not-installed">
	                            <?php foreach ($my_projects_array as $project){ ?>
	                            <a href="#" data-id="<?php echo $project['id']; ?>" data-name="<?php echo $project['name']; ?>" data-slug="<?php echo $project['slug']; ?>" data-repo="<?php echo $project['git_repository']; ?>" data-initial="<?php echo strtoupper($project['name'][0]); ?>" class="dropdown-item no-follow"><?php echo $project['name']; ?></a>
	                            <?php } ?>
							</div>
							<?php } ?>

                            <div class="dropdown-divider"></div>
                            <!--<a href="#" class="dropdown-item">Install a project</a>-->
                            <a href="#" class="dropdown-item">Create a new project</a>
                        </div>
                        <?php foreach ($projects as $project)
                        {
                            if ($project['id'] == $project_menu_id) $invisible_class = ''; else $invisible_class = 'invisible';
                        ?>

                        <a href="/admin/interface/project-<?php echo $project['id']; ?>/interface-contents/" type="button" class="btn btn-sm btn-primary menu-select project-<?php echo $project['id']; ?> follow <?php echo $invisible_class; ?>"><i class="fa fa-pencil"></i> Contents</a>
                        <!--<a href="/admin/interface/project-<?php echo $project['id']; ?>/interface-timeline/" type="button" class="btn btn-sm btn-primary menu-select project-<?php echo $project['id']; ?> follow <?php echo $invisible_class; ?>"><i class="fa fa-calendar"></i> Timeline</a>-->
                        <a href="/admin/interface/project-<?php echo $project['id']; ?>/interface-mailing/" type="button" class="btn btn-sm btn-primary menu-select project-<?php echo $project['id']; ?> follow <?php echo $invisible_class; ?>"><i class="fa fa-envelope"></i> Mailing</a>
                        <!--<a href="/admin/interface/project-<?php echo $project['id']; ?>/interface-history/" type="button" class="btn btn-sm btn-primary menu-select project-<?php echo $project['id']; ?> follow <?php echo $invisible_class; ?>"><i class="fa fa-history"></i> History</a>-->
                        <!--<a href="/admin/interface/project-<?php echo $project['id']; ?>/interface-development/" type="button" class="btn btn-sm btn-primary menu-select project-<?php echo $project['id']; ?> follow <?php echo $invisible_class; ?>"><i class="fa fa-code"></i> Development</a>-->
                        <a href="/admin/interface/project-<?php echo $project['id']; ?>/interface-settings/" type="button" class="btn btn-sm btn-primary menu-select project-<?php echo $project['id']; ?> follow last <?php echo $invisible_class; ?>"><i class="fa fa-cogs"></i> Settings</a>

                        <?php } ?>
                    </div>
                </li>
            </ul>

            <ul id="user-info" class="nav navbar-nav navbar-right">
                <!--<li><span class="logged">Logged in as:</span></li>-->
                <li>
                    <div class="btn-group">
                      <button id="user-info-more" type="button" class="btn btn-sm btn-primary">
                          <span id="user-pic"><?php if (isset($user_info['photo_url']) && $user_info['photo_url']) echo '<img src="' . $user_info['photo_url'] . '" />'; else echo '<i class="fa fa-user fa-3x"></i>'; ?></span>
                          <?php echo $user_info['username']; ?>
                      </button>
                      <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a href="#" class="dropdown-item">Action</a>
                        <a href="#" class="dropdown-item">Another action</a>
                        <a href="#" class="dropdown-item">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">Separated link</a>
                    </div>
                    </div>
                </li>
            </ul>


        </div>
    </nav>
    </div>

    <div class="container-fluid height-100">
      <div id="content" class="row height-100">
        <div class="sidebar left-navigation">


        </div>

        <div id="main-content-view" class="height-100">

        </div>


    </div>

    <div class="custom-container-content"></div>
    <div id="text-editor-modals"></div>
	<div id="update-modals">
		<div class="modal fade" id="project-install-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="modalLabel">Installing project <strong class="project-name"></strong></h5>
		      </div>
		      <div class="modal-body">
				  <div class="cssload-loader" style="float:left; left:0; margin-right:18px;">
				  	<div class="cssload-inner cssload-one"></div>
				  	<div class="cssload-inner cssload-two"></div>
				  	<div class="cssload-inner cssload-three"></div>
				  </div>
		        <p class="project-install-text">
					The project <strong class="project-name"></strong> is installing, please wait...
				</p>
		      </div>
		    </div>
		  </div>
		</div>

		<div class="modal fade" id="project-commit-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="modalLabel">Commiting changes for <strong class="project-name"></strong></h5>
		      </div>
		      <div class="modal-body">
				  <div class="cssload-loader" style="float:left; left:0; margin-right:18px;">
				  	<div class="cssload-inner cssload-one"></div>
				  	<div class="cssload-inner cssload-two"></div>
				  	<div class="cssload-inner cssload-three"></div>
				  </div>
		        <p class="project-install-text">
					Changes for project <strong class="project-name"></strong> are commiting, please wait...
				</p>
		      </div>
		    </div>
		  </div>
		</div>

		<div class="modal fade" id="project-checkout-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="modalLabel">Changing to <strong class="project-name"></strong></h5>
		      </div>
		      <div class="modal-body">
				  <div class="cssload-loader" style="float:left; left:0; margin-right:18px;">
				  	<div class="cssload-inner cssload-one"></div>
				  	<div class="cssload-inner cssload-two"></div>
				  	<div class="cssload-inner cssload-three"></div>
				  </div>
		        <p class="project-install-text">
					Changes for project <strong class="project-name"></strong> are commiting, please wait...
				</p>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
    <div id="action-board"></div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
    <script src="/admin/theme/js/jquery.min.js"></script>
    <!--<script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>-->
    <script src="/admin/theme/js/tether.min.js"></script>
    <!--<script src="/admin/theme/js/jquery-migrate-1.3.0.min.js"></script>-->
    <script src="/admin/theme/js/jquery-ui.min.js"></script>
    <script src="/admin/theme/js/bootstrap.min.js"></script>
    <!-- loads zeroclipboard.swf which is not needed -->
    <!--<script src="/admin/theme/js/docs.min.js"></script>-->

    <!--<script src="/admin/theme/js/jquery.nicescroll.min.js"></script>-->
    <script src="/admin/theme/js/perfect-scrollbar.min.js"></script>
    <script src="/admin/theme/js/bootbox.min.js"></script>
    <script src="/admin/theme/js/jquery.address.min.js"></script>
    <!--<script src="http://www.asual.com/jquery/address/samples/state/jquery.address-1.5.min.js"></script>-->

    <!-- Admin actions -->
    <script src="/admin/theme/js/jAPI.js"></script>
    <script src="/admin/system/admin.actions/admin.actions.php"></script>

    <script type="text/javascript" src="/admin/theme/js/diff_match_patch.js"></script>
    <script type="text/javascript" src="/admin/theme/js/plupload/plupload.full.min.js"></script>
    <script type="text/javascript" src="/admin/theme/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="/admin/theme/js/select2/select2.min.js"></script>
    <script type="text/javascript" src="/admin/theme/js/bootstrap3-typeahead.min.js"></script>
    <!--<script type="text/javascript" src="/admin/theme/js/jquery.fonticonpicker.min.js"></script>-->
    <script type="text/javascript" src="/admin/theme/js/spectrum.js"></script>
    <script type="text/javascript" src="/admin/theme/js/bootstrap-slider.js"></script>

    <!-- Bootstrap IconPicker IconSets -->
    <script type="text/javascript" src="/admin/theme/js/iconset/iconset-elusiveicon-2.0.0.min.js"></script>
    <script type="text/javascript" src="/admin/theme/js/iconset/iconset-fontawesome-4.2.0.min.js"></script>
    <script type="text/javascript" src="/admin/theme/js/iconset/iconset-glyphicon.min.js"></script>
    <script type="text/javascript" src="/admin/theme/js/iconset/iconset-ionicon-1.5.2.min.js"></script>
    <script type="text/javascript" src="/admin/theme/js/iconset/iconset-mapicon-2.1.0.min.js"></script>
    <script type="text/javascript" src="/admin/theme/js/iconset/iconset-octicon-2.1.2.min.js"></script>
    <script type="text/javascript" src="/admin/theme/js/iconset/iconset-typicon-2.0.6.min.js"></script>
    <script type="text/javascript" src="/admin/theme/js/iconset/iconset-weathericon-1.2.0.min.js"></script>
    <!-- Bootstrap IconPicker -->
    <script type="text/javascript" src="/admin/theme/js/bootstrap-iconpicker.min.js"></script>

    <!-- Bootstrap Gridstack -->
    <script type="text/javascript" src="/admin/theme/js/lodash.js"></script>
    <script type="text/javascript" src="/admin/theme/js/gridstack.js"></script>

    <!-- Chart.js -->
    <script type="text/javascript" src="/admin/theme/js/Chart.min.js"></script>

    <!-- Peity for mini charts -->
    <script type="text/javascript" src="/admin/theme/js/jquery.peity.min.js"></script>

    <!-- jsTree -->
    <!--<script type="text/javascript" src="/admin/theme/js/jstree.min.js"></script>-->
    <script type="text/javascript" src="/admin/theme/js/jstree.js"></script>

	<!-- jQuery Query Builder with plugins -->
	<script type="text/javascript" src="/admin/theme/js/interact.min.js"></script>
    <script type="text/javascript" src="/admin/theme/js/query-builder.standalone.js"></script>

    <!-- Grid Editor -->
    <script type="text/javascript" src="/admin/theme/js/jquery.grideditor.js"></script>

	<!-- Noty -->
    <script type="text/javascript" src="/admin/theme/js/noty.min.js"></script>

    <!-- Ace Editor-->
    <script type="text/javascript" src="/admin/theme/js/vkbeautify.js"></script><!-- to beautify code -->
    <script type="text/javascript" src="/admin/theme/js/ace/ace.js"></script>
<!--
    <script src="http://newsharejs-dani.herokuapp.com/channel/bcsocket.js"></script>
    <script src="http://newsharejs-dani.herokuapp.com/share/share.js"></script>
    <script src="http://newsharejs-dani.herokuapp.com/share/ace.js"></script>
    <script src="http://newsharejs-dani.herokuapp.com/share/textarea.js"></script>
-->
    <!-- ALOHA EDITOR 1 -->
    <script src="/admin/theme/js/aloha.custom.js"></script>
    <!--<script src="/admin/theme/js/aloha/lib/aloha-full.min.js" data-aloha-plugins="common/ui,common/format,common/list,common/link,common/align,common/undo,common/contenthandler,common/block,common/paste,common/commands"></script>-->
    <script src="/admin/theme/js/aloha/lib/aloha-full.js" data-aloha-plugins="common/ui,common/format,common/list,common/link,common/align,common/contenthandler,common/block,common/paste,common/commands"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/admin/theme/js/ie10-viewport-bug-workaround.js"></script>

    <!-- Custom script -->
    <script type="text/javascript" src="/admin/theme/js/custom.js"></script>
  </body>
</html>
