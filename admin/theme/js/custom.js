//var init = true,
//state = window.history.pushState !== undefined;

function addressLinks(){
    jQuery('a.follow').address();
}

function loadInterface(interface_data){
    return AdminAction.loadInterface(interface_data);
}

function updateInterface(){
    // update address
    var fn = '';
    $(document).find('a.follow').each(function(index) {
        if (!$(this).data('address')) {
            $(this).on('click', function(e) {
                if (e.shiftKey || e.ctrlKey || e.metaKey || e.which == 2) {
                    return true;
                }
                var target = e.currentTarget;
                if ($(target).is('a')) {
                    e.preventDefault();
                    var value = fn ? fn.call(target) :
                        /address:/.test($(target).attr('rel')) ? $(target).attr('rel').split('address:')[1].split(' ')[0] :
                        window.addressLink.state() !== undefined && !/^\/?$/.test(window.addressLink.state()) ?
                                $(target).attr('href').replace(new RegExp('^(.*' + window.addressLink.state() + '|\\.)'), '') :
                                $(target).attr('href').replace(/^(#\!?|\.)/, '');
                    window.addressLink.value(value);
                }
            }).on('submit', function(e) {
                var target = e.currentTarget;
                if ($(target).is('form')) {
                    e.preventDefault();
                    var action = $(target).attr('action'),
                        value = fn ? fn.call(target) : (action.indexOf('?') != -1 ? action.replace(/&$/, '') : action + '?') +
                            $(target).serialize();
                    window.addressLink.value(value);
                }
            }).data('address', true);
        }

        if ($(this).attr('href') == (window.addressLink.state() + window.addressLink.value())) {
            $(this).addClass('active').focus();
            $(this).parent('li').addClass('active').focus();
        } else {
            $(this).removeClass('active');
            $(this).parent('li').removeClass('active').focus();
        }

        if (window.addressLink.value()){
            var compose_link = window.addressLink.state() + '/';
            var curr_link = window.addressLink.value();
            var path_split = curr_link.split('/');
            var match_link = $(this).attr('href');
            var curr_object = $(this);
//console.log($(this).attr('href'));
            $.each(path_split, function( index, value ) {
                if (value){
                    compose_link = compose_link + value + '/';

                    if (match_link == compose_link) {//console.log(compose_link);
                        curr_object.addClass('active').focus();
                        curr_object.parent('li').addClass('active').focus();
                    }
                }
            });
        }
    });

    // set the dashboard grid
    setDashboardGrid();

    // initiate Aloha editor
    Aloha.jQuery('.html_edit_simple').aloha();
    Aloha.jQuery('.html_edit_advanced').aloha();

    $(".aloha-editable").each(function() {
        var original_id = $(this).attr("id").replace("-aloha", "");
        var classes = $("#" + original_id).attr("class").replace("input-xxlarge", "").replace("span12", "").replace(" ", "");
        $(this).addClass(classes);
    });

    jQuery('.alohablock').alohaBlock();

    // set Aloha editing tools functionality
    jQuery(".html_edit_simple").focusin(function() {
        $(".aloha-custom-editor").addClass('show');
        $(".simple-editor").css("display", "inline");
        $(".advanced-editor").css("display", "none");
        $(".alignment-editor").css("display", "none");
    });

    jQuery(".html_edit_advanced").focusin(function(event) {
        $(".aloha-custom-editor").addClass('show');
        $(".simple-editor").css("display", "none");
        $(".advanced-editor").css("display", "inline");
        $(".alignment-editor").css("display", "none");

        var id = $(this).attr('id');

    });

    jQuery(".aloha-editable").focusin(function() {
        window.alohaEditable = $(this).attr("id");
        //set_link_settings();
    });

    // Aloha editor button functionalities
    var button = $(".edit-tools-toolbar button.btn-cmd, .edit-tools-toolbar a.btn-cmd, .edit-tools-toolbar a.btn-par");

    button.click( function() {
        var action = $(this).attr('action');
        var action_value = $(this).attr('action-value');
        $(".edit-tools-toolbar").addClass('show');
        $("#" + window.alohaEditable).addClass('aloha-editable-active');
        window.alohaRange.select();


        var curAlohaRange = Aloha.Selection.getRangeObject();
        Aloha.Selection.rangeObject.select();

        if ( Aloha.getSelection().rangeCount > 0 ) {
            range = Aloha.getSelection().getRangeAt( 0 );
        } else {
            //console.log('there is no range');
        }

        if (action != undefined){
            Aloha.execCommand( action, false, action_value );
        }
        $("#" + window.alohaEditable).focus();
        updateActivated(action);
    });

    // bind Aloha functions
    Aloha.bind('aloha-selection-changed aloha-command-executed', function() {

        var curAlohaRange = Aloha.Selection.getRangeObject();
        //console.log('active');
        if (curAlohaRange.startOffset < curAlohaRange.endOffset){
            //console.log('selection made!');
            window.alohaRange = curAlohaRange;
        } else {
            //console.log('no selection');
            window.alohaRange = curAlohaRange;
        }
        //console.log(window.paste_mode);
        if (window.paste_mode == undefined){
            $('.btn-cmd').each(function() {
                var action = $(this).attr('action');
                updateActivated(action);
            });

            var action_value = Aloha.queryCommandValue( 'formatblock' );
            $(".btn-par").removeClass("active");
            $("#formatblock" + action_value).addClass("active");
            $("#paragraph-type").html($("#formatblock" + action_value + ".active").html());

            var process_links = undefined;
        } else {
            var process_links = 'true';
            window.paste_mode = undefined;
        }

        if (typeof window.alohaEditable != 'undefined') {
            var currAlohaEditable = window.alohaEditable;
            var currAlohaEditableBack = currAlohaEditable.replace("-aloha", "");
            matchit(currAlohaEditableBack);
        }

        //set_link_settings();
    });

    // set scrollbar
    var body_container = document.getElementById('content-display');
    if (body_container !== null)
    {
        if ($(document).find(".ps-scrollbar-y").length) {
            //Ps.destroy(body_container);
        } else {
            window.perfectScrollbar = Ps.initialize(body_container, {
                //scrollYMarginOffset: 30
            });
        }
        body_container.scrollTop = 0;
    }

    // set Ace editor
    $( ".code-editor" ).each(function( index ) {
        //console.log( index + ": " + $( this ).text() );
        var editor_id = $(this).attr('id');
        window['editor-' + editor_id] = ace.edit(editor_id);
        window['editor-' + editor_id].setTheme("ace/theme/crimson_editor");
        window['editor-' + editor_id].getSession().setMode("ace/mode/html");
        window['editor-' + editor_id].setOption("wrap", "free"); // line breaking
        window['editor-' + editor_id].setFontSize("14px");
        window['editor-' + editor_id].$blockScrolling = Infinity;
        window['editor-' + editor_id].setDisplayIndentGuides(true);
        window['editor-' + editor_id].session.setFoldStyle("markbeginend");
        window['editor-' + editor_id].setShowFoldWidgets("markbeginend" !== "manual");
        window['editor-' + editor_id].setOptions({maxLines: Infinity}); // adjust the height to the actual size of content
        var code = window['editor-' + editor_id].getSession().getValue();
        code = vkbeautify.xml(code);
        window['editor-' + editor_id].getSession().setValue(code);

        var textarea_id = editor_id.replace('code_', '');
        window['editor-' + editor_id].getSession().on('change', function () {
            //$("#" + textarea_id).val(window['editor-' + editor_id].getSession().getValue());

            if ($("#" + editor_id).is(':visible'))
            {
                var text_code = window['editor-' + editor_id].getSession().getValue();
                matchit_code(textarea_id, text_code);
            }
        });
    });

    $(".btn-preview-desktop").click(function(event){
        var currAlohaEditable = window.alohaEditable;
        var currCodeEditor = "code_" + currAlohaEditable.replace("-aloha", "");

        $(".btn-group-preview button").removeClass('active')
        $(this).addClass('active');

        $("#" + currCodeEditor).css('display', 'none');
        $("#" + currAlohaEditable).css('display', 'block');

        $("#" + currAlohaEditable).removeClass('tablet').removeClass('mobile');

        event.preventDefault();
        return false;
    });

    $(".btn-preview-tablet").click(function(event){
        var currAlohaEditable = window.alohaEditable;
        var currCodeEditor = "code_" + currAlohaEditable.replace("-aloha", "");

        $(".btn-group-preview button").removeClass('active')
        $(this).addClass('active');

        $("#" + currCodeEditor).css('display', 'none');
        $("#" + currAlohaEditable).css('display', 'block');

        $("#" + currAlohaEditable).removeClass('mobile');
        $("#" + currAlohaEditable).addClass('tablet');

        event.preventDefault();
        return false;
    });

    $(".btn-preview-mobile").click(function(event){
        var currAlohaEditable = window.alohaEditable;
        var currCodeEditor = "code_" + currAlohaEditable.replace("-aloha", "");

        $(".btn-group-preview button").removeClass('active')
        $(this).addClass('active');

        $("#" + currCodeEditor).css('display', 'none');
        $("#" + currAlohaEditable).css('display', 'block');

        $("#" + currAlohaEditable).removeClass('tablet');
        $("#" + currAlohaEditable).addClass('mobile');

        event.preventDefault();
        return false;
    });

    $(".btn-preview-code").click(function(event){
        var currAlohaEditable = window.alohaEditable;
        var currCodeEditor = "code_" + currAlohaEditable.replace("-aloha", "");
        /*if ($(this).hasClass('active')){
            $(this).removeClass('active');
            $("#" + currCodeEditor).css('display', 'none');
            $("#" + currAlohaEditable).css('display', 'block');
        } else {*/
            $(".btn-group-preview button").removeClass('active')
            $(this).addClass('active');
            $("#" + currCodeEditor).css('display', 'block');
            $("#" + currAlohaEditable).css('display', 'none');

            $("#" + currAlohaEditable).removeClass('tablet').removeClass('mobile');

            window['editor-' + currCodeEditor].resize(); // update code in editor
            //window['editor-' + currCodeEditor].renderer.updateFull();
        //}
        event.preventDefault();
        return false;
    });

    // when there is a click out of editables
    handleBlur();

    // Query Builder
    /*var rules_plugins = {
      condition: 'AND',
      rules: [{
        id: 'name',
        operator: 'equal',
        value: 'Mistic'
      }, {
        condition: 'OR',
        rules: [{
          id: 'category',
          operator: 'in',
          value: [1, 2]
        }, {
          id: 'in_stock',
          operator: 'equal',
          value: 0
        }]
      }]
  };*/
  var rules_plugins = {
    condition: 'AND',
    rules: []
};

    var query_conditions = JSON.parse($("#query-conditions").text());
    //var query_conditions = $("#query-conditions").text();

    $('#query-builder').queryBuilder({
      plugins: [
        'sortable',
        //'filter-description',
        'unique-filter',
        //'bt-tooltip-errors',
        //'bt-selectpicker',
        //'bt-checkbox',
        'invert',
        //'not-group'
      ],

      /*filters: [{
        id: 'name',
        label: 'Name',
        type: 'string',
        unique: true,
        description: 'This filter is "unique", it can be used only once'
      }, {
        id: 'category',
        label: 'Category',
        type: 'integer',
        input: 'checkbox',
        values: {
          1: 'Books',
          2: 'Movies',
          3: 'Music',
          4: 'Goodies'
        },
        color: 'primary',
        description: 'This filter uses Awesome Bootstrap Checkboxes',
        operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null']
      }, {
        id: 'in_stock',
        label: 'In stock',
        type: 'integer',
        input: 'radio',
        values: {
          1: 'Yes',
          0: 'No'
        },
        colors: {
          1: 'success',
          0: 'danger'
        },
        description: 'This filter also uses Awesome Bootstrap Checkboxes',
        operators: ['equal']
      }, {
        id: 'price',
        label: 'Price',
        type: 'double',
        validation: {
          min: 0,
          step: 0.01
        }
    }],*/
    filters: query_conditions,

      //rules: rules_plugins
    });

    $(".new-condition").click(function(event){
        // reset the modal
        $(".modal-condition-id").val("");
        $(".modal-condition-name").val("");
        $(".modal-condition-layout").prop('selectedIndex',0);
        $('#query-builder').queryBuilder('reset');
    });

    $(".edit-condition").click(function(event){
        var condition_id = $(this).closest(".condition").find(".condition-activation input").val();
        var condition_array = $(this).closest(".condition").find(".condition-activation textarea").val();
        var condition_json = JSON.parse(condition_array);
        var condition_name = $(this).closest(".condition").find(".condition-name").text().trim();

        $(".modal-condition-id").val(condition_id);
        $(".modal-condition-name").val(condition_name);
        $('#query-builder').queryBuilder('setRules', condition_json);
    });

    $(".save-condition").click(function(event){
        var result = $('#query-builder').queryBuilder('getRules');
        var project_id = $(".modal-project-id").val();
        var class_name = $(".modal-class-name").val();
        var condition_id = $(".modal-condition-id").val();
        var condition_name = $(".modal-condition-name").val();
        var condition_layout = $(".modal-condition-layout").val();

        if (!$.isEmptyObject(result)) {
            //alert(JSON.stringify(result, null, 2));
            var condition_json = JSON.stringify(result, null, 2);
            //AdminAction.saveCondition(project_id, condition_id, condition_name, condition_json, condition_layout);
            saveCondition(project_id, class_name, condition_id, condition_name, condition_json, condition_layout).done(function(result){
                //console.log(result);
                if (result.new == true){
                    var new_condition = result.new_html;
                    $(".conditions-table tbody").append(new_condition);
                } else {
                    $("#condition-" + condition_id + " .condition-activation textarea").val(condition_json);
                    $("#condition-" + condition_id + " .condition-name").text(condition_name);
                }
                $("#conditionsModal .close").click();
            });
        }

        return false;
    });

    $(".delete-condition").click(function(event){
        var condition_id = $(this).closest(".condition").find(".condition-activation input").val();
        var project_id = $(".modal-project-id").val();
        var condition_name = $(this).closest(".condition").find(".condition-name").text().trim();
        var class_name = $(".modal-class-name").val();

        bootbox.confirm({
            message: "Do you really want to delete the following condition: <b>" + condition_name + "</b>?",
            buttons: {
                confirm: {
                    label: 'Delete',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn-secondary'
                }
            },
            callback: function (result) {
                //console.log('This was logged in the callback: ' + result);
                if (result == true){
                    deleteCondition(project_id, class_name, condition_id).done(function(result){
                        if (result.answer == 'success'){
                            $(document).find("#condition-" + condition_id).slideUp();
                        }
                    });
                }
            }
        });

        return false;
    });

    $(".new-layout").click(function(event){
        // reset the modal
        $("#layoutModal .layout-name").val("").css('border', '1px solid rgb(204, 204, 204)');
        //$('.layout-editor').html("");

        // remove everything
        $('.layout-editor').empty();
        $('.ge-mainControls *').off().remove();
        $('.ge-mainControls').off().remove();
        $(".ge-html-output").off().remove();

        $(".layout-editor").gridEditor({
            content_types: ["aloha"],
        });
    });

    $(".edit-layout").click(function(event){
        var layout_name = $(this).attr("layout-name");
        //var project_id = $(this).attr("project-id");
        var project_id = $(".modal-project-id").val();

        getLayout(project_id, layout_name).done(function(result){
            $("#layoutModal .layout-name").val(layout_name).css('border', '1px solid rgb(204, 204, 204)');

            // remove everything
            $('.layout-editor').empty();
            $('.ge-mainControls *').off().remove();
            $('.ge-mainControls').off().remove();
            $(".ge-html-output").off().remove();

            if (result.answer == 'success')
            {
                $('.layout-editor').html(result.layout);
            }
            else
            {
                $('.layout-editor').html("");
            }

            $("#layoutModal").modal();
            $(".layout-editor").gridEditor({
                content_types: ["aloha"],
            });
        });

        return false;
    });

    $(".change-layout").click(function(event){
        var condition_json = $(this).closest(".condition").find(".condition-activation textarea").val();
        var condition_id = $(this).closest(".condition").find(".condition-activation input").val();
        var project_id = $(".modal-project-id").val();
        var class_name = $(".modal-class-name").val();

        var condition_name = $(this).closest(".condition").find(".condition-name").text().trim();
        var condition_layout = $(this).find(".layout-select").val();


        saveCondition(project_id, class_name, condition_id, condition_name, condition_json, condition_layout).done(function(result){
            $("#condition-" + condition_id + " .btn-group button.dropdown-toggle").text(condition_layout);
            $("#condition-" + condition_id + " .btn-group").removeClass("show");
        });

        return false;
    });

    $(".save-layout").click(function(event){
        var project_id = $("#layoutModal .modal-project-id").val();
        var layout_name = $("#layoutModal .layout-name").val();
        //var layout_html = $(".ge-html-output").val();
        var layout_html = $('.layout-editor').gridEditor('getHtml');

        if (layout_name){
            saveLayout(project_id, layout_name, layout_html).done(function(result){
                $(".conditions-table .layout-selection").html(result.layout_dropdown);
                updateInterface();
                $("#layoutModal .close").click();
            });
        } else {
            $("#layoutModal .layout-name").css('border', '1px solid red');
        }
    });

    $(".delete-layout").click(function(event){
        var project_id = $("#layoutModal .modal-project-id").val();
        var layout_name = $(this).closest(".change-layout").find(".layout-select").val();

        deleteLayout(project_id, layout_name).done(function(result){
            $(".conditions-table .layout-selection").html(result.layout_dropdown);
            updateInterface();
        });

        return false;
    });

    // save button click
    $(".save-btn").click(function(event){
        var class_name = $(this).attr("class-name");
        var id = $(this).attr("content-id");
        var project_id = $(this).attr("project-id");

        saveContent(class_name, id, project_id);
    });


}

function createAlert(title, body, type = 'alert', timeout = 10000, buttons = []){

    var noty_id = title.replace(/\s/g, '-').toLowerCase();

    if (!$(document).find("#" + noty_id).length)
    {
        window.n[noty_id] = new Noty({
          type: type,
          layout: 'bottomLeft',
          theme: 'bootstrap-v4',
          text: '<h5>' + title + '</h5><p>' + body + '<p>',
          timeout: timeout,
          progressBar: true,
          closeWith: ['click', 'button'],
          animation: {
            open: 'noty_effects_open',
            close: 'noty_effects_close'
          },
          //id: false,
          id: noty_id,
          force: false,
          killer: false,
          queue: 'global',
          container: false,
          buttons: buttons,
          sounds: {
            sources: [],
            volume: 1,
            conditions: []
          },
          titleCount: {
            conditions: []
          },
          modal: false
        }).show();
    }
}


function saveCondition(project_id, class_name, condition_id, condition_name, condition_json, condition_layout){
    return AdminAction.saveCondition(project_id, class_name, condition_id, condition_name, condition_json, condition_layout);
}

function deleteCondition(project_id, class_name, condition_id){
    return AdminAction.deleteCondition(project_id, class_name, condition_id);
}

function saveLayout(project_id, layout_name, layout_html){
    return AdminAction.saveLayout(project_id, layout_name, layout_html);
}

function deleteLayout(project_id, layout_name){
    return AdminAction.deleteLayout(project_id, layout_name);
}

function getLayout(project_id, layout_name){
    return AdminAction.getLayout(project_id, layout_name);
}



function initInterface(){
    window.addressLink = $.address.state('/admin/interface').init(function(event) {
        jQuery('a.follow').address();
    }).change(function(event) {
        // set previous url to go back
        window.back_url = document.URL;



        Aloha.unbind('aloha-selection-changed aloha-command-executed');
        $(".aloha-block").off();

        //$(".edit-tools-toolbar").hide();
        $(".edit-tools-toolbar").removeClass('show');

        Aloha.ready( function() {
            var $ = jQuery = Aloha.jQuery;

            var link_string = event.path;
            path_split = link_string.split('/');

            var interface_array = [];
            $.each(path_split, function( index, value ) {
                if (value){
                    value_split = value.split('-');
                    interface_array[index] = value;
                    //interface_array[value_split[0]] = value_split[1];
                }
            });

            loadInterface(interface_array).done(function(result){
                if (result.answer == "success"){
                    $(".sidebar").html(result.menu);
                    $("#main-content-view").html(result.content);
                    updateInterface();
                }
            });
        });
    });

    // check for updates every 5 minutes
    /*var noty_buttons = [
        Noty.button('Update', 'btn btn-secondary', function () {
            console.log('button 2 clicked');
            n.close();
        })
    ];


    createAlert('Xenium update', 'There is anew update, waiting to be installed.', 'alert', 20000, noty_buttons);
    window.setInterval(function () {createAlert('Xenium update', 'There is anew update, waiting to be installed.', 'alert', 20000, noty_buttons)}, 300000);
    //createAlert('Xenium update', 'There is anew update, waiting to install.<br><a href="#">Click here to update now >></a>', 'alert', 20000);*/

    // initial update check and set interval for later checking
    window.n = [];
    updateCheck();
    window.setInterval(function () {updateCheck()}, 300000);

    // project install click
    $("#project-menu .not-installed a").click(function(){
        var project_id = $(this).attr('data-id');
        var project_name = $(this).attr('data-name');
        var project_slug = $(this).attr('data-slug');
        var repo_url = $(this).attr('data-repo');

        bootbox.confirm({
            message: "The following project will be installed on this system: <b>" + project_name + "</b>?",
            buttons: {
                confirm: {
                    label: 'Install project <b>' + project_name + '</b>',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn-secondary'
                }
            },
            callback: function (result) {
                //console.log('This was logged in the callback: ' + result);
                if (result == true){
                    /*deleteCondition(project_id, class_name, condition_id).done(function(result){
                        if (result.answer == 'success'){
                            $(document).find("#condition-" + condition_id).slideUp();
                        }
                    });*/
                    $('#project-install-modal .project-name').text(project_name);
                    $('#project-install-modal').modal();
                    $('body').addClass('modal-open');
                    installProject(project_id, project_name, project_slug, repo_url);
                }
            }
        });

        return false;
    });
}

function installProject(project_id, project_name, project_slug, repo_url){
    installProjectNow(project_id, project_name, project_slug, repo_url).done(function(result){
        //console.log(result);
        $("#project-install-modal .project-install-text").html(result.message + ' Restarting system...');
        setTimeout(function(){ location.reload(); }, 5000);
    });
}

function installProjectNow(project_id, project_name, project_slug, repo_url){
    return AdminAction.installProject(project_id, project_name, project_slug, repo_url);
}

function commitChanges(project_name, project_repo)
{
    $('#project-commit-modal .project-name').text(project_name);
    $('#project-commit-modal').modal();
    commitChangesNow(project_repo).done(function(result){
        $("#project-commit-modal .project-install-text").html(result.message);
        setTimeout(function(){ $('#project-commit-modal').modal('hide'); }, 3000);
    });
}

function commitChangesNow(project_repo){
    return AdminAction.commitProject(project_repo);
}

function checkoutProject(project_name, project_repo, branch)
{
    var vers = branch.replace('release/', 'v.');
    $('#project-checkout-modal .project-name').text(project_name + ' ' + vers);
    $('#project-checkout-modal').modal();
    checkoutProjectNow(project_repo, branch).done(function(result){
        $("#project-checkout-modal .project-install-text").html(result.message);
        setTimeout(function(){ $('#project-checkout-modal').modal('hide'); }, 3000);
    });
}

function checkoutProjectNow(project_repo, branch){
    return AdminAction.checkoutProject(project_repo, branch);
}

function updateCheck()
{
    checkForUpdates().done(function(result){
        //console.log(result);
        $.each(result.alerts, function(i, item) {
            if (typeof item.type === 'undefined') {
                item.type = 'alert';
            }
            if (typeof item.timeout === 'undefined') {
                item.timeout = 10000;
            }

            // process buttons if any
            var noty_buttons = [];
            if (typeof item.buttons !== 'undefined' && item.buttons.length){
                $.each(item.buttons, function(i, button) {
                    noty_buttons[i] = Noty.button(button.name, button.class, function () {
                        eval(button.func);
                    });
                });
            }
            createAlert(item.title, item.body, item.type, item.timeout, noty_buttons);
        });
    });
}

function checkForUpdates()
{
    return AdminAction.checkForUpdates();
}

function updateActivated(action) {
    if ( Aloha.queryCommandIndeterm( action ) ) {
        $("#" + action + ", #" + action + "-simple").addClass('btn-warning');
		return;
	} else {
        $("#" + action + ", #" + action + "-simple").removeClass('btn-warning');
	}

    $("#" + action + ", #" + action + "-simple").attr( 'class',
        Aloha.queryCommandState( action ) ? 'btn btn-secondary btn-cmd active' : 'btn btn-secondary btn-cmd'
    );
}

function handleBlur()
{
    $(document).click(function(event) {
        if($(event.target).hasClass("aloha-editable") == false && $(event.target).parents(".aloha-editable").length == 0 && $(event.target).parents(".edit-tools-toolbar").length == 0 && $(event.target).parents("#link-popover").length == 0) {
            //$('.edit-tools-toolbar').hide();
            $('.edit-tools-toolbar').removeClass('show');
            $("#link-popover").removeClass("in");
            $('.aloha-editable a[link-editing="true"]').removeAttr('link-editing');

            if ( $(document).find('.aloha-editable img').parent().is( "div.txt-image-container" ) ) {
                var width = $("div.txt-image-container").width();
                var height = $("div.txt-image-container").height();

                $("div.txt-image-container img").attr('style', 'width: ' + width + 'px; height: ' + height + 'px;');
                $(document).find('.ui-resizable-handle').remove();
                $(document).find('div.txt-image-container .aloha-block-handle').remove();
                //$(document).find('.aloha-editable img').unwrap();
                $(document).find("div.txt-image-container").children().unwrap();

            }


            if (window.alohaEditable != undefined)
            {
                var currAlohaEditable = window.alohaEditable;
                var currAlohaEditableBack = currAlohaEditable.replace("-aloha", "");

                patch_launch(currAlohaEditable, currAlohaEditableBack);
            }
        } else {
            if($(event.target).is(".aloha-editable img") == false && $(event.target).parents(".edit-tools-toolbar").length == 0){
                var width = $("div.txt-image-container").width();
                var height = $("div.txt-image-container").height();

                var floating = $("div.txt-image-container").css('float');
                var margin_top = $("div.txt-image-container").css('margin-top');
                var margin_right = $("div.txt-image-container").css('margin-right');
                var margin_bottom = $("div.txt-image-container").css('margin-bottom');
                var margin_left = $("div.txt-image-container").css('margin-left');

                $("div.txt-image-container img").attr('style', 'width: ' + width + 'px; height: ' + height + 'px;');

                $("div.txt-image-container img").css('margin-top', margin_top);
                $("div.txt-image-container img").css('margin-right', margin_right);
                $("div.txt-image-container img").css('margin-bottom', margin_bottom);
                $("div.txt-image-container img").css('margin-left', margin_left);

                if (floating != 'none'){
                    $("div.txt-image-container img").css('float', floating);
                }

                $(document).find('.ui-resizable-handle').remove();
                $(document).find('div.txt-image-container .aloha-block-handle').remove();
                //$(document).find('.aloha-editable img').unwrap();
                $(document).find("div.txt-image-container").children().unwrap();
            }

            //$('.edit-tools-toolbar').show();
            $('.edit-tools-toolbar').addClass('show');
        }

        if ($(event.target).parents(".html_edit_advanced").length > 0){
            if ($(event.target).parents(".alohablock").length > 0 || $(event.target).is(".aloha-editable div.alohablock") == true || $(event.target).is(".aloha-editable img") == true || $(event.target).is(".aloha-editable video") == true){
                //console.log('div clicked');
                $(".simple-editor").css("display", "none");
                $(".advanced-editor").css("display", "none");
                $(".alignment-editor").css("display", "inline");

                var horizontal_size = 0;
                var vertical_size = 0;

                if ($(event.target).closest(".alohablock").hasClass("col-md-1") == true){
                    horizontal_size = 1;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-2") == true){
                    horizontal_size = 2;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-3") == true){
                    horizontal_size = 3;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-4") == true){
                    horizontal_size = 4;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-5") == true){
                    horizontal_size = 5;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-6") == true){
                    horizontal_size = 6;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-7") == true){
                    horizontal_size = 7;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-8") == true){
                    horizontal_size = 8;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-9") == true){
                    horizontal_size = 9;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-10") == true){
                    horizontal_size = 10;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-11") == true){
                    horizontal_size = 11;
                } else if ($(event.target).closest(".alohablock").hasClass("col-md-12") == true){
                    horizontal_size = 12;
                }

                window.horSlider.setValue(horizontal_size);
                $("#horizontal-size-info").html(horizontal_size);

                window.horSlider.on("slide", function(slideEvt) {
                	$("#" + window.editing_element_id).removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12").addClass("col-md-" + slideEvt.value);
                    //console.log(window.editing_element_id);
                    $("#horizontal-size-info").html(slideEvt.value);
                });

                if ($(event.target).closest(".alohablock").hasClass("col-ver-25") == true){
                    vertical_size = 25;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-50") == true){
                    vertical_size = 50;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-75") == true){
                    vertical_size = 75;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-100") == true){
                    vertical_size = 100;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-125") == true){
                    vertical_size = 125;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-150") == true){
                    vertical_size = 150;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-175") == true){
                    vertical_size = 175;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-200") == true){
                    vertical_size = 200;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-225") == true){
                    vertical_size = 225;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-250") == true){
                    vertical_size = 250;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-275") == true){
                    vertical_size = 275;
                } else if ($(event.target).closest(".alohablock").hasClass("col-ver-300") == true){
                    vertical_size = 300;
                }

                window.verSlider.setValue(vertical_size);
                $("#vertical-size-info").html(vertical_size);

                window.verSlider.on("slide", function(slideEvt) {
                    $("#" + window.editing_element_id).removeClass("col-ver-25 col-ver-50 col-ver-75 col-ver-100 col-ver-125 col-ver-150 col-ver-175 col-ver-200 col-ver-225 col-ver-250 col-ver-275 col-ver-300").addClass("col-ver-" + slideEvt.value);
                    $("#vertical-size-info").html(slideEvt.value);
                });


                // set element unique id

                var randomnumber = 'editing-' + Math.floor(Math.random()*10001) + '-' + Math.floor(Math.random()*10001) + '-' + Math.floor(Math.random()*10001);

                if (!$(event.target).parents('.alohablock').attr('id'))
                {
                    $(event.target).parents('.alohablock').attr('id', randomnumber);
                    window.editing_element_id = randomnumber;
                } else {
                    window.editing_element_id = $(event.target).parents('.alohablock').attr('id');
                }

                var floating = $(event.target).parents('.alohablock').css('float');
                console.log(floating);
                if (floating == 'left'){
                    $("input[name='element_float']").attr('checked', '');
                    $("#left-float").attr('checked', 'checked');
                    $("#preview_element").css('float', floating);
                } else if (floating == 'right'){
                    $("input[name='element_float']").attr('checked', '');
                    $("#right-float").attr('checked', 'checked');
                    $("#preview_element").css('float', floating);
                } else if (floating == 'none' || floating == undefined || !floating) {
                    $("input[name='element_float']").attr('checked', '');
                    $("#no-float").attr('checked', 'checked');
                    $("#preview_element").css('float', 'none');
                }

                var element_margin_top = $(event.target).parents('.alohablock').css('margin-top').replace('px', '');
                var element_margin_right = $(event.target).parents('.alohablock').css('margin-right').replace('px', '');
                var element_margin_bottom = $(event.target).parents('.alohablock').css('margin-bottom').replace('px', '');
                var element_margin_left = $(event.target).parents('.alohablock').css('margin-left').replace('px', '');

                $("#margin-top").val(element_margin_top);
                $("#margin-right").val(element_margin_right);
                $("#margin-bottom").val(element_margin_bottom);
                $("#margin-left").val(element_margin_left);

                $("#preview_element").css('margin', element_margin_top + 'px ' + element_margin_right + 'px ' + element_margin_bottom + 'px ' + element_margin_left + 'px');

            } else {
                $(".simple-editor").css("display", "none");
                $(".advanced-editor").css("display", "inline");
                $(".alignment-editor").css("display", "none");

                //$("#" + window.editing_element_id).removeAttribute('id');
                $("#" + window.editing_element_id).removeAttr('id');

                delete window.editing_element_id;
            }
        }
    });
}

function matchit(elem_id)
{
    //var text1 = document.getElementById('contenteditable').innerHTML;
    //var text1 = $('#' + elem_id + '-aloha').html().replace("'", "&#39;").replace('"', "&quot;");
    var text1 = $('#' + elem_id + '-aloha').html();
    var text2 = document.getElementById(elem_id).value;
    //var text2 = $('#poskus').html();
    var patches = dmp.patch_fromText(patch_text);

    var diff = dmp.diff_main(text1, text2, true);

    var ms_start = (new Date).getTime();
    var results = dmp.patch_apply(patches, text1);
    var ms_end = (new Date).getTime();

    document.getElementById(elem_id).value = results[0];
    var code = vkbeautify.xml(results[0]);
    window['editor-code_' + elem_id].getSession().setValue(code);

    var patch_list = dmp.patch_make(text1, text2, diff);
    patch_text2 = dmp.patch_toText(patch_list);

    //console.log("TEXT: " + patch_text2);

    if (patch_list[0] != undefined && patch_list[0]["diffs"] != undefined){
        //console.log(patch_list[0]["diffs"]);

        //sharit(patch_list, elem_id);
    }
}


function matchit_reverse(elem_id)
{ //console.log("onchange triggered");
    var text1 = document.getElementById(elem_id).value;
    var patches = dmp.patch_fromText(patch_text);

    var ms_start = (new Date).getTime();
    var results = dmp.patch_apply(patches, text1);
    var ms_end = (new Date).getTime();

    document.getElementById(elem_id + "-aloha").innerHTML = results[0];

    jQuery('.alohablock').alohaBlock();
}

function matchit_code(elem_id, text)
{
    //var text1 = document.getElementById('contenteditable').innerHTML;
    //var text1 = $('#' + elem_id + '-aloha').html().replace("'", "&#39;").replace('"', "&quot;");
    //var text1 = $('#' + elem_id + '-aloha').html();
    var text1 = text;
    var text2 = document.getElementById(elem_id).value;
    //var text2 = $('#poskus').html();
    var patches = dmp.patch_fromText(patch_text);

    var diff = dmp.diff_main(text1, text2, true);

    var ms_start = (new Date).getTime();
    var results = dmp.patch_apply(patches, text1);
    var ms_end = (new Date).getTime();

    document.getElementById(elem_id).value = results[0];

    var patch_list = dmp.patch_make(text1, text2, diff);
    patch_text2 = dmp.patch_toText(patch_list);

    //console.log("TEXT: " + patch_text2);

    if (patch_list[0] != undefined && patch_list[0]["diffs"] != undefined){
        //console.log(patch_list[0]["diffs"]);

        //sharit(patch_list, elem_id);
    }

    matchit_reverse(elem_id);
}



var dmp = new diff_match_patch();
var patch_text = '';

function patch_launch(id1, id2) {
    if ($("#" + id1).is("input") || $("#" + id1).is("select") || $("#" + id1).is("textarea") || $("#" + id1).is("fieldset")){
        var text1 = $("#" + id1).val();
        var text2 = $("#" + id2).html();
    } else {
        var text1 = $("#" + id1).html(); //console.log(text1);
        var text1 = $("#" + id2).val();
        var current_url = window.location.href;

        var current_url_escaped = addslashes(current_url);

        // REMOVED cause of error "text1 undefined"
        //text1 = text1.replace(new RegExp(current_url_escaped, 'g'), '');

        //$("#" + currAlohaEditableBack).val(text);
    }

    //var diff = dmp.diff_main(text1, text2, true);
    //var patch_list = dmp.patch_make(text1, text2, diff);

    var patches = dmp.patch_fromText(patch_text);
    var results = dmp.patch_apply(patches, text1);
    if ($("#" + id2).is("input") || $("#" + id2).is("select") || $("#" + id2).is("textarea") || $("#" + id2).is("fieldset")){
        $("#" + id2).val(results[0]);

        //sharit(patch_list, elem_id);

        //$("#" + id2).attr('value', results[0]);
    } else {
        $("#" + id2).html(results[0]);
        //matchit_reverse(id2);
    }
}

function addslashes( str ) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

// save functionality

function update_textarea(){
    $(document).find(".aloha-editable").each(function() {
        var currAlohaEditable = $(this).attr('id');
        var currAlohaEditableBack = currAlohaEditable.replace("-aloha", "");
        patch_launch(currAlohaEditable, currAlohaEditableBack);
    });
}

function saveData(class_name, id, project_id, new_values_replaced){
    return AdminAction.save(class_name, id, project_id, new_values_replaced);
}

function saveContent(class_name, id, project_id){

    $(document).find(".save-btn").html('<i class="fa fa-clock-o"></i> &nbsp; <span class="save-btn-txt">Saving...</span>');

    update_textarea();

    var new_values = $('form').serialize();
    new_values = 'form_submit=true&' + new_values;
    //console.log(new_values);
    /*var new_values_replaced_first = new_values.replace(/&/g, 'ANDPARAMETER');
    var new_values_replaced_second = new_values_replaced_first.replace(/%26amp%3B/g, 'AMPERSAND');
    var new_values_replaced_third = new_values_replaced_second.replace(/%2B/g, 'PLUSSIGN');
    var new_values_replaced = new_values_replaced_third.replace(/%26nbsp%3B/g, 'SPACESIGN');*/

    saveData(class_name, id, project_id, new_values).done(function(result){
        if (result.answer == "success"){
            /*$(".sidebar").html(result.menu);
            $("#main-content-view").html(result.content);
            updateInterface();*/
        }
        $(document).find(".save-btn").html('<i class="fa fa-save"></i> &nbsp; <span class="save-btn-txt">Save</span>').blur();
        //console.log(result);
    });
}

function setDashboardGrid(){
        var options = {
            //cell_height: 180,
            //vertical_margin: 0,
            draggable: {
                handle: '.chart-title'
            },
            handle: '.chart-title',
            //cellHeight: '356px',
            cellHeight: '148px'
            //float: true
        };

        $(".grid-stack").gridstack(options);
}

initInterface();
