(function(window, undefined) {
    if (window.Aloha === undefined || window.Aloha === null) {
        window.Aloha = {};		
    }
    
    window.that = {'maxWidth': '50px', 'maxHeight': '50px'};
    
    //that.max-width = '50px';
    //that.max-height = '50px';
    
    window.Aloha.settings = { sidebar: { disabled: true } };
    
    window.Aloha.settings.contentHandler = {
        insertHtml: [ 'word', 'generic', 'oembed', 'sanitize' ],
        //insertHtml: [ 'word', 'oembed', 'sanitize' ],
        initEditable: [ 'sanitize' ],
        getContents: [ 'blockelement', 'sanitize', 'basic' ],
        sanitize: 'relaxed', // relaxed, restricted, basic,
        /*sanitize: {
            '.html_edit_simple': { elements: [ 'b', 'i', 'strong', 'em', 'strike', 'u', 'a' ] },
            '.html_edit_advanced': { elements: [ 'b', 'i', 'strong', 'em', 'strike', 'u', 'a', 'br', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'sub', 'sup', 'ul', 'ol', 'li', 'div', 'img', 'video', 'audio', 'span' ] }
        },*/
        allows: {
            elements: ['strong', 'em', 'i', 'b', 'blockquote', 'br', 'cite', 'code', 'dd', 'div', 'dl', 'dt', 'em', 'i', 'li', 'ol', 'p', 'pre', 'q', 'small', 'strike', 'sub', 'sup', 'u', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'img', 'video', 'audio', 'span'],
            attributes: {
                'div'         : ['class', 'style'],
                'span'        : ['class', 'style'],
                'a'           : ['id', 'class', 'style', 'href']
            }
        },
        handler: {
            generic: {
                transformFormattings: false
            },
            sanitize: {
                '.html_edit_simple': { 
                    elements: [ 'b', 'i', 'strong', 'em', 'strike', 'u', 'a' ], 
                    attributes: {'a': ['id', 'class', 'style', 'href']},
                    //protocols: {'a': {'href': ['ftp', 'http', 'https', 'mailto', '__relative__']}, {'class': ['']}, {'id': ['']}}
                    protocols: {'a': {'href': ['ftp', 'http', 'https', 'mailto', '__relative__']}}
                },
                //'.aloha-input': { elements: [ 'b', 'i', 'strong', 'em', 'strike', 'u', 'a' ] },
                '.html_edit_advanced': { 
                    elements: [ 'b', 'i', 'strong', 'em', 'strike', 'u', 'a', 'br', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'sub', 'sup', 'ul', 'ol', 'li', 'div', 'img', 'video', 'audio', 'span' ], 
                    attributes: {
                        'a': ['id', 'class', 'style', 'href'],
                        'div': ['id', 'class', 'style', 'href'],
                        'span': ['id', 'class', 'style', 'href'],
                        'img': ['id', 'class', 'style', 'href'],
                        'video': ['id', 'class', 'style', 'href'],
                        'audio': ['id', 'class', 'style', 'href'],
                    },
                    protocols: {
                        'a': {'href': ['ftp', 'http', 'https', 'mailto', '__relative__']}
                        }
                }
            }
        }
    }
    
    //Aloha.settings = {
        /*
		logLevels: {'error': true, 'warn': true, 'info': true, 'debug': true},
		errorhandling : false,
		ribbon: false,	
		"i18n": {
			// let the system detect the users language
			//"acceptLanguage": '<?=$_SERVER['HTTP_ACCEPT_LANGUAGE']?>'
			"acceptLanguage": 'fr,de-de,de;q=0.8,it;q=0.6,en-us;q=0.7,en;q=0.2'
		},*/
		/*"plugins": {
			"dragndropfiles": {
				config : { 'drop' : {'max_file_size': '200000',
		   										 'upload': {//'uploader_class':GENTICS.Aloha.Uploader, //this is the default
		   										 			    'uploader_class':Something,
                                                                'config': {
			   										 			'url': '/content/neki/tko/',
                                                                'callback': function(resp) { console.log(resp);},
			   										 			'extra_headers':{'Accept':'application/json', 'Uploading':'true'},
			   										 			'additional_params': {"location":"/neki/tko/tam/"},
						   										'www_encoded': false }}}}
			},
		 	"table": {
				config: ['table']
			},
			"image": {
				config : { 'img': { 'max_width': '50px',
						'max_height': '50px' }},
			  	editables : {
					'#title'	: {},
			  	}
			}
	  	}
	};*/
    
    /*window.Aloha.settings.contentHandler.sanitize = {
        // elements allowed in the content
        elements: [ 'b', 'i', 'strong', 'em', 'strike', 'u', 'a', 'br', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'sub', 'sup', 'ul', 'ol', 'li', 'div', 'img', 'video', 'audio', 'span' ],
        // attributes allowed for specific elements
        attributes: {
                'div'         : ['class', 'style'],
                'span'        : ['class', 'style'],
                'a'           : ['class', 'style']
            },
        // protocols allowed for certain attributes of elements
        protocols: {
            'a' : {'href': ['ftp', 'http', 'https', 'mailto', '__relative__']},
            'blockquote' : {'cite': ['http', 'https', '__relative__']},
            'q' : {'cite': ['http', 'https', '__relative__']}
        }
    }*/
    
})(window);
/*
Aloha.settings = {
        contentHandler: {
            sanitize: {
                '.html_edit_simple': { elements: [ 'b', 'strong', 'i', 'em', 'strike', 'a' ] },
                '.html_edit_advanced': { elements: [ 'b', 'strong', 'i', 'em', 'strike', 'a' ] }
            }
        }
    };*/
//Aloha.settings.contentHandler.handler = {};
//Aloha.settings.contentHandler.handler.sanitize = {};
//Aloha.settings.contentHandler.handler.sanitize['.html_edit_simple'] = { elements : [ 'b', 'i', 'strong', 'em', 'strike', 'u', 'a' ] };
//Aloha.settings.contentHandler.handler.sanitize['.html_edit_advanced'] = { elements : [ 'b', 'i', 'strong', 'em', 'strike', 'u', 'a', 'br', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'sub', 'sup', 'ul', 'ol', 'li', 'div', 'img', 'video', 'audio' ] };
