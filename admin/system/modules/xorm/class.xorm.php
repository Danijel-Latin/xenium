<?php

class Xorm
{
    protected $db;

    protected $module;

    // query types
    protected $select_query;
    protected $update_query;
    protected $delete_query;
    protected $insert_query;

    // query assets
    protected $select_fields = array();
    protected $update_fields = array();
    protected $insert_fields = array();
    protected $query_limit;
    protected $query_page;
    protected $query_order_by;
    protected $previous_table;
    protected $current_table;
    protected $named_placeholders = array();
    protected $query_where;
    protected $named_placeholder_number;
    protected $join_where_init;
    protected $tables = array();
    protected $table_joins = array();
    //protected $join_table;

    // cache assets
    protected $table_cache = array();
    protected $permanent_cache = array();
    protected $smarty_cache = array();

    protected $where = array();

    public function __construct($module, $table = '')
    {
        if (isset($module) && is_string($module) && $module)
        {
            $call_module = new $module;
            $this->module = $call_module;
        }
        elseif (isset($module) && is_object($module))
        {
            $this->module = $module;
        }
        else
        {
            trigger_error('A module object or module name must be passed to Xorm to perform queries', E_USER_ERROR);
        }


        if (isset($this->module->table) && !is_array($this->module->table))
        {
            $table_name = $this->module->table;
            if (isset($this->module->table_fields) && is_array($this->module->table_fields))
            {
                if (empty($this->module->table_fields)) $this->module->table_fields = '*';
                $this->current_table = $table_name;
                $this->fields($this->module->table_fields);
            }
        }
        else
        {
            if ($table && in_array($table, $this->module->tables))
            {
                $table_name = $table;
            }
            else
            {
                if (isset($this->module->tables[0]))
                {
                    $table_name = $this->module->tables[0];
                }
                else
                {
                    $table_name = false;
                }
            }
        }
        if ($table_name)
        {
            $this->current_table = $table_name;
            $this->tables[$table_name] = $table_name;
        }

    }

    public function __get($property)
    {
        /*if (property_exists($this, $property)) {
            return $this->$property;
        }*/
        //$this->setTable($property);
        $this->current_table = $property;
        return $this;
    }

    public static function query($query, $fetch = true)
    {
        $db = new PDO(
            'mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8',
            DB_USER,
            DB_PASS,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
        );

        $db_result = $db->prepare($query);
        $db_result->execute();

        if ($fetch)
        {
            $result = $db_result->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
            $result = true;
        }

        $db = null;

        return $result;
    }

    public static function set($module)
    {
        $xorm = new Xorm($module);
        return $xorm;
    }

    private function connect()
    {
        $this->db = new PDO(
            'mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8',
            DB_USER,
            DB_PASS,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
        );
    }

    private function disconnect()
    {
        $this->db = null;
    }

    public function fields($fields = '*')
    {
        if (!isset($this->current_table))
        {
            $module_tables = $this->module->tables;
            reset($module_tables);
            $select_table = key($module_tables);
            $this->setTable($select_table, $fields);
        }
        else
        {
            if (is_array($fields))
            {
                // if "id" is not in fields array and table is not join origin, add it automatically
                if (!in_array('id', $fields) && !$this->previous_table)
                {
                    $this->select_fields[] = $this->current_table . '.id';
                }
                foreach ($fields as $field_key => $field)
                {
                    $this->select_fields[] = $this->current_table . '.' . $field;
                }
                $fields = implode(', ', $fields);
            }

            if ($fields == '*')
            {
                $this->select_fields[] = $this->current_table . '.' . $fields;
            }

            if (!$this->select_query)
            {
                $this->select_query = "SELECT [fields] FROM " . $this->current_table . " ";
            }
        }

        return $this;
    }

    public function setTable($table_name, $fields = '*')
    {
        $this->tables[$table_name] = $table_name;

        if (isset($this->module->tables) && is_array($this->module->tables))
        {
            //if (in_array($table_name, $this->module->tables))
            if (isset($this->module->tables[$table_name]))
            {
                if (is_array($fields))
                {
                    foreach ($fields as $field_key => $field)
                    {
                        //$fields[$field_key] = $table_name . '.' . $field;
                        $this->select_fields[] = $table_name . '.' . $field;
                    }
                    $fields = implode(', ', $fields);
                }

                if ($fields == '*')
                {
                    //$fields = $table_name . '.' . $fields;
                    $this->select_fields[] = $table_name . '.' . $fields;
                }

                $this->join_to_table = $table_name;
                $this->current_table = $table_name;
                $this->select_query = "SELECT [fields] FROM " . $table_name . " ";

                return $this;
            }
            else
            {
                trigger_error('There is no table with name "' . $table_name . '" in module "' . get_class($this->module) . '"', E_USER_ERROR);
            }
        }
        else
        {
            trigger_error('"tables" property in module "' . get_class($this->module) . '" is not an array or not set', E_USER_ERROR);
        }
    }

    public function innerJoin($table_name, $on)
    {
        $this->join('inner', $table_name, $on);
        return $this;
    }

    public function leftJoin($table_name, $on)
    {
        $this->join('left', $table_name, $on);
        return $this;
    }

    public function rightJoin($table_name, $on)
    {
        $this->join('right', $table_name, $on);
        return $this;
    }

    public function outerJoin($table_name, $on)
    {
        $this->join('outer', $table_name, $on);
        return $this;
    }

    public function join($method, $table_name, $on)
    {
        if (is_object($table_name))
        {
            $this->tables[$table_name->table] = $table_name->table;
            $this->previous_table = $this->current_table;
            $this->current_table = $table_name->table;

            if (isset($table_name->fields) && is_array($table_name->fields))
            {
                if (empty($table_name->fields)) $table_name->fields = '*';
                $this->fields($table_name->fields);
            }

            $table_name = $table_name->table;
        }
        else
        {
            $this->tables[$table_name] = $table_name;
            $this->previous_table = $this->current_table;
            $this->current_table = $table_name;
        }

        $this->join_where_init = true;

        if (is_array($this->module->tables))
        {
            //if (in_array($table_name, $this->module->tables))
            //{
                if (is_array($on))
                {
                    $on_query = $this->previous_table . '.' . key($on) . ' = ' . $this->current_table . '.' . $on[key($on)];
                }
                else
                {
                    $on_query = $on;
                }

                switch ($method)
                {
                    case 'inner':
                        $this->select_query .= " INNER JOIN " . $table_name . " ON " . $on_query;
                        $this->table_joins[] = $this->previous_table . " INNER JOIN " . $table_name . " ON " . $on_query;
                        break;
                    case 'left':
                        $this->select_query .= " LEFT JOIN " . $table_name . " ON " . $on_query;
                        $this->table_joins[] = $this->previous_table . " LEFT JOIN " . $table_name . " ON " . $on_query;
                        break;
                    case 'right':
                        $this->select_query .= " RIGHT JOIN " . $table_name . " ON " . $on_query;
                        $this->table_joins[] = $this->previous_table . " RIGHT JOIN " . $table_name . " ON " . $on_query;
                        break;
                    case 'outer':
                        //$this->select_query .= " FULL OUTER JOIN " . $table_name . " ON " . $on_query;
                        $current_query = $this->select_query;
                        $this->select_query .= " LEFT JOIN " . $table_name . " ON " . $on_query . " UNION " . $current_query . " RIGHT JOIN " . $table_name . " ON " . $on_query;
                        $this->table_joins[] = $this->previous_table . " FULL OUTER JOIN " . $table_name . " ON " . $on_query;
                        break;
                }
            /*}
            else
            {
                trigger_error('There is no table with name "' . $table_name . '" in module "' . get_class($this->module) . '"', E_USER_ERROR);
            }*/
        }
        else
        {
            trigger_error('Table property in module "' . get_class($this->module) . '" is not an array', E_USER_ERROR);
        }

        return $this;
    }

    public function limit($limit)
    {
        $this->query_limit = intval($limit);
        return $this;
    }

    public function page($page)
    {
        $this->query_page = intval($page);
        return $this;
    }

    public function orderBy($field, $positioning = 'DESC')
    {
        $this->query_order_by = ' ORDER BY ' . $this->current_table . '.' . $field . ' ' . $positioning . ' ';
        return $this;
    }

    public function where($where_clauses = array())
    {
        $clauses_number = count($where_clauses);
        $clauses_counter = 0;

        if (!$this->query_where) $this->query_where .=  ' WHERE';

        foreach ($where_clauses as $field => $value)
        {
            if ($field == 'AND' || $field == 'OR')
            {
                $field_funcs['prefix'] = $field;
                $field_funcs['name'] = '';
                $field_funcs['operator'] = '';
            }
            else
            {
                $preg_match_1 = preg_match('/(?P<prefix>AND|OR)\::(?P<name>[a-zA-Z0-9-_.]+)\::(?P<operator>=|<=|>=|!=|<>|<|>|LIKE|NOT LIKE|IN|NOT IN|BETWEEN|NOT BETWEEN|REGEXP|NOT REGEXP)/', $field, $field_funcs);
                if (!$preg_match_1) $preg_match_2 = preg_match('/(?P<name>[a-zA-Z0-9-_.]+)\::(?P<operator>=|<=|>=|!=|<>|<|>|LIKE|NOT LIKE|IN|NOT IN|BETWEEN|NOT BETWEEN|REGEXP|NOT REGEXP)/', $field, $field_funcs);
                if (!$preg_match_1 && !$preg_match_2) $preg_match_3 = preg_match('/(?P<prefix>AND|OR)\::(?P<name>[a-zA-Z0-9-_.]+)/', $field, $field_funcs);

                if (!$preg_match_1 && !$preg_match_2 && !$preg_match_3)
                {
                    $field_funcs['prefix'] = 'AND';
                    $field_funcs['name'] = $field;
                    $field_funcs['operator'] = '=';
                }

                if (!isset($field_funcs['prefix'])) $field_funcs['prefix'] = 'AND';
                if (!isset($field_funcs['name'])) $field_funcs['name'] = $field;
                if (!isset($field_funcs['operator'])) $field_funcs['operator'] = '=';

            }

            $clauses_counter++;

            if ($clauses_number >= $clauses_counter && $clauses_counter > 1)
            {
                if ($this->query_where != ' WHERE')
                {
                    $this->query_where .= ' ' . $field_funcs['prefix'];
                }
            }

            if (!isset($this->named_placeholder_number[$field_funcs['name']]))
            {
                $this->named_placeholder_number[$field_funcs['name']] = 0;
            }

            if (isset($this->named_placeholders[':' . $field_funcs['name'] . $this->named_placeholder_number[$field_funcs['name']]]))
            {
                $this->named_placeholder_number[$field_funcs['name']]++;
            }

            if (is_array($value) && $field_funcs['operator'] == 'IN')
            {
                $in_counter = 0;
                $in_statement = array();
                foreach ($value as $in_value)
                {
                    $this->named_placeholders[':' . str_replace('.', '_', $field_funcs['name']) . $this->named_placeholder_number[$field_funcs['name']]] = $in_value;
                    $in_statement[$in_counter] = ':' . str_replace('.', '_', $field_funcs['name']) . $this->named_placeholder_number[$field_funcs['name']];
                    $in_counter++;
                }
                $in_statement_string = '(' . implode(', ', $in_statement) . ')';
                $this->named_placeholders[':' . str_replace('.', '_', $field_funcs['name']) . $this->named_placeholder_number[$field_funcs['name']]] = $in_statement_string;
                $this->query_where .= ' ' . $this->current_table . '.' . $field_funcs['name'] . ' ' . $field_funcs['operator'] . ' ' . ':' . str_replace('.', '_', $field_funcs['name']) . $this->named_placeholder_number[$field_funcs['name']];
            }
            elseif (is_array($value) && ($field_funcs['prefix'] == 'AND' || $field_funcs['prefix'] == 'OR') && !$field_funcs['name']) // if there is another multiple condition
            {
                if (!isset($this->join_where_init) || !$this->join_where_init)
                {
                    $this->query_where .= ' (';
                }
                elseif ($this->join_where_init == true)
                {
                    $this->join_where_init = false;
                    $this->query_where .= ' ' . $field_funcs['prefix'] . ' (';
                }
                $this->where($value);
                $this->query_where .= ' )';
            }
            elseif (is_array($value) && array_keys($value) === range(0, count($value) - 1) && $field_funcs['name'] && $field_funcs['operator'])
            {
                foreach ($value as $field_num => $field_value)
                {
                    $this->named_placeholders[':' . str_replace('.', '_', $field_funcs['name']) . $this->named_placeholder_number[$field_funcs['name']]] = $field_value;
                    if ($field_num == 0 && !$this->join_where_init)
                    {
                        $this->query_where .= ' ' . $this->current_table . '.' . $field_funcs['name'] . ' ' . $field_funcs['operator'] . ' ' . ':' . str_replace('.', '_', $field_funcs['name']) . $this->named_placeholder_number[$field_funcs['name']];
                    }
                    else
                    {
                        $this->join_where_init = false;
                        if ($this->query_where != ' WHERE')
                        {
                            $this->query_where .= ' ' . $field_funcs['prefix'];
                        }
                        $this->query_where .=  ' ' . $this->current_table . '.' . $field_funcs['name'] . ' ' . $field_funcs['operator'] . ' ' . ':' . str_replace('.', '_', $field_funcs['name']) . $this->named_placeholder_number[$field_funcs['name']];
                    }
                    $this->named_placeholder_number[$field_funcs['name']]++;
                }
            }
            else
            {
                $this->named_placeholders[':' . str_replace('.', '_', $field_funcs['name']) . $this->named_placeholder_number[$field_funcs['name']]] = $value;
                if (isset($this->join_where_init) && $this->join_where_init == true)
                {
                    $this->join_where_init = false;
                    if ($this->query_where != ' WHERE')
                    {
                        $this->query_where .= ' ' . $field_funcs['prefix'];
                    }
                    $this->query_where .=  ' ' . $this->current_table . '.' . $field_funcs['name'] . ' ' . $field_funcs['operator'] . ' ' . ':' . str_replace('.', '_', $field_funcs['name']) . $this->named_placeholder_number[$field_funcs['name']];
                }
                else
                {
                    $this->query_where .= ' ' . $this->current_table . '.' . $field_funcs['name'] . ' ' . $field_funcs['operator'] . ' ' . ':' . str_replace('.', '_', $field_funcs['name']) . $this->named_placeholder_number[$field_funcs['name']];
                }
            }
        }

        return $this;
    }

    public function select($caching = true)
    {
        global $_DEBUG, $_PROJECT;

        // check if query is project related
        $project_related = false;
        if (isset($_PROJECT) && is_array($_PROJECT))
        {
            $project_related = true;
        }

        // set up Cache module
        $cache = new Cache($project_related, $_PROJECT);

        // start microtime measurment
        $xorm_start_time = microtime(true);
        $xorm_start_memory = memory_get_usage();

        if (!isset($this->select_fields) || empty($this->select_fields))
        {
            $this->fields();
        }

        $select_fields = implode(', ', $this->select_fields);
        $this->select_query = str_replace('[fields]', $select_fields, $this->select_query);

        $this->select_query .= $this->query_where;

        // ordering the result
        if (isset($this->query_order_by) && $this->query_order_by)
        {
            $this->select_query .= $this->query_order_by;
        }

        // if a limit is set
        if (isset($this->query_limit) && $this->query_limit)
        {
            $this->select_query .= " LIMIT " . $this->query_limit;
        }

        // get file and line info
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $backtrace = $backtrace[0];

        // check if cache exists
        $query_hash = sha1(SALT . $this->select_query . serialize($this->named_placeholders));

        if ($cache->exists($query_hash) && $caching)
        {
            $cache_result = unserialize(gzdecode($cache->get($query_hash)));

            $_DEBUG['xorm'][][$this->select_query] = [
                'calling_file' => $backtrace,
                'execution' => [
                    'time' => convertTime(number_format(microtime(true) - $xorm_start_time, 5)),
                    'memory' => convertMemory(memory_get_usage(true) - $xorm_start_memory),
                    'source' => 'cache'
                ],
                'tables' => $this->tables,
                'table_joins' => $this->table_joins,
                'placeholders' => $this->named_placeholders,
                'results' => $cache_result
            ];

            return $cache_result;
        }
        else
        {
            // connect to database
            $this->connect();

            try
            {
                $db_result = $this->db->prepare($this->select_query);
                $db_result->execute($this->named_placeholders);
                if (isset($this->query_limit) && $this->query_limit == 1)
                {
                    $fetched_row = $db_result->fetch(PDO::FETCH_ASSOC);

                    $_DEBUG['xorm'][][$this->select_query] = [
                        'calling_file' => $backtrace,
                        'execution' => [
                            'time' => convertTime(number_format(microtime(true) - $xorm_start_time, 5)),
                            'memory' => convertMemory(memory_get_usage(true) - $xorm_start_memory),
                            'source' => 'database'
                        ],
                        'tables' => $this->tables,
                        'table_joins' => $this->table_joins,
                        'placeholders' => $this->named_placeholders,
                        'results' => $fetched_row
                    ];

                    if ($caching) $cache->set($query_hash, gzencode(serialize($fetched_row)));

                    return $fetched_row;
                }
                else
                {
                    //$fetched_rows = $db_result->fetchAll(PDO::FETCH_ASSOC); //FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC
                    //$fetched_rows = $db_result->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);
                    $fetched_rows = array();
                    $arr_position = 0;
                    while ($row = $db_result->fetch(PDO::FETCH_ASSOC)) {
                        if (!isset($fetched_rows[$row['id']]))
                        {
                            foreach ($row as $row_key => $row_value)
                            {
                                $fetched_rows[$row['id']][$row_key] = $row_value;
                            }
                            $fetched_rows[$row['id']]['arr_position'] = $arr_position;
                            $arr_position++;
                        }
                        else
                        {
                            if (isset($fetched_rows[$row['id']]['id'])) // if the row with the same id already exists
                            {
                                $temp_array = $fetched_rows[$row['id']];
                                $fetched_rows[$row['id']] = array();
                                $fetched_rows[$row['id']][] = $temp_array;

                                // clear the temp array
                                $temp_array = array();
                                foreach ($row as $row_key => $row_value)
                                {
                                    $temp_array[$row_key] = $row_value;
                                }
                                $temp_array['arr_position'] = $arr_position;
                                $arr_position++;
                                // add to the results under same id
                                $fetched_rows[$row['id']][] = $temp_array;
                            }
                            else
                            {
                                // create a new temp array and add it to the results under the same id
                                $temp_array = array();
                                foreach ($row as $row_key => $row_value)
                                {
                                    $temp_array[$row_key] = $row_value;
                                }
                                $temp_array['arr_position'] = $arr_position;
                                $arr_position++;
                                // add to the results under same id
                                $fetched_rows[$row['id']][] = $temp_array;
                            }
                        }
                    }

                    $_DEBUG['xorm'][][$this->select_query] = [
                        'calling_file' => $backtrace,
                        'execution' => [
                            'time' => convertTime(number_format(microtime(true) - $xorm_start_time, 5)),
                            'memory' => convertMemory(memory_get_usage() - $xorm_start_memory),
                            'source' => 'database'
                        ],
                        'tables' => $this->tables,
                        'table_joins' => $this->table_joins,
                        'placeholders' => $this->named_placeholders,
                        'results' => $fetched_rows
                    ];

                    if ($caching) $cache->set($query_hash, gzencode(serialize($fetched_rows)));

                    return $fetched_rows;
                }
            }
            catch (PDOException $ex)
            {
                //return false;
                //echo "An Error occured!"; //user friendly message
                //some_logging_function($ex->getMessage());
                //echo $this->select_query;
                //echo $ex->getMessage();
                $_DEBUG['xorm'][][$this->select_query] = [
                    'calling_file' => $backtrace,
                    'execution' => [
                        'time' => convertTime(number_format(microtime(true) - $xorm_start_time, 5)),
                        'memory' => convertMemory(memory_get_usage() - $xorm_start_memory),
                        'source' => 'database'
                    ],
                    'tables' => $this->tables,
                    'table_joins' => $this->table_joins,
                    'placeholders' => $this->named_placeholders,
                    'error' => $ex->getMessage()
                ];
            }

            $this->disconnect();
        }
    }

    public function freshSelect()
    {
        return $this->select(false);
    }

    public function selectById($id, $cache = true)
    {
        $id = intval($id);
        $this->where(['id' => $id])->select($cache);
    }

    //*********************//
    // update/insert query //
    //*********************//

    public function getCurrentFields($table)
    {
        $db_result = Xorm::query("SHOW COLUMNS FROM " . $table . ";");
        $return_result = array();

        foreach ($db_result as $field)
        {
            $return_result[$field['Field']] = $field;
        }

        return $return_result;
    }

    public function setFields($fields)
    {
        global $_DEBUG, $_PROJECT;

        // check if query is project related
        $project_related = false;
        $project_id = 0;

        if (isset($_PROJECT) && is_array($_PROJECT))
        {
            $project_related = true;
            $project_id = $_PROJECT['id'];
        }

        if (!isset($this->current_table))
        {
            $module_tables = $this->module->tables;
            reset($module_tables);
            $select_table = key($module_tables);
            $this->current_table = $select_table;
        }
        $current_fields = $this->getCurrentFields($this->current_table);

        if (is_array($fields) && !empty($fields))
        {
            //$table_structure = $this->module->getTableStructure($this->current_table);
            $table_structure = $this->module->tables[$this->current_table];
            $table_fields = $this->module->getTableFields($project_id, $table_structure);

            unset($fields['id']);
            $alter_fields = '';

            foreach ($fields as $field => $field_value)
            {
                if (isset($table_fields[$field]))
                {
                    // check if field exists in current structure, alter table if not
                    if (!isset($current_fields[$field]))
                    {
                        if (!$alter_fields)
                        {
                            $alter_fields .= ' ADD COLUMN ' . $field . ' ' . $table_fields[$field]['field_type'];
                        }
                        else
                        {
                            $alter_fields .= ', ADD COLUMN ' . $field . ' ' . $table_fields[$field]['field_type'];
                        }

                        if (isset($last_processed_field))
                        {
                            $alter_fields .= ' AFTER ' . $last_processed_field;
                        }
                    }

                    $this->update_fields[] = $this->current_table . '.' . $field . ' = ' . ':update_' . $this->current_table . '_' . $field;
                    $this->insert_fields[$this->current_table . '.' . $field] = ':update_' . $this->current_table . '_' . $field;
                    $this->named_placeholders[':update_' . $this->current_table . '_' . $field] = trim($field_value);

                    $last_processed_field = $field;
                }
            }

            // set up fields first
            if ($alter_fields)
            {
                $alter_query = 'ALTER TABLE ' . $this->current_table . ' ' . $alter_fields;
                Xorm::query($alter_query, false); die();
            }

            $fields = implode(', ', $this->update_fields);

            if (!$this->update_query)
            {
                $this->update_query = "UPDATE " . $this->current_table . " SET " . $fields . " ";
            }

            if (!$this->insert_query)
            {
                $this->insert_query = "INSERT INTO " . $this->current_table . " (" . implode(", ", array_keys($this->insert_fields)) . ") VALUES (" . implode(", ", $this->insert_fields) . ") ";
            }
        }

        return $this;
    }

    public function update()
    {
        if ($this->update_query)
        {
            if ($this->query_where)
            {
                $this->update_query .= ' ' . $this->query_where;
            }

            $this->connect();
            $db_result = $this->db->prepare($this->update_query);
            $db_result->execute($this->named_placeholders);
            $this->disconnect();
        }
    }

    public function updateId($id = 0)
    {
        if ($this->update_query && $id)
        {
            if ($this->query_where)
            {
                $this->update_query .= ' WHERE id = ' . intval($id);
            }

            $this->connect();
            $db_result = $this->db->prepare($this->update_query);
            $db_result->execute($this->named_placeholders);
            $this->disconnect();
        }
    }

    public function insert($fields = array())
    {
        if (!empty($fields))
        {
            $this->setFields($fields);
        }

        if ($this->insert_query)
        {
            $this->connect();
            $db_result = $this->db->prepare($this->insert_query);
            $db_result->execute($this->named_placeholders);
            $last_id = $this->db->lastInsertId();
            $this->disconnect();

            return $last_id;
        }
    }

    public function delete($where = array())
    {
        if (!isset($this->current_table))
        {
            $module_tables = $this->module->tables;
            reset($module_tables);
            $select_table = key($module_tables);
            $this->current_table = $select_table;
        }

        $this->delete_query = "DELETE FROM " . $this->current_table;

        if (!empty($where))
        {
            $this->where($where);
            $this->delete_query .= " " . $this->query_where;
        }

        $this->connect();
        $db_result = $this->db->prepare($this->delete_query);
        $db_result->execute($this->named_placeholders);
        $this->disconnect();
    }

    public function deleteId($id = 0)
    {
        $id = intval($id);

        if ($id)
        {
            $this->delete(['id' => $id]);
        }
    }
}

?>
