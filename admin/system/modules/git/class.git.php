<?php
	/**
	 * Default implementation of IGit interface
	 *
	 * @author  Jan Pecha, <janpecha@email.cz>
	 * @license New BSD License (BSD-3), see file license.md
	 *
	 * Update 22.07.2017
	 * @author Danijel Latin
	 * Minor structure changes to meet Xenium demands
	 */

	//namespace Cz\Git;

	class Git
	{
		/** @var  string */
		protected $repository;

		/** @var  string|NULL  @internal */
		protected $cwd;


		/**
		 * @param  string
		 */
		public function __construct($repository)
		{
			if(basename($repository) === '.git')
			{
				$repository = dirname($repository);
			}

			$this->repository = realpath($repository);

			if($this->repository === FALSE)
			{
				throw new GitException("Repository '$repository' not found.");
			}
		}


		/**
		 * @return string
		 */
		public function getRepositoryPath()
		{
			return $this->repository;
		}


		/**
		 * Creates a tag.
		 * `git tag <name>`
		 * @param  string
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function createTag($name)
		{
			return $this->begin()
				->run('git tag', $name)
				->end();
		}


		/**
		 * Removes tag.
		 * `git tag -d <name>`
		 * @param  string
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function removeTag($name)
		{
			return $this->begin()
				->run('git tag', array(
					'-d' => $name,
				))
				->end();
		}


		/**
		 * Renames tag.
		 * `git tag <new> <old>`
		 * `git tag -d <old>`
		 * @param  string
		 * @param  string
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function renameTag($oldName, $newName)
		{
			return $this->begin()
				// http://stackoverflow.com/a/1873932
				// create new as alias to old (`git tag NEW OLD`)
				->run('git tag', $newName, $oldName)
				// delete old (`git tag -d OLD`)
				->removeTag($oldName) // WARN! removeTag() calls end() method!!!
				->end();
		}


		/**
		 * Returns list of tags in repo.
		 * @return string[]|NULL  NULL => no tags
		 */
		public function getTags()
		{
			return $this->extractFromCommand('git tag', 'trim');
		}


		/**
		 * Merges branches.
		 * `git merge <options> <name>`
		 * @param  string
		 * @param  array|NULL
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function merge($branch, $options = NULL)
		{
			return $this->begin()
				->run('git merge', $options, $branch)
				->end();
		}


		/**
		 * Creates new branch.
		 * `git branch <name>`
		 * (optionaly) `git checkout <name>`
		 * @param  string
		 * @param  bool
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function createBranch($name, $checkout = FALSE)
		{
			$this->begin();

			// git branch $name
			$this->run('git branch', $name);

			if($checkout)
			{
				$this->checkout($name);
			}

			return $this->end();
		}


		/**
		 * Removes branch.
		 * `git branch -d <name>`
		 * @param  string
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function removeBranch($name)
		{
			return $this->begin()
				->run('git branch', array(
					'-d' => $name,
				))
				->end();
		}


		/**
		 * Gets name of current branch
		 * `git branch` + magic
		 * @return string
		 * @throws Cz\Git\GitException
		 */
		public function getCurrentBranchName()
		{
			try
			{
				$branch = $this->extractFromCommand('git branch -a', function($value) {
					if(isset($value[0]) && $value[0] === '*')
					{
						return trim(substr($value, 1));
					}

					return FALSE;
				});

				if(is_array($branch))
				{
					return $branch[0];
				}
			}
			catch(Exception $e) {}
			throw new Exception('Getting current branch name failed.');
			//echo 'Getting current branch name failed.':
		}


		/**
		 * Returns list of all (local & remote) branches in repo.
		 * @return string[]|NULL  NULL => no branches
		 */
		public function getBranches()
		{
			return $this->extractFromCommand('git branch -a', function($value) {
				return trim(substr($value, 1));
			});
		}


		/**
		 * Returns list of local branches in repo.
		 * @return string[]|NULL  NULL => no branches
		 */
		public function getLocalBranches()
		{
			return $this->extractFromCommand('git branch', function($value) {
				return trim(substr($value, 1));
			});
		}


		/**
		 * Checkout branch.
		 * `git checkout <branch>`
		 * @param  string
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function checkout($name)
		{
			return $this->begin()
				->run('git checkout', $name)
				->end();
		}


		/**
		 * Removes file(s).
		 * `git rm <file>`
		 * @param  string|string[]
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function removeFile($file)
		{
			if(!is_array($file))
			{
				$file = func_get_args();
			}

			$this->begin();

			foreach($file as $item)
			{
				$this->run('git rm', $item, '-r');
			}

			return $this->end();
		}


		/**
		 * Adds file(s).
		 * `git add <file>`
		 * @param  string|string[]
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function addFile($file)
		{
			if(!is_array($file))
			{
				$file = func_get_args();
			}

			$this->begin();

			foreach($file as $item)
			{
				// TODO: ?? is file($repo . / . $item) ??
				$this->run('git add', $item);
			}

			return $this->end();
		}


		/**
		 * Adds all created, modified & removed files.
		 * `git add --all`
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function addAllChanges()
		{
			return $this->begin()
				->run('git add --all')
				->end();
		}


		/**
		 * Renames file(s).
		 * `git mv <file>`
		 * @param  string|string[]  from: array('from' => 'to', ...) || (from, to)
		 * @param  string|NULL
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function renameFile($file, $to = NULL)
		{
			if(!is_array($file)) // rename(file, to);
			{
				$file = array(
					$file => $to,
				);
			}

			$this->begin();

			foreach($file as $from => $to)
			{
				$this->run('git mv', $from, $to);
			}

			return $this->end();
		}


		/**
		 * Commits changes
		 * `git commit <params> -m <message>`
		 * @param  string
		 * @param  string[]  param => value
		 * @throws Cz\Git\GitException
		 * @return self
		 */
		public function commit($message, $params = NULL)
		{
			if(!is_array($params))
			{
				$params = array();
			}

			return $this->begin()
				->run("git commit", $params, array(
					'-m' => $message,
				))
				->end();
				//$path = $this->repository;
				//$this->chmod_r($path);

				/*return $this->begin()
					->run("git commit -m  \"Xenium changes\" 2>&1")
					->end();*/
		}

		public function user($email, $name)
		{
			$this->begin()
				->run('git config --global user.email "' . $email . '"')
				->end();
			$this->begin()
				->run('git config --global user.name "' . $name . '"')
				->end();
		}

		public function chmod_r($path)
		{
		    $dir = new DirectoryIterator($path);
		    foreach ($dir as $item) {
		        chmod($item->getPathname(), 0777);
		        if ($item->isDir() && !$item->isDot()) {
		            $this->chmod_r($item->getPathname());
		        }
		    }
		}


		/**
		 * Exists changes?
		 * `git status` + magic
		 * @return bool
		 */
		public function hasChanges()
		{
			$this->begin();
			$lastLine = exec('git status');
			$this->end();
			return (strpos($lastLine, 'nothing to commit')) === FALSE; // FALSE => changes
		}

		/**
		 * Exists changes?
		 * `git status` + magic
		 * @return ordinary status info
		 */
		public function status()
		{
			/*$this->begin();
			$answer = exec('git status');
			$this->end();
			return $answer;*/

			return $this->extractFromCommand('git status', function($value) {
				return trim(substr($value, 1));
			});
		}


		/**
		 * @deprecated
		 */
		public function isChanges()
		{
			return $this->hasChanges();
		}


		/**
		 * Pull changes from a remote
		 * @param  string|NULL
		 * @param  array|NULL
		 * @return self
		 * @throws GitException
		 */
		public function pull($remote = NULL, array $params = NULL)
		{
			if(!is_array($params))
			{
				$params = array();
			}

			return $this->begin()
				->run("git pull $remote", $params)
				->end();
		}


		/**
		 * Push changes to a remote
		 * @param  string|NULL
		 * @param  array|NULL
		 * @return self
		 * @throws GitException
		 */
		public function push($remote = NULL, array $params = NULL)
		{
			if(!is_array($params))
			{
				$params = array();
			}

			return $this->begin()
				->run("git push $remote", $params)
				->end();
		}


		/**
		 * Run fetch command to get latest branches
		 * @param  string|NULL
		 * @param  array|NULL
		 * @return self
		 * @throws GitException
		 */
		public function fetch($remote = NULL, array $params = NULL)
		{
			if(!is_array($params))
			{
				$params = array();
			}

			return $this->begin()
				->run("git fetch $remote", $params)
				->end();
		}


		/**
		 * Get a list of all remotes
		 * @param  bool
		 * @return array of remotes
		 */
		public function getRemoteInfo($return_url = false)
		{
			$add = '';
			if ($return_url) $add = ' -v';
			return $this->extractFromCommand('git remote' . $add, function($value) {
				//return trim(substr($value, 1));
				return trim($value);
			});
		}


		/**
		 * Adds new remote repository
		 * @param  string
		 * @param  string
		 * @param  array|NULL
		 * @return self
		 */
		public function addRemote($name, $url, array $params = NULL)
		{
			return $this->begin()
				->run('git remote add', $params, $name, $url)
				->end();
		}


		/**
		 * Renames remote repository
		 * @param  string
		 * @param  string
		 * @return self
		 */
		public function renameRemote($oldName, $newName)
		{
			return $this->begin()
				->run('git remote rename', $oldName, $newName)
				->end();
		}


		/**
		 * Removes remote repository
		 * @param  string
		 * @return self
		 */
		public function removeRemote($name)
		{
			return $this->begin()
				->run('git remote remove', $name)
				->end();
		}


		/**
		 * Changes remote repository URL
		 * @param  string
		 * @param  string
		 * @param  array|NULL
		 * @return self
		 */
		public function setRemoteUrl($name, $url, array $params = NULL)
		{
			return $this->begin()
				->run('git remote set-url', $params, $name, $url)
				->end();
		}


		/**
		 * @return self
		 */
		protected function begin()
		{
			if($this->cwd === NULL) // TODO: good idea??
			{
				$this->cwd = getcwd();
				chdir($this->repository);
			}

			return $this;
		}


		/**
		 * @return self
		 */
		protected function end()
		{
			if(is_string($this->cwd))
			{
				chdir($this->cwd);
			}

			$this->cwd = NULL;
			return $this;
		}


		/**
		 * @param  string
		 * @param  callback|NULL
		 * @return string[]|NULL
		 */
		protected function extractFromCommand($cmd, $filter = NULL)
		{
			$output = array();
			$exitCode = NULL;

			$this->begin();
			exec("$cmd", $output, $exitCode);
			$this->end();

			if($exitCode !== 0 || !is_array($output))
			{
				throw new GitException("Command $cmd failed.");
			}

			if($filter !== NULL)
			{
				$newArray = array();

				foreach($output as $line)
				{
					$value = $filter($line);

					if($value === FALSE)
					{
						continue;
					}

					$newArray[] = $value;
				}

				$output = $newArray;
			}

			if(!isset($output[0])) // empty array
			{
				return NULL;
			}

			return $output;
		}


		/**
		 * Runs command.
		 * @param  string|array
		 * @return self
		 * @throws Cz\Git\GitException
		 */
		protected function run($cmd/*, $options = NULL*/)
		{
			$args = func_get_args();
			$cmd = self::processCommand($args);
			exec($cmd, $output, $ret);

			/*if($ret !== 0)
			{
				//throw new GitException("Command '$cmd' failed (exit-code $ret).", $ret);
				echo "Command '$cmd' failed (exit-code $ret).";
			}*/
			//echo $ret;
			if($ret !== 0)
			{
				print_r($cmd);
			}

			return $this;
		}


		protected static function processCommand(array $args)
		{
			$cmd = array();

			$programName = array_shift($args);

			foreach($args as $arg)
			{
				if(is_array($arg))
				{
					foreach($arg as $key => $value)
					{
						$_c = '';

						if(is_string($key))
						{
							$_c = "$key ";
						}

						$cmd[] = $_c . escapeshellarg($value);
					}
				}
				elseif(is_scalar($arg) && !is_bool($arg))
				{
					$cmd[] = escapeshellarg($arg);
				}
			}

			return "$programName " . implode(' ', $cmd);
		}


		/**
		 * Init repo in directory
		 * @param  string
		 * @param  array|NULL
		 * @return self
		 * @throws GitException
		 */
		public static function init($directory, array $params = NULL)
		{
			if(is_dir("$directory/.git"))
			{
				throw new GitException("Repo already exists in $directory.");
			}

			if(!is_dir($directory) && !@mkdir($directory, 0777, TRUE)) // intentionally @; not atomic; from Nette FW
			{
				throw new GitException("Unable to create directory '$directory'.");
			}

			$cwd = getcwd();
			chdir($directory);
			exec(self::processCommand(array(
				'git init',
				$params,
				$directory,
			)), $output, $returnCode);

			if($returnCode !== 0)
			{
				throw new GitException("Git init failed (directory $directory).");
			}

			$repo = getcwd();
			chdir($cwd);

			return new static($repo);
		}


		/**
		 * Clones GIT repository from $url into $directory
		 * @param  string
		 * @param  string|NULL
		 * @param  array|NULL
		 * @return self
		 */
		public static function cloneRepository($url, $directory = NULL, array $params = NULL)
		{
			if($directory !== NULL && is_dir("$directory/.git"))
			{
				throw new GitException("Repo already exists in $directory.");
			}

			$cwd = getcwd();

			if($directory === NULL)
			{
				$directory = self::extractRepositoryNameFromUrl($url);
				$directory = "$cwd/$directory";
			}
			elseif(!self::isAbsolute($directory))
			{
				$directory = "$cwd/$directory";
			}

			if ($params === NULL) {
				$params = '-q';
			}

			exec(self::processCommand(array(
				'git clone',
				$params,
				$url,
				$directory
			)), $output, $returnCode);

			if($returnCode !== 0)
			{
				throw new GitException("Git clone failed (directory $directory).");
			}

			return new static($directory);
		}


		/**
		 * @param  string
		 * @param  array|NULL
		 * @return bool
		 */
		public static function isRemoteUrlReadable($url, array $refs = NULL)
		{
			exec(self::processCommand(array(
				'GIT_TERMINAL_PROMPT=0 git ls-remote',
				'--heads',
				'--quiet',
				'--exit-code',
				$url,
				$refs,
			)) . ' 2>&1', $output, $returnCode);

			return $returnCode === 0;
		}


		/**
		 * @param  string  /path/to/repo.git | host.xz:foo/.git | ...
		 * @return string  repo | foo | ...
		 */
		public static function extractRepositoryNameFromUrl($url)
		{
			// /path/to/repo.git => repo
			// host.xz:foo/.git => foo
			$directory = rtrim($url, '/');
			if(substr($directory, -5) === '/.git')
			{
				$directory = substr($directory, 0, -5);
			}

			$directory = basename($directory, '.git');

			if(($pos = strrpos($directory, ':')) !== FALSE)
			{
				$directory = substr($directory, $pos + 1);
			}

			return $directory;
		}


		/**
		 * Is path absolute?
		 * Method from Nette\Utils\FileSystem
		 * @link   https://github.com/nette/nette/blob/master/Nette/Utils/FileSystem.php
		 * @return bool
		 */
		public static function isAbsolute($path)
		{
			return (bool) preg_match('#[/\\\\]|[a-zA-Z]:[/\\\\]|[a-z][a-z0-9+.-]*://#Ai', $path);
		}

		public static function installProject($project_id, $project_name, $project_slug, $repo_url)
		{
			// prepare the folder
			$project_folder = DIR_WWW . 'projects/project_' . $project_slug . '/';
			mkdir($project_folder);

			// initialize Git repository
			$repo = Git::init($project_folder);

			$XeniumAPI = new API('set_session');
			$session_result = json_decode($XeniumAPI->get(), true);
			$repo->user($session_result['user_info']['email'], $session_result['user_info']['username']);

			$repo_url_arr = explode('@', $repo_url);

			$api = new API('git/bitbucket/request_token');
			$api_result = json_decode($api->get(), true);

			$access_token = $api_result['access_token'];

			$new_repo_url = 'https://x-token-auth:' . $access_token . '@' . $repo_url_arr[1];

			// set remote
			$repo->addRemote($project_slug, $new_repo_url);

			// pull project
			$repo->pull($project_slug . ' master');
			// fetch to check for new releases
			$repo->fetch($project_slug, ['-p']);

			// change to the latest version
			$local_branches = $repo->getLocalBranches();
			$current_branch = $repo->getCurrentBranchName();
			$latest_branch = 'master';
			if (count($local_branches) > 1)
			{
				foreach ($local_branches as $branch)
				{
					if ($branch != 'master')
					{
						$latest_branch = $branch;
					}
				}
			}

			// change to latest branch if available
			if ($current_branch != $latest_branch)
			{
				$repo->checkout($latest_branch);
			}

			$project = new Project;
			$new_project = new Xorm($project);  // Xenium xenium {"status":"open"}
	        $last_id = $new_project->www_projects->insert([
	            'project_name' => $project_name,
	            'project_slug' => $project_slug,
	            'project_settings' => '{"status":"open"}',
				'ref_id' => intval($project_id)
	        ]);

			return ['answer' => 'success', 'message' => 'Project <b>' . $project_name . '</b> was installed successfully.'];
		}

		// added for Xenium
		public static function checkForUpdates($repo_folder, $answer = array())
		{
			global $UPDATE_INFO;

			//$repo = new Git(DIR_WWW);
			$repo = new Git($repo_folder);
	        $repo_info = $repo->getRemoteInfo(true);
	        $repo_info_arr = preg_split('/\s+/', $repo_info[0]);
	        $repo_name = trim($repo_info_arr[0]);
	        $repo_url = trim($repo_info_arr[1]);

			$project_slug = 'xenium-cms';

	        // https://x-token-auth:{access_token}@bitbucket.org/user/repo.git
	        // before
	        // https://Danijel-Latin@bitbucket.org/Danijel-Latin/multiproject.git
			if ($repo_folder != DIR_WWW && strpos($repo_url, 'xenium.git') === false)
			{
		        $repo_url_arr = explode('@', $repo_url);

		        $api = new API('git/bitbucket/request_token');
		        $api_result = json_decode($api->get(), true);

		        $access_token = $api_result['access_token'];

		        $new_repo_url = 'https://x-token-auth:' . $access_token . '@' . $repo_url_arr[1];

		        if ($new_repo_url != $repo_url)
		        {
		            $repo->setRemoteUrl($repo_name, $new_repo_url);
		        }

				// get project slug
				$repo_folder_arr = explode('/', $repo_folder);
				$full_project_slug = end($repo_folder_arr);
				$project_slug = str_replace('project_', '', $full_project_slug);

				$project = new Project;
				$project_info = $project->getBySlug(trim($project_slug));
				$git_project_id = $project_info['id'];

				$latest_accessable_version = 'master';
				if (isset($UPDATE_INFO['projects'][$git_project_id]))
				{
					$latest_accessable_version = 'release/' . $UPDATE_INFO['projects'][$git_project_id];
				}
			}
			else
			{
				$latest_accessable_version = 'release/' . $UPDATE_INFO['xenium'];
			}

	        $repo->fetch($repo_name, ['-p']);
	        $branches = $repo->getBranches();
	        $current_branch = $repo->getCurrentBranchName();

	        // get remotes
	        $remote_branches = array();
	        foreach ($branches as $branch)
	        {
	            if (strpos($branch, 'remotes/') !== false) {
	                $branch = str_replace('release/', 'release|', $branch);
	                $branch_arr = explode('/', $branch);
	                $branch_name = str_replace('release|', 'release/', end($branch_arr));
	                $remote_branches[] = $branch_name;
	            }
	        }

	        //$latest_remote_branch = end($remote_branches);
			$latest_remote_branch = $latest_accessable_version;

	        if ($current_branch == 'master' || $current_branch < $latest_remote_branch)
	        {
				$version = str_replace('release/', 'version ', $latest_remote_branch);
				$version_short = str_replace('release/', 'v.', $latest_remote_branch);

				if ($repo_folder == DIR_WWW)
				{
		            $answer['alerts'][] = [
		                'title' => 'Xenium update',
		                'body' => 'There is a new system update, waiting to be installed.',
		                'timeout' => 20000,
		                'buttons' => [
		                    ['name' => 'Cancel', 'class' => 'btn btn-secondary btn-sm', 'func' => 'window.n[\'xenium-update\'].close();'],
		                    ['name' => 'Update to ' . $version_short, 'class' => 'btn btn-primary btn-sm', 'func' => 'window.n[\'xenium-update\'].close();']
		                ]
		            ];
				}
				else
				{
					// check for project changes, offer commit
					if ($repo->hasChanges() && !INTERFACE_TYPE)
					{
						$title = $project_info['project_name'] . ' - changes made';
						$title_id = strtolower(str_replace(' ', '-', $title));

						$answer['alerts'][] = [
			                'title' => $title,
			                'body' => 'Changes were made on your project <strong>' . $project_info['project_name'] . '</strong>.',
							'type' => 'info',
			                'timeout' => 20000,
			                'buttons' => [
			                    ['name' => 'Cancel', 'class' => 'btn btn-secondary btn-sm', 'func' => 'window.n[\'' . $title_id . '\'].close();'],
			                    ['name' => 'Commit ' . $version_short . ' changes', 'class' => 'btn btn-primary btn-sm', 'func' => 'commitChanges(\'' . $project_info['project_name'] . '\', \'' . $repo_folder . '\'); window.n[\'' . $title_id . '\'].close();']
			                ]
			            ];
					}

					$title = $project_info['project_name'] . ' project update';
					$title_id = strtolower(str_replace(' ', '-', $title));

					$answer['alerts'][] = [
		                'title' => $title,
		                'body' => 'There is a new update, waiting to be installed for project <strong>' . $project_info['project_name'] . '</strong>.',
		                'timeout' => 20000,
		                'buttons' => [
		                    ['name' => 'Cancel', 'class' => 'btn btn-secondary btn-sm', 'func' => 'window.n[\'' . $title_id . '\'].close();'],
		                    ['name' => 'Update to ' . $version_short, 'class' => 'btn btn-primary btn-sm', 'func' => 'checkoutProject(\'' . $project_info['project_name'] . '\', \'' . $repo_folder . '\', \'' . $latest_remote_branch . '\'); window.n[\'' . $title_id . '\'].close();']
		                ]
		            ];
				}
	        }

			return $answer;
		}
	}
