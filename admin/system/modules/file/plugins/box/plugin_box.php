<?php

/**
 * @author Danijel
 * @copyright 2014
 */

class plugin_Box
{
    private $content_endpoint = 'https://api.box.com/2.0/';

    private $defaultOptions = array(
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_VERBOSE        => true,
        CURLOPT_HEADER         => true,
        CURLINFO_HEADER_OUT    => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => false,
    );

    public function get_box_token($settings_json= '', $project_id = 0, $code='')
    {
        $current_time =  date('Y-m-d H:i:s');
        //echo 'code ' . $code;
        if (isset($_SESSION['box_session']) && $current_time < $_SESSION['box_session']['expiration'] && !$code)
        {
            $return_token = $_SESSION['box_session']['token'];
        }
        else
        {
            $options = $this->defaultOptions;
            //$options[CURLOPT_HTTPHEADER] = array ("Authorization: Bearer " . $settings['token']);
            $options[CURLOPT_POST] = true;

            $postfields = array();
            //$postfields["filename"] = '@' . $file;
            /*$postfields["grant_type"] = 'authorization_code';
            $postfields["code"] = $code;*/
            $postfields["client_id"] = '64kpxsyi3nvli2jk7xnbbey5oj69gcuf';
            $postfields["client_secret"] = 'JI5bUzIOe8EQgA4qKeOK4v9mrbGT1n3m';

            //$options[CURLOPT_POSTFIELDS] = http_build_query($postfields);


            if ($code)
            {
                //$settings = json_decode($settings_json, true);
                $token_expiration_time = date('Y-m-d H:i:s', strtotime('+1 hour'));
                $refresh_token_expiration_time = date('Y-m-d H:i:s', strtotime('+60 days'));

                $postfields["grant_type"] = 'authorization_code';
                $postfields["code"] = $code;

                $postfields["state"] = urlencode('redirect=http://' . $_SERVER['SERVER_NAME']);

                //$handle = curl_init("https://app.box.com/api/oauth2/token");

                $options[CURLOPT_POSTFIELDS] = $postfields;

                $handle = curl_init("https://app.box.com/api/oauth2/token");

                curl_setopt_array($handle, $options);

                $response = curl_exec($handle);

                curl_close($handle);

                $arr = explode("\n", $response);
                $response_line = end($arr);
                $result = json_decode($response_line, true);

                $result['token_expiration_time'] = $token_expiration_time;
                $result['refresh_token_expiration_time'] = $refresh_token_expiration_time;

                $response_line = json_encode($result);

                $xdb_update = new Xdb;
                $update = $xdb_update->set_table('settings_file')
                                     ->simple_update_fields(array('storage_settings' => $response_line))
                                     ->where(array('project_id' => $project_id))
                                     ->db_update('file', array('project_id'));

                $file = new File;
                $file->update_upload_settings();

                $return_token = $result['access_token'];

                $_SESSION['box_session'] = array('token' => $return_token, 'expiration' => $token_expiration_time);

            }
            else
            {
                $settings = json_decode($settings_json, true);

                if ($settings['token_expiration_time'] > $current_time)
                {
                    $return_token = $settings['access_token'];

                    $_SESSION['box_session'] = array('token' => $settings['access_token'], 'expiration' => $settings['token_expiration_time']);
                }
                else
                {
                    $refresh_token = $settings['refresh_token'];
                    $refresh_token_expiration_time = $settings['refresh_token_expiration_time'];

                    if ($refresh_token_expiration_time > $current_time)
                    {
                        //curl https://app.box.com/api/oauth2/token \ -d 'grant_type=refresh_token&refresh_token={valid refresh token}&client_id={your_client_id}&client_secret={your_client_secret}' \ -X POST

                        $token_expiration_time = date('Y-m-d H:i:s', strtotime('+1 hour'));
                        $refresh_token_expiration_time = date('Y-m-d H:i:s', strtotime('+60 days'));

                        $postfields["grant_type"] = 'refresh_token';
                        $postfields["refresh_token"] = $refresh_token;

                        $postfields["state"] = urlencode('redirect=http://' . $_SERVER['SERVER_NAME']);

                        $options[CURLOPT_POSTFIELDS] = $postfields;

                        $handle = curl_init("https://app.box.com/api/oauth2/token");

                        curl_setopt_array($handle, $options);

                        $response = curl_exec($handle);

                        curl_close($handle);
                        //print_r($response);
                        $arr = explode("\n", $response);
                        $response_line = end($arr);
                        $result = json_decode($response_line, true);

                        $result['token_expiration_time'] = $token_expiration_time;
                        $result['refresh_token_expiration_time'] = $refresh_token_expiration_time;
                        //print_r($result);
                        $response_line = json_encode($result);

                        $xdb_update = new Xdb;
                        $update = $xdb_update->set_table('settings_file')
                                             ->simple_update_fields(array('storage_settings' => $response_line))
                                             ->where(array('project_id' => $project_id))
                                             ->db_update('file', array('project_id'));

                        $file = new File;
                        $file->update_upload_settings();
                        //print_r($result);
                        $return_token = $result['access_token'];

                        $_SESSION['box_session'] = array('token' => $return_token, 'expiration' => $token_expiration_time);
                    }
                    else
                    {
                        //echo $refresh_token_expiration_time . '---' . $current_time;
                    }
                }
            }
        }

        return $return_token;
    }

    public function get_box_folder_items($options, $folder_id)
    {
        $folder_items_options = $options;
        //$folder_items_options[CURLOPT_CUSTOMREQUEST] = 'GET';
        $folder_items_options[CURLOPT_POST] = false;


        // get number of items
        $handle_folder_items_nmb = curl_init($this->content_endpoint . "folders/" . $folder_id . "/items?limit=0");

        curl_setopt_array($handle_folder_items_nmb, $folder_items_options);

        $response_folder_items_nmb = curl_exec($handle_folder_items_nmb);

        curl_close($handle_folder_items_nmb);

        //echo $response_folder_items;

        if (is_string($response_folder_items_nmb))
        {
            $folder_arr_nmb = explode("\n", $response_folder_items_nmb);
            $folder_response_line_nmb = end($folder_arr_nmb);
            $folder_items_nmb = json_decode($folder_response_line_nmb, true);
        }

        $items_count = $folder_items_nmb['total_count'];

        $offset = 0;
        $limit = 1000;

        $file_exists_check = false;
        $return_file_array = array();

        $result_array = array();

        for ($i = 1; $i <= $items_count; $i += 1000) {

            $handle_folder_items = curl_init($this->content_endpoint . "folders/" . $folder_id . "/items?limit=" . $limit . "&offset=" . $offset);

            curl_setopt_array($handle_folder_items, $folder_items_options);

            $response_folder_items = curl_exec($handle_folder_items);

            curl_close($handle_folder_items);



            //echo $response_folder_items;

            if (is_string($response_folder_items))
            {
                $folder_arr = explode("\n", $response_folder_items);
                $folder_response_line = end($folder_arr);
                $folder_items = json_decode($folder_response_line, true);
            }

            //print_r($folder_items);

            $result_array = array_merge($result_array, $folder_items['entries']);
            /*
            foreach ($folder_items['entries'] as $item)
            {
                if ($fileName == $item['name']){
                    //echo 'file exists!';
                    $file_exists_check = true;
                    $file_id = $item['id'];
                    $return_file_array = $item;
                    break 2;
                }
            }
            */
            $offset = $offset + 1000;
            $limit = $limit + 1000;
        }

        //return $return_file_array;
        return $result_array;
    }

    public function check_file_exists_box($options, $folder_id, $fileName)
    {
        $folder_items_options = $options;
        //$folder_items_options[CURLOPT_CUSTOMREQUEST] = 'GET';
        $folder_items_options[CURLOPT_POST] = false;


        // get number of items
        $handle_folder_items_nmb = curl_init($this->content_endpoint . "folders/" . $folder_id . "/items?limit=0");

        curl_setopt_array($handle_folder_items_nmb, $folder_items_options);

        $response_folder_items_nmb = curl_exec($handle_folder_items_nmb);

        curl_close($handle_folder_items_nmb);

        //echo $response_folder_items;

        if (is_string($response_folder_items_nmb))
        {
            $folder_arr_nmb = explode("\n", $response_folder_items_nmb);
            $folder_response_line_nmb = end($folder_arr_nmb);
            $folder_items_nmb = json_decode($folder_response_line_nmb, true);
        }

        $items_count = $folder_items_nmb['total_count'];

        $offset = 0;
        $limit = 1000;

        $file_exists_check = false;
        $return_file_array = array();

        for ($i = 1; $i <= $items_count; $i += 1000) {

            $handle_folder_items = curl_init($this->content_endpoint . "folders/" . $folder_id . "/items?limit=" . $limit . "&offset=" . $offset);

            curl_setopt_array($handle_folder_items, $folder_items_options);

            $response_folder_items = curl_exec($handle_folder_items);

            curl_close($handle_folder_items);



            //echo $response_folder_items;

            if (is_string($response_folder_items))
            {
                $folder_arr = explode("\n", $response_folder_items);
                $folder_response_line = end($folder_arr);
                $folder_items = json_decode($folder_response_line, true);
            }

            //print_r($folder_items);

            foreach ($folder_items['entries'] as $item)
            {
                if ($fileName == $item['name']){
                    //echo 'file exists!';
                    $file_exists_check = true;
                    $file_id = $item['id'];
                    $return_file_array = $item;
                    break 2;
                }
            }

            $offset = $offset + 1000;
            $limit = $limit + 1000;
        }

        return $return_file_array;
    }

    public function upload_box($settings_json)
    {
        global $project_id;
        //define(CONTENT_ENDPOINT, 'https://api.box.com/2.0/');

        $token = $this->get_box_token($settings_json, $project_id);


        define(DIRECTORY_SEPARATOR, '/');

        if ($project_id == 0)
        {
            $parent_folder = 'shared';
        }
        else
        {
            $project_info = Projects::get_project_info_by_id($project_id);
            $parent_folder = $project_info['project_slug'];
        }

        $settings = json_decode($settings_json, true);

        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';
        $fileExtension = strtolower(substr(strrchr($fileName,"."),1));
        $fileTmpName = $_FILES['file']['tmp_name'];

        $upload_manager = new Upload;
        $fileType = $upload_manager->get_file_info($fileTmpName)->return_type();
        $fileYear = date('Y');
        $fileMonth = date('m');
        //$targetDir = $targetDir . DIRECTORY_SEPARATOR . $upload_manager->get_file_info($fileTmpName)->return_type() . DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m');

        $options = $this->defaultOptions;
        $options[CURLOPT_HTTPHEADER] = array ("Authorization: Bearer " . $token);
        $options[CURLOPT_POST] = true;



        // GET MAIN FOLDER INFO
        /*
        $options_folder = $options;
        $options_folder[CURLOPT_POST] = false;
        $handle_folder = curl_init($this->content_endpoint . "folders/0");

        curl_setopt_array($handle_folder, $options_folder);

        $response_folder = curl_exec($handle_folder);

        if(curl_errno($handle_folder)) {
            //echo 'Curl error: ' . curl_error($handle_folder);
        }

        curl_close($handle_folder);

        print_r($response_folder);
        */
        // END GET MAIN FOLDER INFO


        // CHECK / CREATE PROJECT FOLDER

        $folder_postfields = array();
        $folder_postfields = '{"name":"' . $parent_folder . '", "parent": {"id": "0"}}';
        $options_folder = $options;
        $options_folder[CURLOPT_POSTFIELDS] = $folder_postfields;

        $handle_folder = curl_init($this->content_endpoint . "folders");

        curl_setopt_array($handle_folder, $options_folder);

        $response_folder = curl_exec($handle_folder);

        curl_close($handle_folder);

        //print_r($response_folder);
        if (is_string($response_folder))
        {
            $folder_arr = explode("\n", $response_folder);
            $folder_response_line = end($folder_arr);
            $folder_result = json_decode($folder_response_line, true);
        }

        //print_r($folder_result);

        if (isset($folder_result['code']) && $folder_result['code'] == 'item_name_in_use')
        {
            $project_folder_id = $folder_result['context_info']['conflicts'][0]['id'];
        }
        else
        {
            $project_folder_id = $folder_result['id'];
        }

        // END CHECK / CREATE PROJECT FOLDER


        // CHECK / CREATE TYPE FOLDER

        $type_folder_postfields = array();
        $type_folder_postfields = '{"name":"' . $fileType . '", "parent": {"id": "' . $project_folder_id . '"}}';
        $type_options_folder = $options;
        $type_options_folder[CURLOPT_POSTFIELDS] = $type_folder_postfields;


        $handle_type_folder = curl_init($this->content_endpoint . "folders");

        curl_setopt_array($handle_type_folder, $type_options_folder);

        $response_type_folder = curl_exec($handle_type_folder);

        curl_close($handle_type_folder);

        //print_r($response_type_folder);
        if (is_string($response_type_folder))
        {
            $type_folder_arr = explode("\n", $response_type_folder);
            $type_folder_response_line = end($type_folder_arr);
            $type_folder_result = json_decode($type_folder_response_line, true);
        }

        //print_r($type_folder_result);

        if (isset($type_folder_result['code']) && $type_folder_result['code'] == 'item_name_in_use')
        {
            $type_folder_id = $type_folder_result['context_info']['conflicts'][0]['id'];
        }
        else
        {
            $type_folder_id = $type_folder_result['id'];
        }

        // END CHECK / CREATE TYPE FOLDER



        // GET FOLDERS ITEMS / CHECK IF FILE EXISTS

        $fileName = iconv("UTF-8", "ASCII", str_replace(' ', '-', strtolower($fileName)));

        $file_array = $this->check_file_exists_box($options, $type_folder_id, $fileName);
        $file_exists_check = false;
        if (isset($file_array['id']))
        {
            $file_exists_check = true;
            $file_id = $file_array['id'];
        }
        /*
        $folder_items_options = $options;
        //$folder_items_options[CURLOPT_CUSTOMREQUEST] = 'GET';
        $folder_items_options[CURLOPT_POST] = false;

        $handle_folder_items = curl_init($this->content_endpoint . "folders/" . $type_folder_id . "/items?limit=1000");

        curl_setopt_array($handle_folder_items, $folder_items_options);

        $response_folder_items = curl_exec($handle_folder_items);

        curl_close($handle_folder_items);

        //echo $response_folder_items;

        if (is_string($response_folder_items))
        {
            $folder_arr = explode("\n", $response_folder_items);
            $folder_response_line = end($folder_arr);
            $folder_items = json_decode($folder_response_line, true);
        }

        //print_r($folder_items);
        $file_exists_check = false;
        foreach ($folder_items['entries'] as $item)
        {
            if ($fileName == $item['name']){
                //echo 'file exists!';
                $file_exists_check = true;
                $file_id = $item['id'];
                break;
            }
        }
        */
        // END GET FOLDERS ITEMS / CHECK IF FILE EXISTS



        // CHECK FILE EXISTS

        //$file_postfields = array();
        //$file_postfields["attributes"] = '{"name":"' . $fileName . '", "parent": {"id": "' . $type_folder_id . '"}, "size": "0"}';
        /*
        $fields = array();
        $fields['name'] = $fileName;
        $fields['parent'] = array('id' => $type_folder_id);
        $fields['size'] = filesize($fileTmpName);

        //$file_postfields = '{"name": "' . $fileName . '", "parent": {"id": "' . $type_folder_id . '"}, "size": "' . filesize($fileTmpName) . '"}'; echo $file_postfields;
        $file_postfields = json_encode($fields); //echo $file_postfields;
        $options_file = $options;
        $options_file[CURLOPT_POSTFIELDS] = $file_postfields;

        $options_file[CURLOPT_CUSTOMREQUEST] = 'OPTIONS';
        //$options_file[CURLOPT_POST] = false;

        //echo $this->content_endpoint . "files/content";
        $upload_endpoint = 'https://upload.box.com/api/2.0/';
        //$handle_file = curl_init($this->content_endpoint . "files/content");
        $handle_file = curl_init($upload_endpoint . "files/content");

        curl_setopt_array($handle_file, $options_file);

        $response_file = curl_exec($handle_file);

        curl_close($handle_file);

        print_r($response_file);
        */
        // END CHECK FILE EXISTS


        if (!$file_exists_check)
        {

        // FILE UPLOAD
        $postfields = array();
        //$postfields["filename"] = '@' . $file;

        if (!class_exists('CurlFile'))
        {
            $postfields["filename"] = '@' . $fileTmpName;
        }
        else
        {
            //$postfields['file'] = new CurlFile($_FILES['Filedata']['tmp_name'],'file/exgpd',$RealTitleID);
            $postfields['file'] = new CurlFile($fileTmpName,'file/exgpd',$fileName);
        }

        $postfields["attributes"] = '{"name" : "' . $fileName . '", "parent":{"id":"' . $type_folder_id . '"}}';
        $postfields["parent_id"] = $type_folder_id;

        $options[CURLOPT_POSTFIELDS] = $postfields;
        //$options[CURLOPT_SAFE_UPLOAD] = false;
        //$handle = curl_init(File::CONTENT_ENDPOINT . "files/content");

        // upload content endpoint
        $upload_endpoint = 'https://upload.box.com/api/2.0/';
        //$handle = curl_init($this->content_endpoint . "files/content");
        $handle = curl_init($upload_endpoint . "files/content");

        curl_setopt_array($handle, $options);

        $response = curl_exec($handle);

        curl_close($handle);

        //echo $response;

        if (is_string($response)) {
            //$response = $this->parse($response);
            $response = $response;

            $arr = explode("\n", $response);
            $response_line = end($arr);
            $result = json_decode($response_line, true);
        }

        //return $response;
        //print_r($result);

        $file_exists = false;
        if (isset($result['code']) && $result['code'] == 'item_name_in_use')
        {
            $file_exists = true;
        }

        // get the file id
        if (isset($result['entries'][0]['id']))
        {
            $file_id = $result['entries'][0]['id'];
        }
        else
        {
            $file_id = $result['context_info']['conflicts']['id'];
        }
        //echo $file_id;

        // END FILE UPLOAD


        // GET FILE
        /*
        $options_file = $options;
        $options_file[CURLOPT_CUSTOMREQUEST] = 'GET';
        $options_file[CURLOPT_POST] = false;

        $handle_file = curl_init($this->content_endpoint . "files/" . $file_id . "/content");

        curl_setopt_array($handle_file, $options_file);

        $response_file = curl_exec($handle_file);

        curl_close($handle_file);

        if (is_string($response_file)) {
            $arr_file = explode("\n", $response_file);

            foreach ($arr_file as $line)
            {
                if (strpos($line,'Location:') !== false)
                {
                    $get_contents = str_replace('Location: ', '', $line);
                    break;
                }
            }
        }

        $return_fields = array();
        $return_fields['fileUrl'] = $get_contents;

        echo json_encode($return_fields);
        */
        // END GET FILE


        // CREATE SHARED URL

        //curl https://api.box.com/2.0/files/FILE_ID \
        //-H "Authorization: Bearer ACCESS_TOKEN" \
        //-d '{"shared_link": {"access": "open"}}' \
        //-X PUT

        //$share_postfields = array();

        if (!$file_exists)
        {
            $share_postfields = '{"shared_link": {"access": "open", "can_download": "true"}}';
            $options_share = $options;
            $options_share[CURLOPT_CUSTOMREQUEST] = 'PUT';
            $options_share[CURLOPT_POSTFIELDS] = $share_postfields;
            $options_share[CURLOPT_POST] = false;

            $handle_share = curl_init($this->content_endpoint . "files/" . $file_id);

            curl_setopt_array($handle_share, $options_share);

            $response_share = curl_exec($handle_share);

            curl_close($handle_share);

            if (is_string($response_share)) {
                $arr_share = explode("\n", $response_share);
                $response_line_share = end($arr_share);
                $result_share = json_decode($response_line_share, true);
            }
        }

        //print_r($result_share);

        }

        // RETURN VALUE

        $return_fields = array();
        //$return_fields['fileUrl'] = $result_share['shared_link']['download_url'];
        $return_fields['id'] = $file_id;
        $return_fields['ext'] = $fileExtension;
        $return_fields['provider'] = 'box';
        $return_fields['fileUrl'] = '/images/cloudstorage/box/' . $file_id . '.' . $fileExtension;
        //$return_fields['fileUrl'] = '/admin/system/on.the.fly/image.cloudstorage.php?provider=box&id=' . $file_id . '&ext=' . $fileExtension;

        echo json_encode($return_fields);

        //print_r($response_share);

        // END CREATE SHARED URL
    }

    public function transfer_box($settings_json, $target_folder, $transfer_file, $transfer_file_type)
    {
        global $project_id, $target_folder_ids, $target_folder_items, $box_token, $parent_folder;
        //define(CONTENT_ENDPOINT, 'https://api.box.com/2.0/');



        if (!isset($box_token))
        {
            $token = $this->get_box_token($settings_json, $project_id);
        }
        else
        {
            $token = $box_token;
        }


        //if (!defined('DIRECTORY_SEPARATOR')) define(DIRECTORY_SEPARATOR, '/');

        if (!isset($parent_folder))
        {
            if ($project_id == 0)
            {
                $parent_folder = 'shared';
            }
            else
            {
                $project_info = Projects::get_project_info_by_id($project_id);
                $parent_folder = $project_info['project_slug'];
            }
        }
        //$startTime = microtime(true);
        //$settings = json_decode($settings_json, true);

        /*$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';
        $fileExtension = strtolower(substr(strrchr($fileName,"."),1));
        $fileTmpName = $_FILES['file']['tmp_name'];
        */
        //$upload_manager = new Upload;

        //$fileType = $upload_manager->get_file_info($fileTmpName)->return_type();
        $fileName = end(explode('/', $transfer_file));
        $fileExtension = strtolower(substr(strrchr($transfer_file,"."),1));
        //$fileExtension = strtolower(end(explode('.', $transfer_file)));
        //$fileExtension = 'jpg';

        $fileType = $transfer_file_type;

        //$fileYear = date('Y');
        //$fileMonth = date('m');
        //$targetDir = $targetDir . DIRECTORY_SEPARATOR . $upload_manager->get_file_info($fileTmpName)->return_type() . DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m');

        $options = $this->defaultOptions;
        $options[CURLOPT_HTTPHEADER] = array ("Authorization: Bearer " . $token);
        $options[CURLOPT_POST] = true;
        //echo "Elapsed time is: ". (microtime(true) - $startTime) ." seconds<br>";



        if (!isset($target_folder_ids[$target_folder]))
        {
            // CHECK / CREATE PROJECT FOLDER

            $folder_postfields = array();
            $folder_postfields = '{"name":"' . $parent_folder . '", "parent": {"id": "0"}}';
            $options_folder = $options;
            $options_folder[CURLOPT_POSTFIELDS] = $folder_postfields;

            $handle_folder = curl_init($this->content_endpoint . "folders");

            curl_setopt_array($handle_folder, $options_folder);

            $response_folder = curl_exec($handle_folder);

            curl_close($handle_folder);

            //print_r($response_folder);
            if (is_string($response_folder))
            {
                $folder_arr = explode("\n", $response_folder);
                $folder_response_line = end($folder_arr);
                $folder_result = json_decode($folder_response_line, true);
            }

            //print_r($folder_result);

            if (isset($folder_result['code']) && $folder_result['code'] == 'item_name_in_use')
            {
                $project_folder_id = $folder_result['context_info']['conflicts'][0]['id'];
            }
            else
            {
                $project_folder_id = $folder_result['id'];
            }

            // END CHECK / CREATE PROJECT FOLDER


            // CHECK / CREATE TYPE FOLDER

            $type_folder_postfields = array();
            $type_folder_postfields = '{"name":"' . $fileType . '", "parent": {"id": "' . $project_folder_id . '"}}';
            $type_options_folder = $options;
            $type_options_folder[CURLOPT_POSTFIELDS] = $type_folder_postfields;


            $handle_type_folder = curl_init($this->content_endpoint . "folders");

            curl_setopt_array($handle_type_folder, $type_options_folder);

            $response_type_folder = curl_exec($handle_type_folder);

            curl_close($handle_type_folder);

            //print_r($response_folder);
            if (is_string($response_type_folder))
            {
                $type_folder_arr = explode("\n", $response_type_folder);
                $type_folder_response_line = end($type_folder_arr);
                $type_folder_result = json_decode($type_folder_response_line, true);
            }

            //print_r($type_folder_result);

            if (isset($type_folder_result['code']) && $type_folder_result['code'] == 'item_name_in_use')
            {
                $type_folder_id = $type_folder_result['context_info']['conflicts'][0]['id'];
            }
            else
            {
                $type_folder_id = $type_folder_result['id'];
            }

            // END CHECK / CREATE TYPE FOLDER



            // CHECK / CREATE TARGET FOLDER

            $type_folder_postfields = array();
            $type_folder_postfields = '{"name":"' . $target_folder . '", "parent": {"id": "' . $type_folder_id . '"}}';
            $type_options_folder = $options;
            $type_options_folder[CURLOPT_POSTFIELDS] = $type_folder_postfields;


            $handle_type_folder = curl_init($this->content_endpoint . "folders");

            curl_setopt_array($handle_type_folder, $type_options_folder);

            $response_type_folder = curl_exec($handle_type_folder);

            curl_close($handle_type_folder);

            //print_r($response_folder);
            if (is_string($response_type_folder))
            {
                $type_folder_arr = explode("\n", $response_type_folder);
                $type_folder_response_line = end($type_folder_arr);
                $type_folder_result = json_decode($type_folder_response_line, true);
            }

            //print_r($type_folder_result);

            if (isset($type_folder_result['code']) && $type_folder_result['code'] == 'item_name_in_use')
            {
                $type_folder_id = $type_folder_result['context_info']['conflicts'][0]['id'];
            }
            else
            {
                $type_folder_id = $type_folder_result['id'];
            }

            // END CHECK / CREATE TARGET FOLDER

            // assign target folder id to globals
            $target_folder_ids[$target_folder] = $type_folder_id;

        }
        else
        {
            $type_folder_id = $target_folder_ids[$target_folder];
        }



        if (!isset($target_folder_items[$target_folder]))
        {
            $folder_containing_items = $this->get_box_folder_items($options, $type_folder_id);
            $target_folder_items[$target_folder] = $folder_containing_items;
        }
        else
        {
            $folder_containing_items = $target_folder_items[$target_folder];
        }
        // check if file exists
        $file_exists_check = false;

        $fileName = iconv("UTF-8", "ASCII", str_replace(' ', '-', strtolower($fileName)));

        foreach ($folder_containing_items as $item)
        {
            if ($fileName == $item['name']){
                //echo 'file exists!';
                $file_exists_check = true;
                $file_id = $item['id'];
                $return_file_array = $item;
                break;
            }
        }



        if (!$file_exists_check)
        {

            // FILE UPLOAD
            $postfields = array();
            //$postfields["filename"] = '@' . $file;
            $postfields["filename"] = '@' . $transfer_file;

            if (!class_exists('CurlFile'))
            {
                $postfields["filename"] = '@' . $transfer_file;
            }
            else
            {
                //$postfields['file'] = new CurlFile($_FILES['Filedata']['tmp_name'],'file/exgpd',$RealTitleID);
                $postfields['file'] = new CurlFile($transfer_file,'file/exgpd',$fileName);
            }

            //$fileName = end(explode('/', $transfer_file));
            $postfields["attributes"] = '{"name" : "' . $fileName . '", "parent":{"id":"' . $type_folder_id . '"}}';
            $postfields["parent_id"] = $type_folder_id;

            $options[CURLOPT_POSTFIELDS] = $postfields;
            //$handle = curl_init(File::CONTENT_ENDPOINT . "files/content");

            // upload content endpoint
            $upload_endpoint = 'https://upload.box.com/api/2.0/';
            //$handle = curl_init($this->content_endpoint . "files/content");
            $handle = curl_init($upload_endpoint . "files/content");

            curl_setopt_array($handle, $options);

            $response = curl_exec($handle);

            curl_close($handle);

            if (is_string($response)) {
                //$response = $this->parse($response);
                $response = $response;

                $arr = explode("\n", $response);
                $response_line = end($arr);
                $result = json_decode($response_line, true);
            }

            //return $response;
            //print_r($result);

            $file_exists = false;
            if (isset($result['code']) && $result['code'] == 'item_name_in_use')
            {
                $file_exists = true;
            }

            // get the file id
            if (isset($result['entries'][0]['id']))
            {
                $file_id = $result['entries'][0]['id'];
            }
            else
            {
                $file_id = $result['context_info']['conflicts']['id'];
            }
            //echo $file_id;

            // END FILE UPLOAD



            if (!$file_exists)
            {
                $share_postfields = '{"shared_link": {"access": "open", "can_download": "true"}}';
                $options_share = $options;
                $options_share[CURLOPT_CUSTOMREQUEST] = 'PUT';
                $options_share[CURLOPT_POSTFIELDS] = $share_postfields;
                $options_share[CURLOPT_POST] = false;

                $handle_share = curl_init($this->content_endpoint . "files/" . $file_id);

                curl_setopt_array($handle_share, $options_share);

                $response_share = curl_exec($handle_share);

                curl_close($handle_share);

                if (is_string($response_share)) {
                    $arr_share = explode("\n", $response_share);
                    $response_line_share = end($arr_share);
                    $result_share = json_decode($response_line_share, true);
                }
            }

            //print_r($result_share);
        }

        // RETURN VALUE

        $return_fields = array();
        //$return_fields['fileUrl'] = $result_share['shared_link']['download_url'];
        $return_fields['id'] = $file_id;
        $return_fields['ext'] = $fileExtension;
        $return_fields['provider'] = 'box';
        $return_fields['fileUrl'] = '/images/cloudstorage/box/' . $file_id . '.' . $fileExtension;
        //$return_fields['fileUrl'] = '/admin/system/on.the.fly/image.cloudstorage.php?provider=box&id=' . $file_id . '&ext=' . $fileExtension;



        return $return_fields;

        //print_r($response_share);

        // END CREATE SHARED URL

    }

    public function box_get_file_url($file_id)
    {
        global $project_id;

        if (!isset($project_id) || !$project_id)
        {
            if (isset($_COOKIE['project_id']))
            {
                $project_id = $_COOKIE['project_id'];
            }
            else
            {
                $project_id = 0;
            }
        }

        $file = new File;
        $settings = $file->get_upload_settings($project_id);
        $token = $this->get_box_token($settings['storage_settings'], $project_id);

        // GET FILE

        $options = $this->defaultOptions;
        $options[CURLOPT_HTTPHEADER] = array ("Authorization: Bearer " . $token);

        $options_file = $options;
        $options_file[CURLOPT_CUSTOMREQUEST] = 'GET';
        $options_file[CURLOPT_POST] = false;

        $handle_file = curl_init($this->content_endpoint . "files/" . $file_id . "/content");

        curl_setopt_array($handle_file, $options_file);

        $response_file = curl_exec($handle_file);

        curl_close($handle_file);

        if (is_string($response_file)) {
            $arr_file = explode("\n", $response_file);

            foreach ($arr_file as $line)
            {
                if (strpos($line,'Location:') !== false)
                {
                    $get_contents = str_replace('Location: ', '', $line);
                    break;
                }
            }
        }
        /*
        $return_fields = array();
        $return_fields['fileUrl'] = $get_contents;

        echo json_encode($return_fields);
        */

        return $get_contents;

        // END GET FILE
    }

    public function box_get_file_contents($file_id)
    {
        global $project_id;

        if (!$project_id)
        {
            if (isset($_COOKIE['project_id']))
            {
                $project_id = $_COOKIE['project_id'];
            }
            else
            {
                $project_id = 0;
            }
        }

        //define(CONTENT_ENDPOINT, 'https://api.box.com/2.0/');
        //echo 'project_id ' . $project_id;
        $file = new File;
        $settings = $file->get_upload_settings($project_id);
        $accessToken = $this->get_box_token($settings['storage_settings'], $project_id);
        //echo $accessToken;


        //$fileId = 12673472942;
        //$accessToken = 'YOURACCESSTOKENGOESHERE';
        /*
        $sBox = stream_context_create(array(
            'http'=> array(
                'header' => "Authorization: Bearer " . $accessToken . "\r\n"
            )
        ));

        print_r($sBox);
        $fileData = file_get_contents(
            "https://api.box.com/2.0/files/" . $file_id . "/content",
            false,
            $sBox
        );
        echo $fileData;*/
        //var_dump($fileData);

        $options = $this->defaultOptions;
        $options[CURLOPT_HTTPHEADER] = array ("Authorization: Bearer " . $accessToken);
        $options[CURLOPT_POST] = false;

        $postfields = array();
            //$postfields["filename"] = '@' . $file;
            /*$postfields["filename"] = '@' . $transfer_file;

            if (!class_exists('CurlFile'))
            {
                $postfields["filename"] = '@' . $transfer_file;
            }
            else
            {
                //$postfields['file'] = new CurlFile($_FILES['Filedata']['tmp_name'],'file/exgpd',$RealTitleID);
                $postfields['file'] = new CurlFile($transfer_file,'file/exgpd',$fileName);
            }

            //$fileName = end(explode('/', $transfer_file));
            $postfields["attributes"] = '{"name" : "' . $fileName . '", "parent":{"id":"' . $type_folder_id . '"}}';
            $postfields["parent_id"] = $type_folder_id;
    */
            //$options[CURLOPT_POSTFIELDS] = $postfields;
            //$handle = curl_init(File::CONTENT_ENDPOINT . "files/content");

            /*
            // upload content endpoint
            $upload_endpoint = 'https://api.box.com/2.0/';
            //$handle = curl_init($this->content_endpoint . "files/content");
            $handle = curl_init($upload_endpoint . "files/" . $file_id . "/content");

            curl_setopt_array($handle, $options);

            $response = curl_exec($handle);
            echo $response;
            curl_close($handle);
            */

            $url = 'https://api.box.com/2.0/files/' . $file_id . '/content';

            $header = array('Authorization: Bearer '.$accessToken);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            $returndata = curl_exec($curl);

            return $returndata;
    }
}

?>
