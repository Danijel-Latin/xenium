<?php

/**
 * @author Danijel
 * @copyright 2013
 *
 *
 */

class File extends Module
{
    protected $file;
    protected $write_string;

    public function __construct($get_file = '')
    {
        if ($get_file)
        {
            $this->file = $get_file;
        }

        $this->loadPlugins();
    }

    public function set_writable()
    {
        if (!file_exists(dirname($this->file)))
        {
            mkdir(dirname($this->file));
        }
        chmod(dirname($this->file), 0777);
        if (file_exists($this->file))
        {
            chmod($this->file, 0777);
        }
        return $this;
    }

    public function set_unwritable()
    {
        chmod(dirname($this->file), 0755);
        chmod($this->file, 0755);
        return $this;
    }

    public function write_to_file($string)
    {
        $this->write_string = $string;

        $cf = fopen($this->file, 'w+') or die("can't open file");
        fwrite($cf, $this->write_string);
        fclose($cf);

        return $this;
    }

    public function create_dir($dir)
    {
        if (!is_dir($dir))
        {
            mkdir($dir, 0777, true);
        }
    }

    public function get_upload_settings($pro_id = '')
    {
        global $salt;

        if ($pro_id || $pro_id == 0)
        {
            $project_id = $pro_id;
        }

        $memcache_check = new xMemcache();
        $memcache_file_settings = $memcache_check->get_memcache(sha1($salt . '_file_settings'));

        if ($memcache_file_settings && is_array($memcache_file_settings))
        {
            $return_array = $memcache_file_settings;
        }
        else
        {
            $xdb_setting = new Xdb;
            $xdb_setting_rows = $xdb_setting->set_table('settings_file')
                                            ->db_select(false);

            $return_array = array();

            foreach ($xdb_setting_rows as $setting_value)
            {
                $return_array[$setting_value['project_id']] = array('id' => $setting_value['id'], 'project_id' => $setting_value['project_id'], 'storage_handler' => $setting_value['storage_handler'], 'storage_settings' => $setting_value['storage_settings']);
            }

            $memcache_check->set_memcache(sha1($salt . '_file_settings'), $return_array);

        }

        if (isset($project_id) && isset($return_array[$project_id]))
        {
            return $return_array[$project_id];
        }
        else
        {
            return $return_array[0];
        }

    }

    public function update_upload_settings()
    {
        global $salt;

        $xdb_setting = new Xdb;
        $xdb_setting_rows = $xdb_setting->set_table('settings_file')
                                        ->db_select(false);

        $return_array = array();

        foreach ($xdb_setting_rows as $setting_value)
        {
            $return_array[$setting_value['project_id']] = array('id' => $setting_value['id'], 'project_id' => $setting_value['project_id'], 'storage_handler' => $setting_value['storage_handler'], 'storage_settings' => $setting_value['storage_settings']);
        }

        $memcache_check = new xMemcache();
        $memcache_file_settings = $memcache_check->get_memcache(sha1($salt . '_file_settings'));

        if ($memcache_file_settings)
        {
            $memcache_check->replace_memcache(sha1($salt . '_file_settings'), $return_array);
        }
        else
        {
            $memcache_check->set_memcache(sha1($salt . '_file_settings'), $return_array);
        }
    }

    public function upload()
    {
        global $project_id;

        if (isset($_COOKIE['project_id']))
        {
            $project_id = $_COOKIE['project_id'];
        }

        $settings = $this->get_upload_settings($project_id);

        $method_name = 'upload_' . $settings['storage_handler'];
        $params = array($settings['storage_settings']);

        //return call_user_func_array(array($this, $method_name), $params);
        return call_user_func_array(array('File', $method_name), $params);
    }
}

?>
