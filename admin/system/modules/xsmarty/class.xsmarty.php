<?php

class xSmarty
{
    protected $smarty;

    public static function init($caching = true, $compile_check = false)
    {
        global $_PROJECT;

        // create the $_PROJECT global
        $project = new Project;
        $project->getByDomain();

        $cache = new Cache();
        $smarty_cache_key = sha1(SALT . 'smarty' . $_PROJECT['project_slug']);

        // include Smarty
        require (DIR_MODULES . 'xsmarty/smarty/Smarty.class.php');
        $smarty = new Smarty;

        $smarty_obj = new self;

        if (!$cache->exists($smarty_cache_key))
        {
            $project_id = $_PROJECT['id'];
            $project_slug = $_PROJECT['project_slug'];
            $project_name = $_PROJECT['project_name'];
            $project_theme = Project::getTheme();

            $project_settings = json_decode($_PROJECT['project_settings'], true);

            //$smarty->caching_type = 'xdb'; // change to Xorm

            $smarty->template_dir = 'projects/project_' .$project_slug . '/themes/' . $project_theme;
            $smarty->compile_dir = 'projects/project_' .$project_slug . '/themes/' . $project_theme . '/compile';
            $smarty->cache_dir = 'projects/project_' .$project_slug . '/themes/' . $project_theme . '/cache';
            $smarty->config_dir = 'projects/project_' .$project_slug . '/themes/' . $project_theme . '/configs';
            $smarty->caching = $caching;
            $smarty->compile_check = $compile_check;
            $smarty->cache_lifetime = -1;
            $smarty->assign('project_slug', $project_slug);
            $smarty->assign('project_theme', $project_theme);
            $smarty->assign('project_folder', '/projects/project_' .$project_theme);
            $smarty->assign('project_theme_folder', '/projects/project_' . $project_slug . '/themes/' . $project_theme);

            $smarty_obj->smarty = $smarty;

            $cache->set($smarty_cache_key, serialize($smarty));
        }
        else
        {
            $smarty_obj->smarty = unserialize($cache->get($smarty_cache_key));
        }

        return $smarty_obj;
    }

    public static function display()
    {
        $smarty_obj = xSmarty::init();


        $display_page = 'index.tpl';
        $cache_id = sha1('neki');
        $smarty_obj->smarty->display($display_page, $cache_id);

        $debugger = new Xdebugger;
        $debugger->debug();
    }
}
?>
