<?php

/**
 * @author Danijel
 * @copyright 2015
 */

/*
require_once 'libs/Smarty.class.php';
$smarty = new Smarty();
$smarty->caching_type = 'xdb';
*/

/**
 * Xdb CacheResource
 *
 * CacheResource Implementation based on the Custom API to use
 * Xdb as the storage resource for Smarty's output caching.
 *
 * Table definition:
 * <pre>CREATE TABLE IF NOT EXISTS `cache_smarty_output` (
 *   `id` CHAR(40) NOT NULL COMMENT 'sha1 hash',
 *   `name` VARCHAR(250) NOT NULL,
 *   `cache_id` VARCHAR(250) NULL DEFAULT NULL,
 *   `compile_id` VARCHAR(250) NULL DEFAULT NULL,
 *   `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 *   `content` LONGTEXT NOT NULL,
 *   PRIMARY KEY (`id`),
 *   INDEX(`name`),
 *   INDEX(`cache_id`),
 *   INDEX(`compile_id`),
 *   INDEX(`modified`)
 * ) ENGINE = InnoDB;</pre>
 *
 * @package CacheResource-examples
 * @author Danijel Latin
 */

/*
CREATE TABLE IF NOT EXISTS `cache_smarty_output` (
  `id` char(40) COLLATE utf8_unicode_ci NOT NULL COMMENT 'sha1 hash',
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `cache_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `compile_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
*/

class Smarty_CacheResource_Xdb extends Smarty_CacheResource_Custom {
    // PDO instance
    protected $db;
    protected $fetch;
    protected $fetchTimestamp;
    protected $save;
    
    protected $cache_table;
    protected $cache_fields;
    
    public function __construct() {
        
        $this->cache_table = 'cache_smarty_output';
        
        $this->cache_fields = 
        array(
        'static_fields' => 
            array(
                'id' => array('value' => '', 'field_type' => 'char(40) COLLATE utf8_unicode_ci NOT NULL', 'index' => true, 'admin_editable' => false),
                'project_id' => array('value' => '', 'field_type' => 'INT NOT NULL', 'index' => true, 'admin_editable' => false),
                'name' => array('value' => '', 'field_type' => 'varchar(250) COLLATE utf8_unicode_ci NOT NULL', 'index' => true, 'admin_editable' => false),
                'cache_id' => array('value' => '', 'field_type' => 'varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL', 'index' => true, 'admin_editable' => false),
                'compile_id' => array('value' => '', 'field_type' => 'varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL', 'index' => true, 'admin_editable' => false),
                'modified' => array('value' => '', 'field_type' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP', 'index' => true, 'admin_editable' => false),
                'content' => array('value' => '', 'field_type' => 'blob NOT NULL', 'index' => true, 'admin_editable' => false)
            )
        );
        
        try {
            $this->db = new PDO("mysql:dbname=" . DB_NAME . ";host=" . DB_HOST, DB_USER, DB_PASS);
        } catch (PDOException $e) {
            throw new SmartyException('Mysql Resource failed: ' . $e->getMessage());
        }
        //$this->fetch = $this->db->prepare('SELECT modified, content FROM cache_smarty_output WHERE id = :id');
        //$this->fetchTimestamp = $this->db->prepare('SELECT modified FROM cache_smarty_output WHERE id = :id');
        $this->save = $this->db->prepare('REPLACE INTO cache_smarty_output (id, name, cache_id, compile_id, content)
            VALUES  (:id, :name, :cache_id, :compile_id, :content)');
    }

    /**
     * fetch cached content and its modification time from data source
     *
     * @param string $id unique cache content identifier
     * @param string $name template name
     * @param string $cache_id cache id
     * @param string $compile_id compile id
     * @param string $content cached content
     * @param integer $mtime cache modification timestamp (epoch)
     * @return void
     */
    protected function fetch($id, $name, $cache_id, $compile_id, &$content, &$mtime)
    {
        global $project_id;
        /*
        $this->fetch->execute(array('id' => $id));
        $row = $this->fetch->fetch();
        $this->fetch->closeCursor();  
        */
        
        $memcache = new xMemcache;
        //$memcache_smarty = $memcache->get_memcache('smarty_cache_' . $cache_id);
        $memcache_smarty = $memcache->get_memcache('smarty_cache_' . $project_id);
        
        if (!isset($memcache_smarty[$cache_id]) || !$memcache_smarty[$cache_id])
        {
            $get_cache = new Xdb;
            $cache_rows = $get_cache->select_fields('modified, content')
                                    ->set_table($this->cache_table)
                                    ->where(array('id' => $id))
                                    ->limit(1)
                                    //->db_select(true, 0, strtolower(get_class($this)));
                                    ->db_select(false);
            
            if (isset($cache_rows[0]))
            {
                $row = $cache_rows[0];
                
                if (!isset($memcache_smarty))
                {
                    $memcache_smarty[$cache_id] = $row;
                    $memcache->set_memcache('smarty_cache_' . $project_id, $memcache_smarty);
                }
                else
                {
                    $memcache_smarty[$cache_id] = $row;
                    $memcache->replace_memcache('smarty_cache_' . $project_id, $memcache_smarty);
                }
            }
        }
        else
        {
            $row = $memcache_smarty[$cache_id];
        }
        
        if (isset($row) && $row) {
            $content = gzdecode($row['content']);
            $mtime = strtotime($row['modified']);
        } else {
            $content = null;
            $mtime = null;
        }
    }
    
    /**
     * Fetch cached content's modification timestamp from data source
     *
     * @note implementing this method is optional. Only implement it if modification times can be accessed faster than loading the complete cached content.
     * @param string $id unique cache content identifier
     * @param string $name template name
     * @param string $cache_id cache id
     * @param string $compile_id compile id
     * @return integer|boolean timestamp (epoch) the template was modified, or false if not found
     */
    protected function fetchTimestamp($id, $name, $cache_id, $compile_id)
    {
        global $project_id;
        
        //$this->fetchTimestamp->execute(array('id' => $id));
        //$mtime = strtotime($this->fetchTimestamp->fetchColumn());
        //$this->fetchTimestamp->closeCursor();
        
        $memcache = new xMemcache;
        //$memcache_smarty = $memcache->get_memcache('smarty_cache_' . $cache_id);
        $memcache_smarty = $memcache->get_memcache('smarty_cache_' . $project_id);
        
        //if (!$memcache_smarty)
        if (!isset($memcache_smarty[$cache_id]) || !$memcache_smarty[$cache_id])
        {
            $get_cache = new Xdb;
            $cache_rows = $get_cache->select_fields('modified, content')
                                    ->set_table($this->cache_table)
                                    ->where(array('id' => $id))
                                    ->limit(1)
                                    //->db_select(true, 0, strtolower(get_class($this)));
                                    ->db_select(false);
            
            if (isset($cache_rows[0]))
            {
                $row = $cache_rows[0];
                
                if (!isset($memcache_smarty))
                {
                    $memcache_smarty[$cache_id] = $row;
                    $memcache->set_memcache('smarty_cache_' . $project_id, $memcache_smarty);
                }
                else
                {
                    $memcache_smarty[$cache_id] = $row;
                    $memcache->replace_memcache('smarty_cache_' . $project_id, $memcache_smarty);
                }
            }
        }
        else
        {
            $row = $memcache_smarty[$cache_id];
        }
        
        if (isset($row))
        {
            $mtime = strtotime($row['modified']);
        }
        else
        {
            $mtime = null;
        }
        
        return $mtime;
    }
    
    /**
     * Save content to cache
     *
     * @param string $id unique cache content identifier
     * @param string $name template name
     * @param string $cache_id cache id
     * @param string $compile_id compile id
     * @param integer|null $exp_time seconds till expiration time in seconds or null
     * @param string $content content to cache
     * @return boolean success
     */
    protected function save($id, $name, $cache_id, $compile_id, $exp_time, $content)
    {
        global $project_id;
        /*
        $this->save->execute(array(
            'id' => $id,
            'name' => $name,
            'cache_id' => $cache_id,
            'compile_id' => $compile_id,
            'content' => gzencode($content, 9),
        ));
        return !!$this->save->rowCount();
        */
        
        if (!$compile_id) $compile_id = 'none';
        
        $content_compressed = gzencode($content, 9);
        
        $current_time = new DateTime();
        $now = $current_time->format('Y-m-d H:i:s');
        
        $this->cache_fields['static_fields']['id']['value'] = $id;
        $this->cache_fields['static_fields']['project_id']['value'] = $project_id;
        $this->cache_fields['static_fields']['name']['value'] = $name;
        $this->cache_fields['static_fields']['cache_id']['value'] = $cache_id;
        $this->cache_fields['static_fields']['compile_id']['value'] = $compile_id;
        $this->cache_fields['static_fields']['content']['value'] = $content_compressed;
        
        $this->cache_fields['static_fields']['modified']['value'] = $now;
        
        //unset($this->cache_fields['static_fields']['modified']);
        
        $row = array(
            'id' => $id,
            'project_id' => $project_id,
            'name' => $name,
            'cache_id' => $cache_id,
            'compile_id' => $compile_id,
            'modified' => $now,
            'content' => $content_compressed
        );
        
        $xdb_cache_replace = new Xdb;
        $insert_new = $xdb_cache_replace->db_replace_content($project_id, $this->cache_table, $this->cache_fields);
        
        $last_id = $insert_new;
        
        $memcache = new xMemcache;
        $memcache_smarty = $memcache->get_memcache('smarty_cache_' . $project_id);
        
        if (!isset($memcache_smarty))
        {
            $memcache_smarty[$cache_id] = $row;
            $memcache->set_memcache('smarty_cache_' . $project_id, $memcache_smarty);
        }
        else
        {
            $memcache_smarty[$cache_id] = $row;
            $memcache->replace_memcache('smarty_cache_' . $project_id, $memcache_smarty);
        }
        
        return $last_id;
        
    }
    
    /**
     * Delete content from cache
     *
     * @param string $name template name
     * @param string $cache_id cache id
     * @param string $compile_id compile id
     * @param integer|null $exp_time seconds till expiration or null
     * @return integer number of deleted caches
     */
    protected function delete($name, $cache_id, $compile_id, $exp_time)
    {
        global $project_id;
        
        $memcache = new xMemcache;
        $memcache_smarty = $memcache->get_memcache('smarty_cache_' . $project_id);
        
        if (!isset($memcache_smarty))
        {
        
            // delete the whole cache
            if ($name === null && $cache_id === null && $compile_id === null && $exp_time === null) {
                // returning the number of deleted caches would require a second query to count them
                $query = $this->db->query('TRUNCATE TABLE cache_smarty_output');
                return -1;
            }
            // build the filter
            $where = array();
            // equal test name
            if ($name !== null) {
                $where[] = 'name = ' . $this->db->quote($name);
            }
            // equal test compile_id
            if ($compile_id !== null) {
                $where[] = 'compile_id = ' . $this->db->quote($compile_id);
            }
            // range test expiration time
            if ($exp_time !== null) {
                $where[] = 'modified < DATE_SUB(NOW(), INTERVAL ' . intval($exp_time) . ' SECOND)';
            }
            // equal test cache_id and match sub-groups
            if ($cache_id !== null) {
                $where[] = '(cache_id = '. $this->db->quote($cache_id)
                    . ' OR cache_id LIKE '. $this->db->quote($cache_id .'|%') .')';
            }
            // run delete query
            $query = $this->db->query('DELETE FROM cache_smarty_output WHERE ' . join(' AND ', $where));
            return $query->rowCount();
        
        }
        else
        {
            if ($name === null && $cache_id === null && $compile_id === null && $exp_time === null) {
                // returning the number of deleted caches would require a second query to count them
                $query = $this->db->query('TRUNCATE TABLE cache_smarty_output');
                return -1;
                
                $memcache->delete_memcache('smarty_cache_' . $project_id);
            }
            // range test expiration time
            if ($exp_time !== null) {
                $where[] = 'modified < DATE_SUB(NOW(), INTERVAL ' . intval($exp_time) . ' SECOND)';
                
                $current_time = new DateTime();
                $now = $current_time->format('Y-m-d H:i:s');
                
                foreach ($memcache_smarty as $cache_key => $cache_value)
                {
                    $saved_time = strtotime($cache_value['modified']);
                    $expiration_time = date("Y-m-d H:i:s", time() + $exp_time);
                    
                    if ($expiration_time < $now)
                    {
                        unset($memcache_smarty[$cache_key]);
                    }
                }
                
                $memcache->replace_memcache('smarty_cache_' . $project_id, $memcache_smarty);
            }
            // equal test cache_id and match sub-groups
            if ($cache_id !== null) {
                $where[] = '(cache_id = '. $this->db->quote($cache_id)
                    . ' OR cache_id LIKE '. $this->db->quote($cache_id .'|%') .')';
                
                unset($memcache_smarty[$cache_id]);
                $memcache->replace_memcache('smarty_cache_' . $project_id, $memcache_smarty);
            }
            // run delete query
            $query = $this->db->query('DELETE FROM cache_smarty_output WHERE ' . join(' AND ', $where));
            return $query->rowCount();
        }
    }
    
    public function delete_cache_ids($ids = array())
    {
        global $project_id;
        
        if (!empty($ids))
        {
            $memcache = new xMemcache;
            $memcache_smarty = $memcache->get_memcache('smarty_cache_' . $project_id);
            
            // build the filter
            $where = array();
            
            foreach ($ids as $cache_id)
            {
                $where[] = '(cache_id = '. $this->db->quote($cache_id)
                    . ' OR cache_id LIKE '. $this->db->quote($cache_id .'|%') .')';
                
                unset($memcache_smarty[$cache_id]);
            }
            
            $query = $this->db->query('DELETE FROM cache_smarty_output WHERE ' . join(' OR ', $where));
            $memcache->replace_memcache('smarty_cache_' . $project_id, $memcache_smarty);
        }
    }
}

?>