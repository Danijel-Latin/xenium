<?php

class Condition extends Module
{
    public static function getGlobal($project_id = 0)
    {
        global $_PROJECT;

        if ($_PROJECT && !is_array($_PROJECT) && !empty($_PROJECT))
        {
            $project = new Project;
            $project->getById($project_id);
        }
        $project_slug = $_PROJECT['project_slug'];

        foreach (glob(DIR_MODULES.'*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
            $dir_array = explode('/', $dir);
            $dir_class = end($dir_array);

            if (file_exists($dir . '/conditions.' . $dir_class . '.php'))
            {
                if ($project_id && file_exists(DIR_WWW . 'projects/' . $project_slug . '/settings/modules/' . $dir_class . '/conditions.' . $dir_class . '.php'))
                {
                    include(DIR_WWW . 'projects/' . $project_slug . '/settings/modules/' . $dir_class . '/conditions.' . $dir_class . '.php');
                }
                else
                {
                    include($dir . '/conditions.' . $dir_class . '.php');
                }
            }
        }
    }

    public static function add($condition_array = array())
    {
        global $_CONDITIONS;
        if (!$_CONDITIONS || empty($_CONDITIONS)) $_CONDITIONS = array();

        if (!empty($condition_array))
        {
            if (isset($condition_array['id']) && $condition_array['id'])
            {
                $_CONDITIONS[$condition_array['id']] = $condition_array;
            }
        }
    }

    public static function generate($condition_json, $condition_layout)
    {
        $condition_array = json_decode($condition_json, true);
        $condition = 'if (' . self::make($condition_array) . '){ $layout = \'' . $condition_layout . '\'; $layout_file = \'' . $condition_layout . '.html\'; }';

        return $condition;
    }

    public static function make($condition_array)
    {
        if ($condition_array['condition'] == 'AND') $condition_amp = '&&'; else $condition_amp = '||';
        $condition = '';

        foreach ($condition_array['rules'] as $rule)
        {
            if ($condition)
            {
                $condition .= ' ' . $condition_amp . ' ';
            }

            if (isset($rule['rules']))
            {
                $condition .= '(' . Condition::make($rule) . ')';
            }
            else
            {
                $ask_array = explode('->', $rule['id']);
                $class_name = $ask_array[0];
                $func = $ask_array[1];
                if (isset($ask_array[2])) $field = $ask_array[2]; else $field = '';

                $operator = self::operator($rule['operator']);
                if ($field) $field = '\'' . $field . '\'';

                if ($rule['type'] == 'string') $rule['value'] = '\'' . $rule['value'] . '\'';

                if ($operator == 'in_array' || $operator == '!in_array')
                {
                    $condition .= $operator . '(' . $class_name . '::' . $func . '(' . $field . ')' . ', [' . $rule['value'] . '])';
                }
                elseif($operator == 'between')
                {
                    $condition .= '(' . $class_name . '::' . $func . '(' . $field . ')' . ' >= ' . $rule['value'][0] . ' && ' . $class_name . '::' . $func . '(' . $field . ')' . ' <= ' . $rule['value'][1] . ')';
                }
                elseif($operator == 'not_between')
                {
                    $condition .= '(' . $class_name . '::' . $func . '(' . $field . ')' . ' < ' . $rule['value'][0] . ' && ' . $class_name . '::' . $func . '(' . $field . ')' . ' > ' . $rule['value'][1] . ')';
                }
                else
                {
                    $condition .= $class_name . '::' . $func . '(' . $field . ') ' . $operator . ' ' . $rule['value'];
                }
            }
        }

        return $condition;
    }

    public static function operator($operator = 'equal')
    {
        switch($operator)
        {
            case 'equal':
                $operator = '==';
                break;
            case 'not_equal':
                $operator = '!=';
                break;
            case 'in':
                $operator = 'in_array';
                break;
            case 'not_in':
                $operator = '!in_array';
                break;
            case 'less':
                $operator = '<';
                break;
            case 'less_or_equal':
                $operator = '<=';
                break;
            case 'greater':
                $operator = '>';
                break;
            case 'greater_or_equal':
                $operator = '>=';
                break;
            case 'between':
                $operator = 'between';
                break;
            case 'not_between':
                $operator = 'not_between';
                break;
            case 'is_null':
                $operator = '== null';
                break;
            case 'is_not_null':
                $operator = '!= null';
                break;
        }

        return $operator;
    }
}

?>
