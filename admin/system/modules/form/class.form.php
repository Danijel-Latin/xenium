<?php

/**
 * @author Danijel
 * @copyright 2013
 */

class Form
{
    public static function clean($string)
    {
        $beginning = '[|';
        $end = '|]';
        $beginningPos = strpos($string, $beginning);
        $endPos = strpos($string, $end);
        if ($beginningPos === false || $endPos === false)
        {
            return $string;
        }

        $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

        return str_replace($textToDelete, '', $string);

    }

    public static function populateAdminForm($html, $populate_array, $structure)
    {
        //$populate_fields = $populate_array[0];
        $populate_fields = $populate_array;
        //print_r($populate_fields);
        foreach ($populate_fields as $field => $field_value)
        {
            $set_value['[|' . $field . '|]'] = $field_value;
        }
        $html = strtr($html, $set_value);

        // if all values were not replaced, check for default ones
        // static contents
        foreach ($structure['static_fields'] as $s_field => $s_field_value)
        {
            //$html = str_replace('[|' . $s_field . '|]', '', $html);
            $set_default_value['[|' . $s_field . '|]'] = $s_field_value['default_value'];
        }
        // dynamic contents
        $languages = Languages::getAllProjectLanguages($project_id);
        foreach ($languages as $language)
        {
            foreach ($structure['dynamic_fields'] as $d_field => $d_field_value)
            {
                $set_default_value['[|' . $d_field . '_' . $language['code'] . '|]'] = $d_field_value['default_value'];
            }
        }
        // replace all the found values
        $html = strtr($html, $set_default_value);

        return $html;
    }

    //public function generate_admin_form($values_array, $populate_array, $editing, $project_id, $id)
    public static function generateAdminForm($class_name, $populate_array, $project_id, $id)
    {
        global $_CONDITIONS;
        $static_area = false;

        $project = new Project;
        $project_info = $project->getById($project_id);
        $project_slug = $project_info['project_slug'];
        //echo $condition_json;
        $conditions_url = DIR_PROJECTS . 'project_' . $project_slug . '/conditions/';
        $layouts_url = DIR_PROJECTS . 'project_' . $project_slug . '/layouts/';

        $html = '<form id="EditContent_' . $class_name . '_' . $project_id . '_' . $id . '"><ul id="language-tabs" class="nav nav-tabs" role="tablist">';
        $tab_counter = 0;
        $dynamic_area = '<div id="Content" class="tab-content">';
        $static_content = '';

        $called_class = new $class_name;
        //$values_array = $called_class->fields;
        $values_array = $called_class->tables['www_pages'];

        foreach ($values_array['static_fields'] as $static_field => $static_field_settings)
        {
            if ($static_field_settings['admin_editable'] == true )
            {
                $static_area = true;

                $type = $static_field_settings['html']['type'];
                $label = $static_field_settings['html']['label'];

                $prepend = '';
                $append = '';
                if (isset($static_field_settings['html']['prepend'])){ $prepend = $static_field_settings['html']['prepend']; }
                if (isset($static_field_settings['html']['append'])){ $append = $static_field_settings['html']['append']; }

                $static_content .= '<label>' . $label . '</label>';

                $attributes = '';
                if (isset($static_field_settings['html_attributes']) && is_array($static_field_settings['html_attributes']))
                {
                    foreach ($static_field_settings['html_attributes'] as $attribute => $attr_value)
                    {
                        $attributes .= ' ' . $attribute . '="' . $attr_value . '"';
                    }
                }

                $static_content .= $prepend;

                switch ($type)
                {
                    case 'input':
                        $static_content .= '<input id="' . $static_field . '_' . $id . '_' . $project_id . '" name="' . $static_field . '" value="[|' . $static_field . '|]"' . $attributes . ' />';
                        break;
                    case 'textarea':
                        $static_content .= '<textarea id="' . $static_field . '_' . $id . '_' . $project_id . '" name="' . $static_field . '"' . $attributes . '>[|' . $static_field . '|]</textarea>';
                        break;
                    case 'image':
                        //print_r($populate_array);
                        $image_json = str_replace('&quot;', '"', $populate_array[0][$static_field]);
                        $static_content .= '<textarea style="display: none;" id="' . $static_field . '_' . $id . '_' . $project_id . '" name="' . $static_field . '">[|' . $static_field . '|]</textarea>';

                        $images = json_decode($image_json, true);
                        $populate_image = '';
                        if (count($images))
                        {
                            foreach ($images as $image)
                            {
                                if (isset($image['fileUrl']))
                                {
                                    $populate_image .= '<div class="image" id="' . $image['id'] . '">
                                        <div class="img" style="background: url(' . $image['fileUrl'] . ')"></div>
                                    </div>';
                                }
                                else
                                {
                                    $populate_image .= '<div class="image" id="' . $image['id'] . '" year="' . $image['fileYear'] . '" month="' . $image['fileMonth'] . '" image="' . $image['fileName'] . '">
                                        <div class="img" style="background: url(/images/450x350/' . $image['fileYear'] . '/' . $image['fileMonth'] . '/' . $image['fileName'] . ')"></div>
                                    </div>';
                                }

                            }
                        }
                        $static_content .= '<div id="' . $static_field . '_' . $id . '_' . $project_id . '-plupload"  class="image-block">' . $populate_image . '</div>';


                }

                $static_content .= $append;

            }
            else
            {
                //$html .= '<input id="' . $static_field . '_' . $id . '_' . $project_id . '" name="' . $static_field . '" value="[|' . $static_field . '|]" style="display: none;" />';
                $html .= '<textarea id="' . $static_field . '_' . $id . '_' . $project_id . '" name="' . $static_field . '" class="not-editable">[|' . $static_field . '|]</textarea>';
            }
        }

        if (isset($values_array['dynamic_fields']))
        {
            $languages = Languages::getAllProjectLanguages($project_id);


            if ($static_area)
            {
                $html .= '<li class="nav-item title-tab"><a class="nav-link active info-tab" data-toggle="tab" role="tab" no-follow="true" href="#content_info"><!--<i class="fa fa-info-circle fa-2x"></i>--><i class="icon icon-linea-arrows-10-79"></i> <span>Modifications</span></a></li>
                         <li class="nav-item title-tab"><a class="nav-link info-tab" data-toggle="tab" role="tab" no-follow="true" href="#contents"><!--<i class="fa fa-file-text-o fa-2x"></i>--><i class="icon icon-icon icon-linea-basic-10-101"></i> <span>Contents</span></a></li>';
                $dynamic_area .= '<div id="content_info" class="tab-pane fade active show">' . $static_content . '</div>
                                 <div id="contents" class="row tab-pane fade">
                                 <div class="col-md-1 tabs-left-container">
                                 <ul id="language-tabs" class="nav nav-tabs flex-column tabs-left tabs-left-fixed" role="tablist">';
            }
            else
            {
                $html .= '<li class="nav-item title-tab"><a class="nav-link info-tab active" data-toggle="tab" role="tab" no-follow="true" href="#contents"><i class="icon icon-icon icon-linea-basic-10-101"></i> <span>Contents</span></a></li>';
                $dynamic_area .= '<div id="contents" class="row tab-pane fade active show">
                <div class="col-md-1 tabs-left-container">
                <ul id="language-tabs" class="nav nav-tabs tabs-left flex-column tabs-left-fixed" role="tablist">';
            }

            //$dynamic_area_lang = '<div class'

            $lng_tab_counter = 0;
            foreach ($languages as $language)
            {
                //if ($tab_counter == 0 && !$static_area)
                if ($lng_tab_counter == 0)
                {
                    $html_lang .= '<li class="nav-item"><a class="nav-link active" data-toggle="tab" role="tab" no-follow="true" href="#content_' . $language['code'] . '"><img src="/admin/theme/images/flags/32/' . $language['icon'] . '" /></a></li>';
                    $tab_class = 'tab-pane fade active show';
                }
                else
                {
                    $html_lang .= '<li class="nav-item"><a class="nav-link" data-toggle="tab" role="tab" no-follow="true" href="#content_' . $language['code'] . '"><img src="/admin/theme/images/flags/32/' . $language['icon'] . '" /></a></li>';
                    $tab_class = 'tab-pane fade';
                }

                $dynamic_area_lang .= '<div id="content_' . $language['code'] . '" class="' . $tab_class . '">';
                foreach ($values_array['dynamic_fields'] as $dynamic_field => $dynamic_field_settings)
                {
                    if ($dynamic_field_settings['admin_editable'] == true)
                    {
                        $type = $dynamic_field_settings['html']['type'];
                        $label = $dynamic_field_settings['html']['label'];

                        $prepend = '';
                        $append = '';
                        if (isset($static_field_settings['html']['prepend'])){ $prepend = $static_field_settings['html']['prepend']; }
                        if (isset($static_field_settings['html']['append'])){ $append = $static_field_settings['html']['append']; }

                        $dynamic_area_lang .= '<label>' . $label . '</label>';

                        $attributes = '';
                        foreach ($dynamic_field_settings['html_attributes'] as $attribute => $attr_value)
                        {
                            $attributes .= ' ' . $attribute . '="' . $attr_value . '"';
                        }

                        $dynamic_area_lang .= $prepend;

                        switch ($type)
                        {
                            case 'input';
                                $dynamic_area_lang .= '<input id="' . $dynamic_field . '_' . $language['code'] . '_' . $id . '_' . $project_id . '" name="' . $dynamic_field . '_' . $language['code'] . '" value="[|' . $dynamic_field . '_' . $language['code'] . '|]"' . $attributes . ' />';
                                break;
                            case 'textarea':
                                $dynamic_area_lang .= '<textarea id="' . $dynamic_field . '_' . $language['code'] . '_' . $id . '_' . $project_id . '" name="' . $dynamic_field . '_' . $language['code'] . '"' . $attributes . '>[|' . $dynamic_field . '_' . $language['code'] . '|]</textarea>';
                                if (isset($dynamic_field_settings['html_attributes']['mode']) && $dynamic_field_settings['html_attributes']['mode'] == 'advanced')
                                {
                                    $dynamic_area_lang .= '<pre id="code_' . $dynamic_field . '_' . $language['code'] . '_' . $id . '_' . $project_id . '" class="code-editor">[|' . $dynamic_field . '_' . $language['code'] . '|]</pre>';
                                }
                                break;
                        }

                        $dynamic_area_lang .= $append;
                    }
                }
                $dynamic_area_lang .= '</div>';

                $lng_tab_counter++;
            }
            //$html .= $html_lang;
            $dynamic_area .= $html_lang . '</ul></div>' . '<div class="tab-content col-md-11 lang-form-container">' . $dynamic_area_lang . '</div>';
            $dynamic_area .= '</div>'; // finish contents tab
            // add layouts
            $html .= '<li class="nav-item title-tab"><a class="nav-link info-tab" data-toggle="tab" role="tab" no-follow="true" href="#layouts"><!--<i class="fa fa-th-large fa-2x"></i>--><i class="icon icon-linea-software-10-35"></i> <span>Layouts</span></a></li>';

            $condition_files = glob($conditions_url . '*_' . strtolower($class_name) . '.php');
            usort( $condition_files, function( $a, $b ) { return filemtime($a) - filemtime($b); } );

            $layouts = Layout::getAll($project_id);
            $layout_dropdown = Layout::generateDropdown($layouts);

            // create the template layout selection
            include($conditions_url . 'default_' . strtolower($class_name) . '.php');
            if (!file_exists($layouts_url . $condition_layout . '.html'))
            {
                $condition_layout = 'Default';
            }

            $dynamic_area .= '<div id="layouts" class="tab-pane fade">
                <br />
                <h4>Create conditions and choose layouts for them</h4>
                <p>
                The layout will be displayed if the conditions are met, otherwise the default layout will be shown.
                </p>
                <br />

                <table class="table table-hover conditions-table" project-id="' . $project_id . '">
                  <thead>
                    <tr>
                      <th style="width: 5%;">Active</th>
                      <th style="width: 40%;">Condition</th>
                      <th style="width: 45%;">Layout</th>
                      <th style="width: 10%;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="condition" id="condition-default">
                      <td class="condition-activation">
                        <i class="icon icon-linea-arrows-10-92" style="opacity: .5;"></i>
                        <input type="hidden" value="default" />
                        <textarea class="invisible"></textarea>
                      </td>
                      <td class="condition-name">Default state</td>
                      <td>
                      <div class="btn-group">
                          <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ' . $condition_layout . '
                          </button>
                          <div class="dropdown-menu">
                              <div class="layout-selection">
                              ' . $layout_dropdown . '
                              </div>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item new-layout" href="#" data-toggle="modal" data-target="#layoutModal">Create a new layout</a>
                          </div>
                      </div>
                      </td>
                      <td class="actions">
                          <a href="#" role="button" aria-disabled="true" disabled class="btn btn-secondary disabled edit-condition" title="Edit" data-toggle="modal" data-target="#conditionsModal"><i class="icon icon-linea-software-10-59"></i></a>
                          <a href="#" role="button" aria-disabled="true" disabled class="btn btn-secondary disabled" title="Remove"><i class="icon icon-linea-arrows-10-57"></i></a>
                      </td>
                    </tr>';

                    if (!empty($condition_files))
                    {
                        foreach ($condition_files as $file)
                        {
                            $file_array = explode('/', $file);
                            $file_name = end($file_array);

                            if ($file_name != 'default_' . $class_name . '.php')
                            {
                                include($file);
                                if (!file_exists($layouts_url . $condition_layout . '.html'))
                                {
                                    $condition_layout = 'Default';
                                }

                                $dynamic_area .= '<tr class="condition" id="condition-' . $condition_id . '">
                                  <td class="condition-activation">
                                    <i class="icon icon-linea-arrows-10-92"></i>
                                    <input type="hidden" value="' . $condition_id . '" />
                                    <textarea class="invisible">' . $condition_json . '</textarea>
                                  </td>
                                  <td class="condition-name">
                                    ' . $condition_name . '
                                  </td>
                                  <td>
                                      <div class="btn-group">
                                          <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            ' . $condition_layout . '
                                          </button>
                                          <div class="dropdown-menu">
                                              <div class="layout-selection">
                                              ' . $layout_dropdown . '
                                              </div>
                                              <div class="dropdown-divider"></div>
                                              <a class="dropdown-item new-layout" href="#" data-toggle="modal" data-target="#layoutModal">Create a new layout</a>
                                          </div>
                                      </div>
                                  </td>
                                  <td class="actions">
                                      <a href="#" role="button" class="btn btn-secondary edit-condition" title="Edit" data-toggle="modal" data-target="#conditionsModal"><i class="icon icon-linea-software-10-59"></i></a>
                                      <a href="#" role="button" class="btn btn-secondary delete-condition" title="Remove"><i class="icon icon-linea-arrows-10-57"></i></a>
                                  </td>
                                </tr>';
                            }
                        }
                    }

                    $dynamic_area .= '</tbody>
                </table>

                <!--<div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <input type="radio" name="layout-selection"> <span>Default layout</span>
                            </div>
                            <div class="panel-body">
                                Panel content
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <input type="radio" name="layout-selection"> <span>My other layout</span>
                            </div>
                            <div class="panel-body">
                                Panel content
                            </div>
                        </div>
                    </div>
                </div>
                <hr />-->
                <button type="button" class="btn btn-primary new-condition" data-toggle="modal" data-target="#conditionsModal">
                    Create a new condition
                </button>
                <button type="button" class="btn btn-primary new-layout" data-toggle="modal" data-target="#layoutModal">
                    Create a new custom layout
                </button>

                <!--<hr />
                <div id="query-builder">

                </div>-->

                <!-- Modal layout -->
                <div class="modal fade full-screen" id="layoutModal" role="dialog" aria-labelledby="layoutModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="layoutModalLabel">Layout editor <input placeholder="Layout name" class="layout-name" style="margin-left: 18px;" /></h4>
                                <input type="hidden" class="modal-project-id" value="' . $project_id . '" />
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-10 layout-editor">

                                    </div>
                                    <div class="col-md-2 layout-editing">

                                    </div>
                                </div>
                                <!--<button class="btn btn-secondary" data-toggle="modal" href="#createBlockModal">Launch modal</button>-->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary save-layout">Save changes</button>
                            </div>

                            <div id="createBlockModal" class="modal fade modal-over" tabindex="-1" data-focus-on="input:first" style="display: none;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-body">
                            neki tle
                            </div></div></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal conditions -->
                <div class="modal fade" id="conditionsModal" role="dialog" aria-labelledby="layoutModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="layoutModalLabel">Conditions editor</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Condition name:</label>
                                        <input type="hidden" class="modal-condition-id" />
                                        <input type="hidden" class="modal-project-id" value="' . $project_id . '" />
                                        <input type="hidden" class="modal-class-name" value="' . $class_name . '" />
                                        <input type="text" class="form-control modal-condition-name" />
                                    </div>
                                    <div class="col-md-6">
                                        <label>Display layout:</label>
                                        <select class="form-control modal-condition-layout">
                                            <option val="">Default layout</option>
                                            <option val="1">Layout 1</option>
                                            <option val="2">Layout 2</option>
                                            <option val="3">Layout 3</option>
                                        </select>
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Edit condition:</label>
                                        <div id="query-builder">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary save-condition">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>';

            $dynamic_area .= '</div>';

            // output available conditions
            Condition::getGlobal($project_id); // get global conditions

            $dynamic_area .= '<div id="query-conditions" class="invisible">' . json_encode(array_values($_CONDITIONS)) . '</div>';

            $dynamic_area .= '
            <script type="text/javascript">
            $(".layout-editor").gridEditor({
                //new_row_layouts: [[12], [6,6], [9,3]],
                content_types: ["aloha"],
            });
            </script>';
            $html .= '</ul>';

            /*$api = new API('interface/new');
            $api_result = json_decode($api->post(['name' => 'Mycomputer', 'type' => 0]), true);
            print_r($api_result);*/
        }
        /*
        foreach ($values_array['static_fields'] as $static_field => $static_field_settings)
        {
            if ($static_field_settings['admin_editable'] == true )
            {
                $type = $static_field_settings['html']['type'];
                $label = $static_field_settings['html']['label'];

                $html .= '<label>' . $label . '</label>';

                $attributes = '';
                foreach ($static_field_settings['html_attributes'] as $attribute => $attr_value)
                {
                    $attributes .= ' ' . $attribute . '="' . $attr_value . '"';
                }

                switch ($type)
                {
                    case 'input':
                        $html .= '<input id="' . $static_field . '" name="' . $static_field . '" value="[|' . $static_field . '|]"' . $attributes . ' />';
                        break;
                    case 'textarea':
                        $html .= '<textarea id="' . $static_field . '" name="' . $static_field . '"' . $attributes . '>[|' . $static_field . '|]</textarea>';
                        break;
                }
            }
            else
            {
                $html .= '<input id="' . $static_field . '" name="' . $static_field . '" value="[|' . $static_field . '|]" style="display: none;" />';
            }
        }
        */
        $html .= $dynamic_area;

        $html .= '</form>';

        $form = Form::populateAdminForm($html, $populate_array, $values_array);

        return $form;
    }
}

?>
