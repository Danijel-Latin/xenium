<?php

/**
 * @author Danijel
 * @copyright 2016
 */

class Project extends Module
{
    public $tables;

    public function __construct()
    {
        parent::__construct();
        $this->tables = array('www_projects', 'www_blog', 'www_domains');

        $this->www_projects = Field::structure(
            // ststic fields
            Field::stat()->project_name->varchar(50),
            Field::stat()->project_slug->varchar(50),
            Field::stat()->project_settings->text(),
            Field::stat()->ref_id->int(),
            Field::stat()->theme_name->varchar(150),
            Field::stat()->theme_slug->varchar(150)
        );
    }

    protected function getByDomain($domain = '')
    {
        global $_PROJECT;

        if (!$domain)
        {
            $domain = $_SERVER['HTTP_HOST'];
        }

        $project_xorm = new Xorm($this);
        $project = $project_xorm->www_projects->fields('*')
                                ->rightJoin('www_domains', ['id' => 'project_id'])
                                ->where(['domain' => $domain])
                                ->select();

        // set the project global
        $_PROJECT = $project[key($project)];
        return $_PROJECT;
    }

    public function getById($id)
    {
        global $_PROJECT;

        if ($id)
        {
            $project_xorm = new Xorm($this);
            $project = $project_xorm->www_projects->fields('*')
                                    ->where(['id' => intval($id)])
                                    ->select();

            // set the project global
            $_PROJECT = $project[key($project)];
            return $_PROJECT;
        }
    }

    public function getBySlug($slug)
    {
        if ($slug && preg_match("/^[a-zA-Z0-9-]+$/", $slug))
        {
            $project_xorm = new Xorm($this);
            $project = $project_xorm->www_projects->fields('*')
                                    ->where(['project_slug' => $slug])
                                    ->freshSelect();

            return $project[key($project)];
        }
    }

    public function install($project_id, $project_name, $project_slug, $repo_url)
    {
        Git::installProject($project_id, $project['project_name'], $project['project_slug'], $project[]);
    }

    public static function getTheme()
    {
        global $_PROJECT;

        if (isset($_PROJECT['theme_slug']) && $_PROJECT['theme_slug'] != '')
        {
            return $_PROJECT['theme_slug'];
        }
        else
        {
            return 'default';
        }
    }

    public static function getAdminCurrent()
    {
        $current_url = $_SERVER['REQUEST_URI'];
        $url_bits = explode('/', $current_url);
        if (ctype_digit($url_bits[3]))
        {
            $project_id = $url_bits[3];
        }
        else
        {
            $project_id = 0;
        }

        return $project_id;
    }

    public static function getAll()
    {
        return Xorm::set(Project::www_projects())->freshSelect();
    }

    public static function getInfo($project_id)
    {
        $project_info = Xorm::set(Project::www_projects())
                            ->where(['id' => $project_id])
                            ->limit(1)
                            ->freshSelect();
        return $project_info;
    }
}

?>
