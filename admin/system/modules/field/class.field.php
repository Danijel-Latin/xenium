<?php

/**
 * @author Danijel
 * @copyright 2016
 */

class Field extends Module
{
    public $field = array();
    public $field_name;
    public $field_mode;
    public $field_condition_settings = array();

    public function __get($key)
    {
        $this->field_name = $key;
        return $this;
    }

    public static function structure()
    {
        if (func_num_args() < 1)
        {
            throw new BadFunctionCallException("At least one field must be defined!");
        }
        else
        {
            $field_array = array();
            $field_array['static_fields']['id'] = ['field_type' => 'id INT NOT NULL AUTO_INCREMENT PRIMARY KEY', 'admin_editable' => false];
            foreach (func_get_args() as $param) {
                //print_r($param);
                $field_array[$param->field_mode][$param->field_name] = $param->field;
            }
            //d($field_array);
            return $field_array;
        }
    }

    public static function stat()
    {
        $set_field = new self;
        $set_field->field_mode = 'static_fields';
        return $set_field;
    }

    public static function dyn()
    {
        $set_field = new self;
        $set_field->field_mode = 'dynamic_fields';
        return $set_field;
    }

    public function condition($condition_settings = array())
    {
        if (isset($this->field_name) && $this->field_name)
        {
            $label = ucwords(str_replace('_', ' ', $this->field_name));
            if ($this->field_mode == 'dynamic_fields') $label .= ' (dyn)';

            // get the calling class
            $caller = debug_backtrace();
            $caller = $caller[3];
            //$r = $caller['function'] . '()';
            $calling_class = '';
            if (isset($caller['class']))
            {
                $calling_class .= $caller['class'];
            }
            if (isset($caller['object']) && !$calling_class)
            {
                $calling_class .= get_class($caller['object']);
            }

            if (empty($this->field_condition_settings))
            {
                $this->field_condition_settings = ['id' => $calling_class . '->fieldCondition->' . $this->field_name, 'label' => $label, 'type' => 'integer'];
            }

            if (!empty($condition_settings))
            {
                foreach ($condition_settings as $condition_key => $condition_value)
                {
                    $this->field_condition_settings[$condition_key] = $condition_value;
                }
            }

            Condition::add($this->field_condition_settings);
        }
    }

    public function tinyint($default_value = 0)
    {
        $this->field['field_type'] = 'TINYINT NOT NULL';
        $this->field['index'] = true;
        $this->field['admin_editable'] = true;
        $this->field['value'] = intval($default_value);

        $this->condition();

        return $this;
    }

    public function smallint($default_value = 0)
    {
        $this->field['field_type'] = 'SMALLINT NOT NULL';
        $this->field['index'] = true;
        $this->field['admin_editable'] = true;
        $this->field['value'] = intval($default_value);

        $this->condition();

        return $this;
    }

    public function mediumint($default_value = 0)
    {
        $this->field['field_type'] = 'MEDIUMINT NOT NULL';
        $this->field['index'] = true;
        $this->field['admin_editable'] = true;
        $this->field['value'] = intval($default_value);

        $this->condition();

        return $this;
    }

    public function int($default_value = 0)
    {
        $this->field['field_type'] = 'INT NOT NULL';
        $this->field['index'] = true;
        $this->field['admin_editable'] = true;
        $this->field['value'] = intval($default_value);

        $this->condition();

        return $this;
    }

    public function bigint($default_value = 0)
    {
        $this->field['field_type'] = 'BIGINT NOT NULL';
        $this->field['index'] = true;
        $this->field['admin_editable'] = true;
        $this->field['value'] = intval($default_value);

        $this->condition();

        return $this;
    }

    public function decimal($m = 10, $d = 2, $default_value = 0)
    {
        $this->field['field_type'] = 'DECIMAL (' . $m . ', ' . $d . ') NOT NULL';
        $this->field['index'] = true;
        $this->field['admin_editable'] = true;
        $this->field['value'] = floatval($default_value);

        $this->condition(['type' => 'decimal']);

        return $this;
    }

    public function bool($default_value = 0)
    {
        $this->tinyint($default_value);

        /*Condition::add([
            'id' => $this->field_name,
            'label' => str_replace('_', ' ', $this->field_name),
            'type' => 'integer',
            'input' => 'radio',
            'values' => [0 => 'false', 1 => 'true'],
            'operators' => ['equal']
        ]);*/

        $this->condition([
            'input' => 'radio',
            'values' => [0 => 'false', 1 => 'true'],
            'operators' => ['equal']
        ]);

        return $this;
    }

    public function varchar($length = 255, $default_value = '')
    {
        $this->field['field_type'] = 'VARCHAR( ' . $length . ' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL';
        $this->field['index'] = true;
        $this->field['admin_editable'] = true;
        $this->field['value'] = sanitize($default_value);

        $this->condition(['type' => 'string']);

        return $this;
    }

    public function text($default_value = '')
    {
        $this->field['field_type'] = 'TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL';
        $this->field['index'] = true;
        $this->field['admin_editable'] = true;
        $this->field['value'] = sanitize($default_value);

        $this->condition(['type' => 'string']);

        return $this;
    }

    public function date($default_value = '')
    {
        $this->field['field_type'] = 'DATE NULL';
        $this->field['index'] = true;
        $this->field['admin_editable'] = true;
        $this->field['value'] = sanitize($default_value);

        $this->condition(['type' => 'date']);

        return $this;
    }

    /****************************/
    /* setting other properties */
    /****************************/

    public function label($label)
    {
        if ($label) $this->field['html']['label'] = $label;

        if ($this->field_mode == 'dynamic_fields') $label .= ' (dyn)';
        $this->condition(['label' => $label]);

        return $this;
    }

    public function type($type)
    {
        if ($type) $this->field['html']['type'] = $type;
        return $this;
    }

    public function htmlAttributes($attributes = array())
    {
        if (!empty($attributes))
        {
            $this->field['html_attributes'] = $attributes;
        }
        return $this;
    }

    public function hidden()
    {
        $this->field['admin_editable'] = 0;
        return $this;
    }

    /*****************/
    /* hidden fields */
    /*****************/

    public function hiddenTinyint($default_value = '')
    {
        $this->tinyint($default_value)->hidden();
        return $this;
    }

    public function hiddenSmallint($default_value = '')
    {
        $this->smallint($default_value)->hidden();
        return $this;
    }

    public function hiddenMediumint($default_value = '')
    {
        $this->mediumint($default_value)->hidden();
        return $this;
    }

    public function hiddenInt($default_value = '')
    {
        $this->int($default_value)->hidden();
        return $this;
    }

    public function hiddenBigint($default_value = '')
    {
        $this->bigint($default_value)->hidden();
        return $this;
    }

    public function hiddenDecimal($m = 10, $d = 2, $default_value = 0)
    {
        $this->decimal($m, $d, $default_value)->hidden();
        return $this;
    }

    public function hiddenBool($default_value = 0)
    {
        $this->hiddenTinyint($default_value);
        /*Condition::add([
            'id' => $this->field_name,
            'label' => str_replace('_', ' ', $this->field_name),
            'type' => 'integer',
            'input' => 'radio',
            'values' => [0 => 'false', 1 => 'true'],
            'operators' => ['equal']
        ]);*/
        $this->condition([
            'input' => 'radio',
            'values' => [0 => 'false', 1 => 'true'],
            'operators' => ['equal']
        ]);

        return $this;
    }

    public function hiddenInput($length = 255, $default_value = '')
    {
        $this->varchar($length, $default_value)->hidden();
        return $this;
    }

    /***************/
    /* HTML fields */
    /***************/

    public function input($label = '', $length = 255, $default_value = '')
    {
        $this->varchar($length, $default_value)->label($label)->type('input')->htmlAttributes(['class' => 'form-control', 'type' => 'text']);
        return $this;
    }

    public function richInput($label = '', $length = 255, $default_value = '')
    {
        $this->varchar($length, $default_value)->label($label)->type('input')->htmlAttributes(['class' => 'form-control  html_edit_simple', 'type' => 'text']);
        return $this;
    }

    public function textarea($label = '', $default_value = '')
    {
        $this->text($default_value)->label($label)->type('textarea')->htmlAttributes(['class' => 'form-control', 'type' => 'text']);
        return $this;
    }

    public function simpleTextarea($label = '', $default_value = '')
    {
        $this->text($default_value)->label($label)->type('textarea')->htmlAttributes(['class' => 'col-md-12  form-control  html_edit_simple', 'rows' => 6, 'type' => 'text']);
        return $this;
    }

    public function richTextarea($label = '', $default_value = '')
    {
        $this->text($default_value)->label($label)->type('textarea')->htmlAttributes(['class' => 'col-md-12 rows12 form-control  html_edit_advanced', 'rows' => 16, 'type' => 'text', 'mode' => 'advanced']);
        return $this;
    }

    public function image($label = '', $default_value = '[]')
    {
        $this->text($default_value)->label($label)->type('image')->htmlAttributes(['type' => 'image']);
        return $this;
    }
}
