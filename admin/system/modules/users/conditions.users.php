<?php

Condition::add([
    'id' => 'Users->loggedIn',
    'label' => 'User logged in',
    'type' => 'integer',
    'input' => 'radio',
    'values' => ['false', 'true'],
    'operators' => ['equal']
]);

Condition::add([
    'id' => 'Users->id',
    'label' => 'User Id',
    'type' => 'integer'
]);

// other user properties
$user_properties = ['email', 'username', 'name', 'lastname'];

foreach ($user_properties as $property)
{
    Condition::add([
        'id' => 'Users->property->' . $property,
        'label' => 'User property: ' . $property,
        'type' => 'string'
    ]);
}

/*
Array
(
    [0] => Array
        (
            [id] => Page->fieldCondition->neki
            [label] => Title
            [type] => string
        )

    [1] => Array
        (
            [id] => Page->fieldCondition->project_id
            [label] => Project Id
            [type] => integer
        )

    [7] => Array
        (
            [id] => Field->fieldCondition->published
            [label] => Published
            [type] => integer
            [input] => radio
            [values] => Array
                (
                    [0] => false
                    [1] => true
                )

            [operators] => Array
                (
                    [0] => equal
                )

        )

)
*/

?>
