<?php

/**
 * @author Danijel
 * @copyright 2016
 */

class Module
{
    private $project_id;
    private $methods = array();
    private $plugins = array();
    public $obj;
    public $cache_setting;
    public $tables = array();
    public $table;
    public $table_fields = array();

    public $construct_args = array();

    public function __construct()
    {
        $num_args = func_num_args();
        if ($num_args)
        {
            $this->construct_args = func_get_args();
        }
        $this->loadPlugins();
        $this->cache_setting = false;

        $class_methods = get_class_methods($this);
        foreach ($class_methods as $method_name) {
            //echo "$method_name\n";
            $this->methods[$method_name] = $method_name;
        }
    }

    public function __call($method, $args)
    {
        if(! key_exists($method, $this->methods))
           throw new Exception ("Call to undefined method: class -> " . get_class($this) . " method -> " . $method);


        if (isset($this->plugins[$this->methods[$method]]))
        {
           return call_user_func_array(array($this->plugins[$this->methods[$method]], $method), $args);
        }
        else
        {
           $start = microtime(true);
           $return_val = call_user_func_array(array($this, $method), $args);
           $end = microtime(true);
           Xdebugger::addMeasure(get_class($this) . '::' . $method, $start, $end);
           return $return_val;
        }
    }

    public static function __callStatic($method, $args){
        $self_class = new self;
        //return 'I am '.$method.' and I am called with the arguments : '.implode(',', $args);
        $self_class->table = $method;
        $self_class->table_fields = $args;
        return $self_class;
    }

    public function __set($key, $value)
    {
        //$self_class = new self;
        //$self_class->tables[$key] = $value;
        //print $self_class->tables;
        $this->tables[$key] = $value;
        return $this;
    }

    public function __get($key)
    {
        //return $this->tables[$key];
        $this->table = $key;
        return $this;
    }

    /*public function setFields()
    {
        if (func_num_args() < 1)
        {
            throw new BadFunctionCallException("At least one field must be defined!");
        }
        else
        {
            $field_array = array();
            foreach (func_get_args() as $param) {
                //print_r($param);
                $field_array[$param->field_mode][$param->field_name] = $param->field;
            }
            //d($field_array);
            return $field_array;
        }
    }*/

    public static function fieldCondition($field)
    {
        global $_DATA;

        $class_name = strtolower(get_class(self));
        if ($field && isset($_DATA[$class_name]) && isset($_DATA[$class_name][$field]))
        {
            return $_DATA[$class_name][$field];
        }
        else
        {
            return false;
        }
    }

    public function getCallingMethod()
    {
        print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
    }

    public function startMeasure()
    {
        echo 'here';
    }

    public function isProjectRelated()
    {
        global $project_slug;

        $class_name = get_class($this);
        if (isset($project_slug) && $project_slug && file_exists(DIR_WWW . 'projects/' . $project_slug . '/settings/modules/' . strtolower($class_name) . '/class.' . strtolower($class_name) . '.php'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected function loadPlugins()
    {
        global $project_slug;

        $class_name = get_class($this);
        if ($this->isProjectRelated())
        //if ($this->$obj->isProjectRelated())
        {
            $base = DIR_WWW . 'projects/' . $project_slug . '/settings/modules/' . strtolower($class_name) . '/plugins/';
        }
        else
        {
            $base = DIR_MODULES . strtolower($class_name) . '/plugins/';
        }

        $plugins = glob($base . '*');
        foreach($plugins as $plugin)
        {
            if (is_dir($plugin))
            {
                $name = basename($plugin, '.php');
                include_once $plugin . '/plugin_' . $name . '.php';
                $className = 'plugin_' . $name;
                if (!empty($this->construct_args))
                {
                    //$obj = new $className(...$this->construct_args);
                    $r = new ReflectionClass($className);
                    $obj = $r->newInstanceArgs($this->construct_args);
                }
                else
                {
                    $obj = new $className();
                }
                $this->plugins[$name] = $obj;

                foreach (get_class_methods($obj) as $method )
                     $this->methods[$method] = $name;

                foreach (get_object_vars($obj) as $var_name => $var_value )
                {
                    if (property_exists($class_name, $var_name))
                    {
                        if (is_array($this->{$var_name}))
                        {
                            $this->{$var_name} = array_merge_recursive($this->{$var_name}, $var_value);
                        }
                        else
                        {
                            $this->{$var_name} = $this->{$var_name} . $var_value;
                        }

                    }
                    else
                    {
                        $this->{$var_name} = $var_value;
                    }
                }
            }
        }
    }

    public function loadProp($prop_name)
    {
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/modules/props.' . strtolower(get_called_class()) . '/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }

    public static function loadCacheSetting($class_name)
    {
        $class_copy = new $class_name;
        return $class_copy->cache_setting;
        //return self::cache_setting;
    }

    public function adminGetAll($project_id, $options = array())
    {
        $where_array = array('trashed' => 0, 'project_id' => $project_id);
        $where_array = array_merge($where_array, $options);

        $admin_content = new Xorm($this);
        $return_content = $admin_content->fields('*')
                                        ->where($where_array)
                                        ->orderBy('position', 'ASC')
                                        ->freshSelect();
        return $return_content;
    }

    public function adminGet($project_id, $id)
    {
        $admin_content = new Xorm($this);
        $return_content = $admin_content->fields('*')
                                        ->where(['project_id' => $project_id, 'id' => $id])
                                        ->limit(1)
                                        ->freshSelect();
        return $return_content;
    }

    public function adminGetModuleMenu($project_id = 0)
    {
        $menu_structure = $this->module_menu_structure;
        $return_menu = '<span class="title-button-group">';

        foreach($menu_structure as $menu_key => $menu_value)
        {
            if ($project_id)
            {
                $menu_value['target'] = str_replace('[project_id]', $project_id, $menu_value['target']);
                $project_id_tag = ' project-id="' . $project_id . '"';
            }
            else
            {
                $menu_value['target'] = str_replace('/[project_id]', '', $menu_value['target']);
                $project_id_tag = '';
            }

            $add_class = '';
            if ($menu_value['target_type'] == 'url')
            {
                $add_class .= ' follow';
            }

            $add_tags = '';
            if ($menu_value['target_type'] == 'modal')
            {
                $add_tags .= ' data-target="' . $menu_value['target'] . '" data-toggle="modal" no-follow="true"';
                $menu_value['target'] = '#';
            }

            $class_tag = ' class-name="' . get_class($this) . '"';

            if (isset($menu_value['prepend']) && $menu_value['prepend'])
            {
                $return_menu .= $menu_value['prepend'];
            }

            $return_menu .= '<a id="' . $menu_key . '" role="button" class="btn btn-secondary' . $add_class . '" href="' . $menu_value['target'] . '" title="' . $menu_value['title'] . '"' . $add_tags . $project_id_tag . $class_tag . '>' . $menu_value['icon'] . '</a>';

            if (isset($menu_value['append']) && $menu_value['append'])
            {
                $return_menu .= $menu_value['append'];
            }
        }

        $return_menu .= '</span>';

        return $return_menu;
    }

    public function adminGetModuleEditMenu($project_id = 0, $content)
    {
        $menu_structure = $this->module_edit_menu_structure;
        $return_menu = '<span class="title-button-group">';

        foreach($menu_structure as $menu_key => $menu_value)
        {
            if ($project_id)
            {
                $menu_value['target'] = str_replace('[project_id]', $project_id, $menu_value['target']);
                $project_id_tag = ' project-id="' . $project_id . '"';
            }
            else
            {
                $menu_value['target'] = str_replace('/[project_id]', '', $menu_value['target']);
                $project_id_tag = '';
            }

            $content_id_tag = ' content-id="' . $content['id'] . '"';
            $class_tag = ' class-name="' . get_class($this) . '"';

            $add_class = '';
            if ($menu_value['target_type'] == 'url')
            {
                $add_class .= ' follow';
            }

            $add_tags = '';
            if ($menu_value['target_type'] == 'modal')
            {
                $add_tags .= ' data-target="' . $menu_value['target'] . '" data-toggle="modal" no-follow="true"';
                $menu_value['target'] = '#';
            }

            $add_tags .= $content_id_tag . $class_tag;

            if ($menu_value['target_type'] == 'function')
            {
                if (isset($menu_value['function'])) $add_class .= ' ' . $menu_value['function'];
                if (isset($content[$menu_value['function_status']]) && $content[$menu_value['function_status']]) $add_class .= ' active';
                $menu_value['target'] = '#';
            }

            $class_tag = ' class-name="' . get_class($this) . '"';

            if (isset($menu_value['prepend']) && $menu_value['prepend'])
            {
                $return_menu .= $menu_value['prepend'];
            }

            $return_menu .= '<a id="' . $menu_key . '" class="btn btn-secondary' . $add_class . '" href="' . $menu_value['target'] . '" title="' . $menu_value['title'] . '"' . $add_tags . $project_id_tag . $class_tag . '>' . $menu_value['icon'] . '</a>';

            if (isset($menu_value['append']) && $menu_value['append'])
            {
                $return_menu .= $menu_value['append'];
            }
        }

        $return_menu .= '</span>';

        return $return_menu;
    }

    public function getTableStructure($table)
    {
        return $this->tables[$table];
    }

    public function getTableFields($project_id, $table_structure)
    {
        $return_array = array();

        foreach ($table_structure['static_fields'] as $field => $value)
        {
            //$return_array[$field] = $field;
            $return_array[$field] = $value;
        }

        if (isset($table_structure['dynamic_fields']) && is_array($table_structure['dynamic_fields']))
        {
            $languages = Languages::getAllProjectLanguages($project_id);

            foreach ($table_structure['dynamic_fields'] as $field => $value)
            {
                foreach ($languages as $language)
                {
                    //$return_array[$field . '_' . $language['code']] = $field . '_' . $language['code'];
                    $return_array[$field . '_' . $language['code']] = $value;
                }
            }
        }

        return $return_array;
    }

    public function wwwGetAll()
    {
        global $project_id, $selected_language;

        $class_name = get_class($this);
        $class = new $class_name;
        $xdb_contents = new Xdb;
        $xdb_contents_rows = $xdb_contents->select_fields('id, image, title_' . $selected_language . ', slug_' . $selected_language . ', description_' . $selected_language)
                                          ->set_table($class->table)
                                          ->where(array('trashed' => 0, 'title_' . $selected_language . '::!=' => ''))
                                          //->where(array('trashed' => 0))
                                          ->order_by('id', 'ASC')
                                          //->limit($page . ', 10')
                                          //->db_select(false);
                                          ->db_select(true, 0, strtolower($class_name));

        foreach ($xdb_contents_rows as $row_key => $row_value)
        {
            if (isset($xdb_contents_rows[$row_key]['image']))
            {
                $image = json_decode(str_replace('&quot;', '"', $row_value['image']), true);

                unset($xdb_contents_rows[$row_key]['image']);
                $xdb_contents_rows[$row_key]['main_image'] = $image[0];
            }

            foreach($static_content->fields['dynamic_fields'] as $dynamic_field => $dynamic_field_value)
            {
                if (isset($xdb_contents_rows[$row_key][$dynamic_field . '_' . $selected_language]))
                {
                    $xdb_contents_rows[$row_key]['dynamic_' . $dynamic_field] = html_entity_decode($xdb_contents_rows[$row_key][$dynamic_field . '_' . $selected_language], ENT_QUOTES, 'UTF-8');
                    unset($xdb_contents_rows[$row_key][$dynamic_field . '_' . $selected_language]);
                }
            }
        }

        return $xdb_contents_rows;
    }

    public function getById($id)
    {
        $admin_content = new Xorm($this);
        $return_content = $admin_content->selectById($id);
        return $return_content;
    }

    public function insertNew($project_id, $parent = 0)
    {
        $class = get_class($this);
        $last_node = new Xorm($class);
        $last_node_row = $last_node->fields(['title'])
                                   ->where(['title::LIKE' => 'New node%'])
                                   ->orderBy('id')
                                   ->limit(1)
                                   ->freshSelect();

        if (is_array($last_node_row) && count($last_node_row))
        {
            //$max_title = $last_node_row[0]['MAX(title)'];
            //$max_title = $last_node_row[0]['title'];
            $max_title = $last_node_row['title'];

            if ($max_title == 'New node'){
                $new_title = 'New node (1)';
            }
            else
            {
                $title_number = str_replace(')', '', str_replace('New node (', '', $max_title));
                $new_number = $title_number + 1;
                $new_title = 'New node (' . $new_number . ')';
            }
        }
        else
        {
            $new_title = 'New node';
        }

        // position

        $max_position = new Xorm($class);
        $max_position_row = $max_position->fields(['position'])
                                         ->where(['parent_id' => $parent, 'project_id' => $project_id])
                                         ->orderBy('position')
                                         ->limit(1)
                                         ->freshSelect();

        if (is_array($max_position_row) && !empty($max_position_row))
        {
            $new_position = $max_position_row['position'] + 1;
        }
        else
        {
            $new_position = 0;
        }

        $new_content = new Xorm($class);
        $last_id = $new_content->insert([
            'project_id' => $project_id,
            'title' => $new_title,
            'parent_id' => $parent,
            'position' => $new_position
        ]);

        $return_array['id'] = $last_id;
        $return_array['title'] = $new_title;
        $return_array['parent'] = $parent;

        echo json_encode($return_array);
    }

    public function updateTitle($project_id, $id, $title)
    {
        $class = get_class($this);
        $title = sanitize(trim($title));
        $project_id = intval($project_id);
        $id = intval($id);

        if (is_int($project_id) && is_int($id))
        {
            $module_update = new Xorm($class);
            $module_update->setFields(['title' => $title])
                          ->where(['id' => $id, 'project_id' => $project_id])
                          ->update();
        }
    }

    public function updateParent($project_id, $id, $parent_id)
    {
        $class = get_class($this);
        $project_id = intval($project_id);
        $id = intval($id);
        $parent_id = intval($parent_id);

        if (is_int($project_id) && is_int($id) && is_int($parent_id))
        {//echo $project_id . ' ' . $id . ' ' . $parent_id;
            $module_update = new Xorm($class);
            $module_update->setFields(['parent_id' => $parent_id])
                          ->where(['id' => $id, 'project_id' => $project_id])
                          ->update();
        }
    }

    public function updateOrder($project_id, $new_order)
    {
        $class = get_class($this);
        $project_id = intval($project_id);
        $order = json_decode($new_order, true);

        foreach ($order as $content_id => $new_order)
        {
            $content_id = intval($content_id);
            $new_order = intval($new_order);

            if (is_int($project_id) && is_int($content_id) && is_int($new_order))
            {
                $module_update = new Xorm($class);
                $module_update->setFields(['position' => $new_order])
                              ->where(['id' => $content_id, 'project_id' => $project_id])
                              ->update();
            }
        }
    }

    public function save($id, $project_id, $new_values)
    {
        /*$xdb_update = new Xdb;
        $update = $xdb_update->set_table($this->table)
                             ->update_fields($project_id, $this->fields, $new_values, 'update')
                             ->where(array('id' => $id))
                             ->db_update(strtolower(get_class($this)), array('trashed'));*/
        //$xdb_static_contents_update->update_permanent_cache_single($blog->table, $id);
        //print_r($_POST);

        $class = get_class($this);
        $project_id = intval($project_id);
        $id = intval($id);

        $project = new Project;
        $project->getById($project_id);

        $module_update = new Xorm($class);
        $module_update->setFields($_POST)
                      ->where(['id' => $id, 'project_id' => $project_id])
                      ->update();
    }

    public function publish($id, $project_id, $published)
    {
        $class = get_class($this);
        $project_id = intval($project_id);
        $id = intval($id);
        $published = intval($published);

        $module_update = new Xorm($class);
        $module_update->setFields(['published' => $published])
                      ->where(['id' => $id, 'project_id' => $project_id])
                      ->update();
    }

    public function trash($id, $project_id, $trashed)
    {
        $class = get_class($this);
        $project_id = intval($project_id);
        $id = intval($id);
        $trashed = intval($trashed);

        $module_update = new Xorm($class);
        $module_update->setFields(['trashed' => $trashed])
                      ->where(['id' => $id, 'project_id' => $project_id])
                      ->update();
    }
}

?>
