<?php

class API
{
    public $ch;
    public $root;
    public $endpoint;

    public function __construct($endpoint)
    {
        getInterfaceId();
        
        $this->ch = curl_init();
        $this->root = 'https://api.xenium.network/v1/';
        $this->endpoint = $endpoint . '/';

        $strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; domain=' . $_SERVER['HTTP_HOST'] . '; path=/';
        session_write_close();

        curl_setopt($this->ch, CURLOPT_URL, $this->root . $this->endpoint);
        curl_setopt($this->ch, CURLOPT_USERAGENT,'Xenium CMS');
        curl_setopt($this->ch, CURLOPT_REFERER, 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($this->ch, CURLOPT_COOKIE, $strCookie);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($this->ch, CURLOPT_CAINFO, $_SERVER['DOCUMENT_ROOT'] . "/admin/system/tools/cert/cacert.pem");
        if (INTERFACE_TOKEN)
        {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array("Interface_Token: " . INTERFACE_TOKEN));
        }

        return $this;
    }

    public function endpoint($endpoint)
    {
        if (strpos($endpoint, '?') === false) {
            $this->endpoint = $endpoint . '/';
        }
        else
        {
            $this->endpoint = $endpoint;
        }
        curl_setopt($this->ch, CURLOPT_URL, $this->root . $this->endpoint);

        return $this;
    }

    public function get($get_data = '')
    {
        if ($get_data)
        {
            if (is_array($get_data)) $get_data = http_build_query($get_data);
            //curl_setopt($this->ch, CURLOPT_POSTFIELDS, $get_data);
            $new_endpoint = $this->endpoint . '?' . $get_data;
            $this->endpoint($new_endpoint);
        }
        $answer = curl_exec($this->ch);
        if (curl_error($this->ch)) {
            //echo curl_error($this->ch);
        }
        //curl_close ($this->ch);

        return $answer;
    }

    public function post($post_data)
    {
        curl_setopt($this->ch, CURLOPT_POST, true);
        if (is_array($post_data)) $post_data = http_build_query($post_data);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_data);

        $answer = curl_exec($this->ch);
        if (curl_error($this->ch)) {
            //echo curl_error($ch);
        }
        //curl_close ($this->ch);

        return $answer;
    }

    public function close()
    {
        curl_close ($this->ch);
    }
}

?>
