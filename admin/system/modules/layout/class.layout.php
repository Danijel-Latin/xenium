<?php

class Layout extends Module
{
    public static function getAll($project_id)
    {
        $project = new Project;
        $project_info = $project->getById($project_id);
        $project_slug = $project_info['project_slug'];
        //echo $condition_json;
        $layouts_url = DIR_PROJECTS . 'project_' . $project_slug . '/layouts/';

        $layout_files = glob($layouts_url . '*.html');
        usort( $layout_files, function( $a, $b ) { return filemtime($a) - filemtime($b); } );

        $layouts_array = ['Default'];

        foreach ($layout_files as $file)
        {
            $file_array = explode('/', $file);
            $filename = end($file_array);
            $filename = trim(str_replace('.html', '', $filename));
            if ($filename != 'Default')
            {
                $layouts_array[] = $filename;
            }
        }

        return $layouts_array;
    }

    public static function generateDropdown($layouts = array())
    {
        $layout_dropdown = '';
        if (!empty($layouts))
        {
            foreach ($layouts as $layout)
            {
                if ($layout == 'Default')
                {
                    $layout_dropdown .= '<a class="dropdown-item change-layout" href="#"><span class="layout-name">' . $layout . '</span><input class="layout-select" type="hidden" value="' . $layout . '"/> <span class="layout-actions"><i class="fa fa-edit edit-layout" layout-name="' . $layout . '" data-toggle="modal" data-target="#layoutModal"></i></span></a>';
                }
                else
                {
                    $layout_dropdown .= '<a class="dropdown-item change-layout" href="#"><span class="layout-name">' . $layout . '</span><input class="layout-select" type="hidden" value="' . $layout . '"/> <span class="layout-actions"><i class="fa fa-edit edit-layout" layout-name="' . $layout . '" data-toggle="modal" data-target="#layoutModal"></i> <i class="fa fa-close delete-layout"></i></span></a>';
                }
            }
        }

        return $layout_dropdown;
    }
}

?>
