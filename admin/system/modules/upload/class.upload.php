<?php

/**
 * @author Danijel
 * @copyright 2013
 */

class Upload
{
    public $file_info;
    public $uploaded_file;
    
    public function get_file_info($file_name)
    {
        $this->uploaded_file = $file_name;
        
        if(function_exists("finfo_open"))
        {
            //$finfo = finfo_open(FILEINFO_MIME_TYPE);
            
            $finfo = finfo_open(FILEINFO_MIME);
            $this->file_info = finfo_file($finfo, $file_name);
            /*
            $file_info = new finfo(FILEINFO_MIME);  // object oriented approach!
            $this->file_info = $file_info->buffer(file_get_contents($file_name));
            echo 'finfo ' . $this->file_info;
            */
        }
        else
        {
            $this->file_info = mime_content_type($file_name);
        }
        return $this;
    }
    
    public function return_type()
    {
        if ($this->file_info)
        {
            $file_type = explode('/', $this->file_info);
            return $file_type[0];
        }
        else
        {
            
        }
    }
    
    public function return_ext()
    {
        $file_ext = explode('/', $this->file_info);
        return $file_type[1];
    }
    
    public function upload_file($file_name)
    {
        $type = $this->get_file_info($file_name)->return_type();
        $ext = $this->get_file_info($file_name)->return_ext();
    }
}

?>