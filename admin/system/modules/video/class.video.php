<?php

/**
 * @author Danijel
 * @copyright 2015
 * @moduleName Video
 * @prepProcessContentSaveFunction prep_process_video
 * @postProcessContentViewFunction post_process_video
 */

class Video extends Module
{
    public function __construct()
    {
        $this->load_plugins();
    }
    
    public function prep_process_video($text)
    {
        $text = str_replace(' aloha-block aloha-block-DefaultBlock', '', $text);
        $text = str_replace(' data-aloha-block-type="DefaultBlock"', '', $text);
        $text = str_replace('data-sortable-item="[object Object]"', '', $text);
        $text = str_replace(' contenteditable="false"', '', $text);
        $text = preg_replace('#\s(id)="[^"]+"#', '', $text);
        return $text;
    }
    
    public function post_process($block_width, $block_height, $settings_array)
    {
        global $project_id;
        
        $project_info = Projects::get_project_info_by_id($project_id);
        
        $include_settings_file = $_SERVER['DOCUMENT_ROOT'] . '/projects/' . $project_info['project_slug'] . '/themes/' . $project_info['theme_slug'] . '/settings/settings.video.php';
        include($include_settings_file);  // gets block_settings
        
        $class_x = $block_settings[$block_width]['width_class'];
        $class_y = $block_settings[$block_height]['height_class'];
        
        $video_url = $settings_array['url'];
        
        if (isset($settings_array['type']) && $settings_array['type'] == 'youtube')
        {
            $replacement = '<video id="vid' . rand() . '" src="" class="video-js vjs-default-skin vjs-big-play-centered ' . $class_x . ' ' . $class_y . '" controls preload="auto" data-setup=\'{ "techOrder": ["youtube"], "src": "' . $video_url . '" }\'></video>';
        }
        
        return $replacement;
    }
}

?>