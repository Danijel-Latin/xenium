<?php

/**
 * @author Danijel
 * @copyright 2016
 */

spl_autoload_register(
   function($className)
   {
       if (strpos($className, 'phpFastCache') !== false)
       {
           $className = str_replace("_", "\\", $className);
           $className = ltrim($className, '\\');
           $fileName = '';
           $namespace = '';
           if ($lastNsPos = strripos($className, '\\'))
           {
               $namespace = substr($className, 0, $lastNsPos);
               $className = substr($className, $lastNsPos + 1);
               $fileName = str_replace('\\', '/', $namespace) . '/';
           }
           $fileName .= str_replace('_', '/', $className) . '.php';

           require $fileName;
       }
    }
);

use phpFastCache\CacheManager;

class plugin_phpFastCache
{
    public $cache;
    public $project_related;
    public $project_info = array();

    public function __construct($project_related = false, $project_info = array())
    {
        //$this->cache = $this->cacheManager();
        if ($project_related)
        {
            $cache_dir = DIR_PROJECTS . "project_" . $project_info['project_slug'] . "/cache";
        }
        else
        {
            $cache_dir = DIR_PROJECTS . "cache";
        }

        $config = array(
            "storage"   => "files",
            "path"      =>  $cache_dir,
        );
        CacheManager::setup($config);
        CacheManager::CachingMethod("phpfastcache");

        $this->cache = CacheManager::getInstance();
    }

    public function cacheManager($type = 'Files')
    {
        //$this->cache = $type
        $manager = new CacheManager;
        return call_user_func_array(array($manager, $type), array());
    }

    public function get($key)
    {
        return $this->cache->get($key);
    }

    public function set($key, $value, $duration = 0)
    {
        return $this->cache->set($key, $value, $duration);
    }

    public function delete($key)
    {
        return $this->cache->delete($key);
    }

    public function flush()
    {
        return $this->cache->clean();
    }

    public function exists($key)
    {
        return $this->cache->isExisting($key);
    }
}
