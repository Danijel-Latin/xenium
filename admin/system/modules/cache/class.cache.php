<?php

/**
 * @author Danijel
 * @copyright 2013
 */

class Cache extends Module
{
    public $plugin;
    public $project_related;
    public $project_info = array();

    public function __construct($project_related = false, $project_info = array())
    {
        $num_args = func_num_args();
        if ($num_args)
        {
            $this->construct_args = func_get_args();
        }
        if ($project_related)
        {
            $this->project_related = $project_related;
            $this->project_info = $project_info;
        }
        $plugin = 'phpfastcache';
        $this->loadPlugins();
    }
}
?>
