<?php

/**
 * @author Danijel
 * @copyright 2015
 * @moduleName Image
 * @prepProcessContentSaveFunction prep_process_image
 * @postProcessContentViewFunction post_process_image
 */

class Image extends Module
{
    public function __construct()
    {
        $this->load_plugins();
    }


    public function prep_process_image($text)
    {
        $text = str_replace(' aloha-block aloha-block-DefaultBlock', '', $text);
        $text = str_replace(' data-aloha-block-type="DefaultBlock"', '', $text);
        $text = str_replace('data-sortable-item="[object Object]"', '', $text);
        $text = str_replace(' contenteditable="false"', '', $text);
        $text = preg_replace('#\s(id)="[^"]+"#', '', $text);
        return $text;
    }

    public function post_process($block_width, $block_height, $settings_array)
    {
        if (isset($settings_array['provider']))
        {
            $replacement = Image::get_provider_image($settings_array, $block_width, $block_height);
        }
        else
        {
            $replacement = Image::get_local_image($settings_array, $block_width, $block_height);
        }

        return $replacement;
    }

    public function post_process_image($text, $group_name, $parent_table, $parent_id)
    {
        global $selected_language, $current_country, $currency, $offer_class, $blog, $image_size, $trim, $title_trim, $exclusion_list, $tb_counter, $previous_group_name, $block_size;

        if (!isset($previous_group_name))
        {
            $previous_group_name = '';
        }

        if (!isset($tb_counter) || $previous_group_name != $group_name);
        {
            $tb_counter = 0;
        }

        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        if ($text)
        {
            $dom = new DOMDocument('1.0', 'UTF-8');
            @$dom->loadHTML('<?xml encoding="UTF-8">' . $text);
            $dom->encoding = 'utf-8';
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = false;
            $divs = $dom->getElementsByTagName('div');

            $text = $dom->saveXML();

            $filling = 0;

            $replacement_front = '<div class="row">';
            $replacement_end = '</div>';

            //$blocks_num = count($divs); echo 'blocks ' . $blocks_num;
            //$current_block_num = 0;

            foreach($divs as $div)
            {
                $tb_class = $div->getAttribute('class');
                $class_array = explode(' ', $tb_class);

                if (in_array('imageblock', $class_array))
                {
                    foreach ($class_array as $class)
                    {
                        if (strpos($class,'col-md-') !== false)
                        {
                            $block_size = str_replace('col-md-', '', $class);
                        }
                        else
                        {
                            $block_size = 3;
                        }

                        if (strpos($class,'col-ver-') !== false)
                        {
                            $block_height = str_replace('col-ver-', '', $class);
                        }
                        else
                        {
                            $block_height = 100;
                        }
                        /*
                        $file = new File;
                        if (method_exists($file, $class . '_post_process'))
                        {
                            return call_user_func_array(array($file, $method_name), $params);
                            break;
                        }
                        */
                    }
                }


                /*
                if (strpos($tb_class,'col1') !== false)
                {
                    $block_size = 1;
                    $add_filling = 3;
                    $row_type = 'quarters';
                }

                if (strpos($tb_class,'col2') !== false)
                {
                    $block_size = 2;
                    $add_filling = 6;
                    $row_type = 'quarters';
                }

                if (strpos($tb_class,'col3') !== false)
                {
                    $block_size = 3;
                    $add_filling = 9;
                    $row_type = 'quarters';
                }

                if (strpos($tb_class,'col4') !== false)
                {
                    $block_size = 4;
                    $add_filling = 12;
                    $row_type = 'quarters';
                }

                if (strpos($tb_class,'col-one-third') !== false)
                {
                    $block_size = 'one-third';
                    $add_filling = 4;
                    $row_type = 'thirds';
                }

                if (strpos($tb_class,'col-two-thirds') !== false)
                {
                    $block_size = 'two-thirds';
                    $add_filling = 8;
                    $row_type = 'thirds';
                }
                */




                if (strpos($tb_class,'imageblock') !== false)
                {
                    $settings = $div->firstChild->nodeValue;
                    $settings_array = json_decode($settings, true);

                    if (isset($settings_array['provider']))
                    {
                        $replacement = Image::get_provider_image($settings_array, $block_size, $block_height);
                    }
                    else
                    {
                        $replacement = Image::get_local_image($settings_array, $block_size, $block_height);
                    }

                    $replacable = $dom->saveXML($div);
                    $text = str_replace($replacable, $replacement, $text);

                    /*
                    $replacable = $dom->saveXML($div);
                    echo $replacable;
                    echo '
                    ';
                    echo $text;*/

                    //print_r($offer);
                }


            }

            //echo $text;

            $text = str_replace('</body></html>', '', $text);
            $text = str_replace('<html><body>', '', $text);
            $text = str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">', '', $text);
            $text = str_replace('<?xml version="1.0" standalone="yes"?>', '', $text);
            $text = str_replace('<?xml version="1.0" encoding="utf-8" standalone="yes"?>', '', $text);
            $text = str_replace('<?xml encoding="UTF-8"?>', '', $text);
        }

        //$offer_counter++;
        //echo $offer_counter;
        $previous_group_name = $group_name;
        /*echo '

        ' . $text;*/
        return $text;
    }

    public function get_provider_image($settings, $block_width = '', $block_height = '', $module = 'image')
    {
        global $project_id;
        //echo 'proj' . $project_id . 'proj';

        $project_info = Projects::get_project_info_by_id($project_id);

        $include_settings_file = $_SERVER['DOCUMENT_ROOT'] . '/projects/' . $project_info['project_slug'] . '/themes/' . $project_info['theme_slug'] . '/settings/settings.' . $module . '.php';
        include($include_settings_file);  // gets block_settings

        $resize_x = $block_settings[$block_width]['width_image'];
        $resize_y = $block_settings[$block_height]['height_image'];

        $class_x = $block_settings[$block_width]['width_class'];
        $class_y = $block_settings[$block_height]['height_class'];

        if (!isset($block_settings['resize_mode']))
        {
            $resize_mode = 6;
            $resize_mode_name = 'ZEBRA_IMAGE_CROP_CENTER';
        }
        else
        {
            $resize_mode = $block_settings['resize_mode'];
            if ($resize_mode == 0)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_BOXED';
            }
            elseif ($resize_mode == 1)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_NOT_BOXED';
            }
            elseif ($resize_mode == 2)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_CROP_TOPLEFT';
            }
            elseif ($resize_mode == 3)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_CROP_TOPCENTER';
            }
            elseif ($resize_mode == 4)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_CROP_TOPRIGHT';
            }
            elseif ($resize_mode == 5)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_CROP_MIDDLELEFT';
            }
            elseif ($resize_mode == 6)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_CROP_CENTER';
            }
            elseif ($resize_mode == 7)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_CROP_MIDDLERIGHT';
            }
            elseif ($resize_mode == 8)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_CROP_BOTTOMLEFT';
            }
            elseif ($resize_mode == 9)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_CROP_BOTTOMCENTER';
            }
            elseif ($resize_mode == 10)
            {
                $resize_mode_name = 'ZEBRA_IMAGE_CROP_BOTTOMRIGHT';
            }
        }

        $id = $settings['id'];
        $provider = $settings['provider'];

        $tmp_folder = $_SERVER['DOCUMENT_ROOT'] . '/uploads/image/tmp/';

        if (!file_exists($tmp_folder))
        {
            File::create_dir($tmp_folder);
        }

        $tmp_file = $tmp_folder . $id . '.' . $settings['ext'];

        $resized_folder = $tmp_folder . $resize_x . 'x' . $resize_y . '_' . $resize_mode_name . '/';
        $resized_folder_name = $resize_x . 'x' . $resize_y . '_' . $resize_mode_name;
        $resized_file = $resized_folder  . $id . '.' . $settings['ext'];

        $file = new File;

        if (!file_exists($resized_file))
        {
            $params = array();
            $params['id'] = $id;
            $method_name = $provider . '_get_file_url';


            $file_url = call_user_func_array(array($file, $method_name), $params);

            $file_contents = file_get_contents_curl($file_url);

            file_put_contents($tmp_file, $file_contents);

            $image = new Zebra_Image();
            $image->source_path = $tmp_file;



            if (!file_exists($resized_folder))
            {
                File::create_dir($resized_folder);
            }

            $image->target_path = $resized_file;
            if (!$image->resize($resize_x, $resize_y, $resize_mode)) show_error($image->error, $image->source_path, $image->target_path);
            //readfile($resized_file);
        }

        //print file_get_contents($resized_file);

        $settings = $file->get_upload_settings($project_id);
        //print_r($settings);
        $method_name = 'transfer_' . $settings['storage_handler'];
        $params = array($settings['storage_settings'], $resized_folder_name, $resized_file, 'image');

        $return_result = call_user_func_array(array($file, $method_name), $params);
        //print_r($return_result);
        /*
        $file = new File($resized_file);
        $method_name = 'transfer_' . $settings['provider'];
        $return_result = call_user_func_array(array($file, $method_name), $params);
        */

        if ($module == 'image')
        {
            return '<img src="' . $return_result['fileUrl'] . '" class="' . $class_x . ' ' . $class_y . '" />';
        }
        else
        {
            return '<img src="' . $return_result['fileUrl'] . '" />';
        }
    }

    public function get_local_image($settings)
    {
        if (isset($settings['image_file']))
        {
            return '<img src="' . $settings['image_file'] . '" />';
        }
        else
        {
            return '<img src="/uploads/image/' . $settings['fileYear'] . '/' . $settings['fileMonth'] . '/' . $settings['fileName'] . '" />';
        }
    }
}

?>
