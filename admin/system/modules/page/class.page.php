<?php

/**
 * @author Danijel
 * @copyright 2015
 */

class Page extends Module
{
    public $table;
    public $fields;

    public $list_prop;
    public $edit_prop;

    public $module_name;
    public $module_menu_structure = array();
    public $module_edit_menu_structure = array();

    public function __construct()
    {
        $this->module_name = 'Pages';

        $this->module_menu_structure =
        array(
            'viewAll' => array(
                //'target' => '/admin/interface/[project_id]/contents/viewAll/page/',
                'target' => '/admin/interface/project-[project_id]/interface-contents/',
                'target_type' => 'url',
                //'icon' => '<i class="icon icon-linea-software-10-17"></i>',
                'icon' => '<i class="ton-li-server-1"></i>',
                'title' => 'View all pages',
                'load_prop' => 'template_main_container_treeview',
                'load_prop_settings' => array(),
                'prepend' => '',
                'append' => '<span class="separator"></span>'
            ),
            'new' => array(
                'target' => '#',
                'target_type' => 'function',
                //'icon' => '<i class="icon icon-linea-basic-10-101"><sup><i class="fa fa-certificate"></i></sup></i>',
                'icon' => '<i class="icon icon-linea-basic-10-101"><sub>&#xe00f;</sub></i>',
                'title' => 'Create a new page'
            ),
            'newLink' => array(
                'target' => '#',
                'target_type' => 'function',
                'icon' => '<i class="icon icon-linea-basic-10-56"><sub>&#xe00f;</sub></i>',
                'title' => 'Create a new link',
                'append' => '<span class="separator"></span>'
            ),
            'viewTrashed' => array(
                'target' => '/admin/interface/project-[project_id]/interface-contents/module-page/action-viewTrashed/',
                'target_type' => 'url',
                'icon' => '<i class="icon icon-linea-basic-10-118"></i>',
                'title' => 'View trashed content',
                'load_prop' => 'template_main_container_treeview'
            ),
            'history' => array(
                'target' => '/admin/interface/project-[project_id]/interface-contents/module-page/action-viewHistory/',
                'target_type' => 'url',
                'icon' => '<i class="icon icon-linea-basic-10-1"></i>',
                'title' => 'View editing history',
                'load_prop' => 'template_main_container_history'
            )
        );

        $this->module_edit_menu_structure =
        array(
            'viewAll' => array(
                //'target' => '/admin/interface/[project_id]/contents/viewAll/page/',
                'target' => '/admin/interface/project-[project_id]/interface-contents/',
                'target_type' => 'url',
                'icon' => '<i class="ton-li-server-1"></i>',
                'title' => 'View all pages',
                'load_prop' => 'template_main_container_treeview',
                'load_prop_settings' => array(),
                'prepend' => '',
                'append' => '<span class="separator"></span>'
            ),
            'published' => array(
                'target' => '#',
                'target_type' => 'function',
                'function' => 'publish',
                'function_status' => 'published',
                'icon' => '<i class="icon icon-linea-basic-10-29"></i>',
                'title' => 'Published status'
            ),
            'trashed' => array(
                'target' => '#',
                'target_type' => 'function',
                'function' => 'trash',
                'function_status' => 'trashed',
                'icon' => '<i class="icon icon-linea-basic-10-118"></i>',
                'title' => 'Trashed status'
            ),
            'history' => array(
                'target' => '/admin/interface/project-[project_id]/interface-contents/module-page/action-viewHistory/',
                'target_type' => 'url',
                'icon' => '<i class="icon icon-linea-basic-10-1"></i>',
                'title' => 'View editing history',
                'load_prop' => 'template_main_container_history'
            )
        );

        $this->www_pages = Field::structure(
            // ststic fields
            Field::stat()->neki->input('Title'),
            Field::stat()->project_id->hiddenSmallint(),
            Field::stat()->parent_id->hiddenSmallint(),
            Field::stat()->position->hiddenSmallint(),
            Field::stat()->type->hiddenInput(10),
            Field::stat()->title->hiddenInput(255),
            Field::stat()->slug->hiddenInput(255),
            Field::stat()->published->hiddenBool(),
            Field::stat()->trashed->hiddenBool(),
            // dynamic fields
            Field::dyn()->title->richInput('Title'),
            Field::dyn()->slug->hiddenInput(255),
            Field::dyn()->description->richTextarea('Description'),
            Field::dyn()->seo_description->textarea('SEO description')
        );
/*
        $this->table = 'www_pages';

        $this->fields =
        array(
        'static_fields' =>
            array(
                'project_id' => array('value' => '', 'field_type' => 'INT NOT NULL', 'index' => true, 'admin_editable' => false),
                'parent_id' => array('value' => '', 'field_type' => 'INT NOT NULL', 'index' => true, 'admin_editable' => false),
                'position' => array('value' => '', 'field_type' => 'INT NOT NULL', 'index' => true, 'admin_editable' => false),
                'type' => array('value' => '', 'field_type' => 'VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL', 'index' => true, 'admin_editable' => false),
                'title' => array('value' => '', 'field_type' => 'VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL', 'index' => true, 'admin_editable' => false),
                'slug' => array('value' => '', 'field_type' => 'VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL', 'index' => true, 'admin_editable' => false),
                'published' => array('value' => '', 'field_type' => 'INT NOT NULL', 'index' => true, 'admin_editable' => false),
                'trashed' => array('value' => '', 'field_type' => 'INT NOT NULL', 'index' => true, 'admin_editable' => false)
            ),
        'dynamic_fields' =>
            array(
                'title' => array(
                    'value' => '',
                    'field_type' => 'VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
                    'admin_editable' => true,
                    'html' => array(
                        'type' => 'input',
                        'label' => 'Title:'),
                    'html_attributes' => array(
                        'class' => 'html_edit_simple  form-control',
                        'type' => 'text')),
                'slug' => array(
                    'value' => '',
                    'field_type' => 'VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
                    'admin_editable' => false),
                'description' => array(
                    'value' => '',
                    'field_type' => 'TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
                    'admin_editable' => true,
                    'html' => array(
                        'type' => 'textarea',
                        'label' => 'Description:'),
                    'html_attributes' => array(
                        'class' => 'col-md-12  html_edit_advanced rows12 form-control',
                        'rows' => 16,
                        'type' => 'text')),
                'seo_description' => array(
                    'value' => '',
                    'field_type' => 'VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
                    'admin_editable' => true,
                    'html' => array(
                        'type' => 'textarea',
                        'label' => 'SEO description:'),
                    'html_attributes' => array(
                        'class' => 'col-md-12 rows12 form-control',
                        'type' => 'text')),
                'seo_keywords' => array(
                    'value' => '',
                    'field_type' => 'VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
                    'admin_editable' => true,
                    'html' => array(
                        'type' => 'input',
                        'label' => 'SEO keywords:'),
                    'html_attributes' => array(
                        'class' => 'form-control',
                        'type' => 'text')),
            )
        );
*/
        $this->loadPlugins();

        $this->cache_setting = true;
    }

    public function adminGetContents($project_id, $options)
    {
        return $this->adminGetTree($project_id, $options);
    }

    public function adminGetContent($project_id, $id)
    {
        return $this->adminGet($project_id, $id);
    }

    public function adminGetTree($project_id, $options)
    {
        $this->project_id = $project_id;
        $contents = $this->adminGetAll($project_id, $options);
        return $this->buildTree($contents);
    }

    public function buildTree(array $elements, $parent_id = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parent_id) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    public function adminRenderTree($rows, $parent = 0)
    {
        $class_name = get_class($this);
        $result = "<ul>";
        foreach ($rows as $row)
        {
            if ($row['parent_id'] == $parent)
            {
                $result.= "<li id='page_" . $row['id'] . "' content-id='" . $row['id'] . "' project-id='" . $row['project_id'] . "' class-name='" . $class_name . "' data-jstree='{\"icon\":\"icon icon-linea-basic-10-101\"}'><span class='branch-title'>" . $row['title'] . "</span><span class='fa fa-pencil branch-title-edit'></span>";
                if ($this->hasChildren($row,$row['id'])) $result.= $this->adminRenderTree($row['children'],$row['id']);
                $published = '';
                if ($row['published']) $published = 'active';
                $result.= '<div class="actions">
                    <a href="/admin/interface/project-' . $row['project_id'] . '/interface-contents/module-page/action-edit/id-' . $row['id'] . '/" role="button" class="btn btn-secondary follow" title="Edit"><!--<i class="fa fa-edit"></i>--><i class="icon icon-linea-software-10-59"></i></a>
                    <button type="button" class="btn btn-secondary publish-content ' . $published . '" content-id="' . $row['id'] . '" project-id="' . $row['project_id'] . '" class-name="' . $class_name . '" title="Published"><!--<i class="fa fa-eye"></i>--><i class="icon icon-linea-basic-10-29"></i></button>
                    <button type="button" class="btn btn-secondary trash-content" content-id="' . $row['id'] . '" project-id="' . $row['project_id'] . '" class-name="' . $class_name . '" title="Remove"><!--<i class="fa fa-remove"></i>--><i class="icon icon-linea-arrows-10-57"></i></button>
                </div></li>';
            }
        }
        $result.= "</ul>";

        return $result;
    }

    public function hasChildren($row,$id)
    {
        if (isset($row['children']) && is_array($row['children']) && !empty($row['children'])) return true; else return false;
    }

    /*public function save($id, $project_id, $new_values)
    {
        return json_encode(['answer' => 'success']);
    }*/

    /*------*
    CONDITONS
    *------*/

    // Category
    public function moduleCondition_Category($value, $operator = 'equal')
    {

    }

    public function moduleCondition_Category_operators()
    {
        return ['equal', 'not_equal', 'in', 'not_in'];
    }

    //public function
}

 ?>
