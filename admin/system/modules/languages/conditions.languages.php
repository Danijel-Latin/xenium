<?php

if ($project_id)
{
    $project_languages = Languages::getAllProjectLanguages($project_id);

    $languages_arr = array();
    foreach ($project_languages as $language)
    {
        $languages_arr[$language['code']] = $language['name'];
    }

    Condition::add([
        'id' => 'Languages->check',
        'label' => 'Language is',
        'type' => 'string',
        'input' => 'radio',
        'values' => $languages_arr,
        'operators' => ['equal', 'not_equal']
    ]);

    Condition::add([
        'id' => 'Languages->isDefault',
        'label' => 'Language is default',
        'type' => 'integer',
        'input' => 'radio',
        'values' => ['false', 'true'],
        'operators' => ['equal']
    ]);

    Condition::add([
        'id' => 'Languages->hasDomain',
        'label' => 'Language has a domain',
        'type' => 'integer',
        'input' => 'radio',
        'values' => ['false', 'true'],
        'operators' => ['equal']
    ]);
}

?>
