<?php
/*
 * This file is part of the DebugBar package.
 *
 * (c) 2013 Maxime Bouroumeau-Fuseau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DebugBar\DataFormatter;

use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;

class DataFormatter implements DataFormatterInterface
{
    public function __construct()
    {
        $this->cloner = new VarCloner();
        $this->dumper = new CliDumper();
    }

    public function formatVar($data)
    {
        $output = '';

        $this->dumper->dump(
            $this->cloner->cloneVar($data),
            function ($line, $depth) use (&$output) {
                // A negative depth means "end of dump"
                if ($depth >= 0) {
                    // Adds a two spaces indentation to the line
                    $output .= str_repeat('  ', $depth).$line."\n";
                }
            }
        );

        return trim($output);
    }

    public function formatXorm($data)
    {
        $output = '';

        /*$this->dumper->dump(
            $this->cloner->cloneVar($data),
            function ($line, $depth) use (&$output) {
                // A negative depth means "end of dump"
                if ($depth >= 0) {
                    // Adds a two spaces indentation to the line
                    $output .= str_repeat('  ', $depth).$line."\n";
                }
            }
        );*/

        $query = key($data);

        $main_syntax = ['SELECT', 'FROM', 'LEFT', 'RIGHT', 'INNER', 'OUTER', 'JOIN', 'UNION', 'ON', 'WHERE', 'FULL'];
        $condition_syntax = ['AND', 'OR'];
        $query_colored = $query;
        foreach ($main_syntax as $syntax)
        {
            $query_colored = str_replace($syntax, '<span style="color: #800000; font-weight: bold;">' . $syntax . '</span>', $query_colored);
        }
        foreach ($condition_syntax as $c_syntax)
        {
            $query_colored = str_replace($c_syntax, '<span style="color: #808030; font-weight: bold;">' . $c_syntax . '</span>', $query_colored);
        }

        // output calling file
        $calling_file = '<div class="calling-file">Called from <span style="font-weight: bold;">"' . $data[$query]['calling_file']['file'] . '"</span> on line <span style="font-weight: bold;">' . $data[$query]['calling_file']['line'] . '</span></div>';

        // output tables
        $tables = '<div class="xorm-debugger-parent" onclick="toggle_slide(event, this);"><div class="xorm-debugger-title"><i class="fa fa-table"></i> Tables (' . count($data[$query]['tables']) . ') <i class="fa fa-caret-right"></i></div><div class="xorm-debugger-slide">';
        foreach ($data[$query]['tables'] as $table_value)
        {
            $query_colored = str_replace($table_value, '<span style="color: #0000FF;">' . $table_value . '</span>', $query_colored);
            $tables .= '<div class="xorm-debugger-child"><span style="color: #0000FF;">' . $table_value . '</span></div>';
        }
        $tables .= '</div></div>';

        // output table joins
        $table_joins = '';
        if (count($data[$query]['table_joins']))
        {
            $table_joins = '<div class="xorm-debugger-parent" onclick="toggle_slide(event, this);"><div class="xorm-debugger-title"><i class="fa fa-sitemap table-joins"></i> Table joins (' . count($data[$query]['table_joins']) . ') <i class="fa fa-caret-right"></i></div><div class="xorm-debugger-slide">';
            foreach ($data[$query]['table_joins'] as $table_joins_value)
            {
                //$query_colored = str_replace($table_joins_value, '<span style="color: #0000FF;">' . $table_joins_value . '</span>', $query_colored);
                foreach ($main_syntax as $syntax)
                {
                    $table_joins_value = str_replace($syntax, '<span style="color: #800000; font-weight: bold;">' . $syntax . '</span>', $table_joins_value);
                }
                foreach ($condition_syntax as $c_syntax)
                {
                    $table_joins_value = str_replace($c_syntax, '<span style="color: #808030; font-weight: bold;">' . $c_syntax . '</span>', $table_joins_value);
                }
                foreach ($data[$query]['tables'] as $table_value)
                {
                    $table_joins_value = str_replace($table_value, '<span style="color: #0000FF;">' . $table_value . '</span>', $table_joins_value);
                }
                $table_joins .= '<div class="xorm-debugger-child">' . $table_joins_value . '</div>';
            }
            $table_joins .= '</div></div>';
        }

        // output placeholders
        $placeholders = '';
        if (count($data[$query]['placeholders']))
        {
            $placeholders = '<div class="xorm-debugger-parent" onclick="toggle_slide(event, this);"><div class="xorm-debugger-title"><i class="fa fa-sign-in"></i> Placeholders (' . count($data[$query]['placeholders']) . ') <i class="fa fa-caret-right"></i></div><div class="xorm-debugger-slide">';
            foreach ($data[$query]['placeholders'] as $placeholder_key => $placeholder_value)
            {
                $query_colored = str_replace($placeholder_key, '<span style="color: #880015; font-weight: bold;">' . $placeholder_key . '</span>', $query_colored);

                // get var_dump of placeholder value
                $placeholder_value_vardump = var_debug($placeholder_value, 25);

                $placeholders .= '<div class="xorm-debugger-child"><span style="color: #880015; font-weight: bold;">' . $placeholder_key . '</span> => ' . $placeholder_value . '<span class="xorm-vardump">' . $placeholder_value_vardump . '</span><div class="clear"></div></div>';
            }
            $placeholders .= '</div></div>';
        }

        $output = $query_colored . $calling_file . $tables . $table_joins . $placeholders;

        $result_counter = 0;

        // output results
        $output .= '<div class="xorm-debugger-parent" onclick="toggle_slide(event, this);"><div class="xorm-debugger-title"><i class="fa fa-list-ul"></i> Results ([RESULT_COUNTS]) <i class="fa fa-caret-right"></i><span class="xorm-preview">[RESULT_PREVIEW_MAIN]</span></div><div class="xorm-debugger-slide">';
        $result_preview_main = '';
        foreach ($data[$query]['results'] as $key => $result)
        {
            //$result_key = key($result);
            //$output .= '<div class="xorm-debugger-child"><span style="color: #880015; font-weight: bold;">' . $result_key . '</span> => ' . $result[$result_key] . '</div>';

            $result_vardump = var_debug($result, 25);
            $result_counter++;
            $result_counter_array = false;

            $output .= '<div class="xorm-debugger-child">';
            $output .= '<div class="xorm-debugger-parent" onclick="toggle_slide(event, this);"><div class="xorm-debugger-title"> ' . $key . ' <i class="fa fa-caret-right"></i><span class="xorm-preview">[RESULT_PREVIEW]</span><span class="xorm-vardump">' . $result_vardump . '</span></div><div class="xorm-debugger-slide">';

            $result_preview = '[';
            $result_preview_id = false;
            foreach ($result as $result_key => $result_value)
            {
                // get var_dump of result value
                $result_value_vardump = var_debug($result_value, 25);
                if (!is_array($result_value))
                {
                    $result_value = htmlspecialchars($result_value);
                    $output .= '<div class="xorm-debugger-child"><span style="color: #880015; font-weight: bold;">' . $result_key . '</span> => ' . $result_value . '<span class="xorm-vardump">' . $result_value_vardump . '</span><div class="clear"></div></div>';
                    $result_preview .= ' "' . $result_key . '" => "' . $result_value . '" ';
                }
                else
                {
                    //$result_preview_id = false;
                    $output .= '<div class="xorm-debugger-child">';
                    $output .= '<div class="xorm-debugger-parent" onclick="toggle_slide(event, this);"><div class="xorm-debugger-title"> ' . $result_key . ' <i class="fa fa-caret-right"></i><span class="xorm-preview">[RESULT_PREVIEW_CHILD]</span><span class="xorm-vardump">' . $result_value_vardump . '</span></div><div class="xorm-debugger-slide">';
                    //$output .= '<div class="xorm-debugger-child"><span style="color: #880015; font-weight: bold;">' . $result_key . '</span> => ' . json_encode($result_value) . '<span class="xorm-vardump">' . $result_value_vardump . '</span><div class="clear"></div></div>';
                    $result_preview_child_prepend = ' "id" => "' . $key . '"';
                    $result_preview_child = ' [';

                    foreach($result_value as $result_m_key => $result_m_value)
                    {
                        // get var_dump of result value
                        $result_m_value_vardump = var_debug($result_m_value, 25);
                        $result_m_value = htmlspecialchars($result_m_value);
                        $output .= '<div class="xorm-debugger-child"><span style="color: #880015; font-weight: bold;">' . $result_m_key . '</span> => ' . $result_m_value . '<span class="xorm-vardump">' . $result_m_value_vardump . '</span><div class="clear"></div></div>';
                        $result_preview_child .= ' "' . $result_m_key . '" => "' . $result_m_value . '" ';
                    }
                    $result_preview_child .= '] ';
                    if (!$result_preview_id)
                    {
                        $result_preview .= $result_preview_child_prepend . $result_preview_child;
                        $result_preview_id = true;
                    }
                    else
                    {
                        $result_preview .= $result_preview_child;
                    }
                    $output = str_replace('[RESULT_PREVIEW_CHILD]', trim_text($result_preview_child, 100), $output);
                    $output .= '</div></div>';
                    $output .= '</div>';
                    $result_counter++;
                    $result_counter_array = true;
                }
            }
            $result_preview .= ']';
            $result_preview_main .= $result_preview;
            $output .= '</div></div>';
            $output .= '</div>';
            $output = str_replace('[RESULT_PREVIEW]', trim_text($result_preview, 100), $output);

            if ($result_counter_array) $result_counter = $result_counter - 1; $result_counter_array = false;
        }
        $output .= '</div></div>';

        $output = str_replace('[RESULT_PREVIEW_MAIN]', trim_text($result_preview_main, 100), $output);
        $output = str_replace('[RESULT_COUNTS]', $result_counter, $output);

        // output error if any
        $error = '';
        if (isset($data[$query]['error']))
        {
            $error = '<div class="xorm-debugger-parent" onclick="toggle_slide(event, this);"><div class="xorm-debugger-title"><i class="fa fa-times-circle"></i> Error <i class="fa fa-caret-right"></i><span class="xorm-preview">' . trim_text($data[$query]['error'], 100) . '</span></div><div class="xorm-debugger-slide">';
            $error .= '<div class="xorm-debugger-child">' . $data[$query]['error'] . '</div>';
            $error .= '</div></div>';
        }

        $output .= $error;

        return trim($output);
    }

    public function formatDuration($seconds)
    {
        if ($seconds < 0.001) {
            return round($seconds * 1000000) . 'μs';
        } elseif ($seconds < 1) {
            return round($seconds * 1000, 2) . 'ms';
        }
        return round($seconds, 2) . 's';
    }

    public function formatBytes($size, $precision = 2)
    {
        if ($size === 0 || $size === null) {
            return "0B";
        }
        $base = log($size) / log(1024);
        $suffixes = array('B', 'KB', 'MB', 'GB', 'TB');
        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }
}
