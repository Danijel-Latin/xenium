<?php

namespace DebugBar\DataCollector;

use DebugBar\DataFormatter\DataFormatterInterface;

class XormCollector extends DataCollector implements Renderable
{
    protected $name;

    protected $queries = array();

    public function __construct($name = 'queries')
    {
        $this->name = $name;
    }

    /*public function collect()
    {
        return array("uniqid" => uniqid());
    }*/

    public function getName()
    {
        return 'xorm';
    }

    public function setDataFormatter(DataFormatterInterface $formater)
    {
        $this->dataFormater = $formater;
        return $this;
    }

    public function getDataFormatter()
    {
        if ($this->dataFormater === null) {
            $this->dataFormater = DataCollector::getDefaultDataFormatter();
        }
        return $this->dataFormater;
    }

    /*public function getWidgets()
    {
        return array(
            "queries" => array(
                "icon" => "database",
                "tooltip" => "uniqid()",
                "widget" => "PhpDebugBar.Widgets.XormWidget",
                "map" => "uniqid",
                "default" => "''"
            )
        );
    }*/

    //public function getAssets()
    //{
        /*return array(
            'css' => 'widgets/sqlqueries/widget.css',
            'js' => 'widgets/sqlqueries/widget.js'
        );*/
    //}

    public function addQuery($query, $label = 'info', $isString = true)
    {
        if (!is_string($query)) {
            //$query = $this->getDataFormatter()->formatVar($query);

            $query_key = key($query);
            $execution_time = $query[$query_key]['execution']['time'];
            $execution_memory = $query[$query_key]['execution']['memory'];
            $execution_source = $query[$query_key]['execution']['source'];

            $query = $this->getDataFormatter()->formatXorm($query);
            $isString = true;
        }
        $this->queries[] = array(
            'query' => $query,
            'is_string' => $isString,
            'label' => $label,
            'execution_time' => $execution_time,
            'execution_memory' => $execution_memory,
            'execution_source' => $execution_source,
            'time' => microtime(true)
        );
    }

    public function collect()
    {
        $queries = $this->getQueries();
        return array(
            'count' => count($queries),
            'queries' => $queries
        );
    }

    public function getQueries()
    {
        $queries = $this->queries;
        /*foreach ($this->aggregates as $collector) {
            $msgs = array_map(function ($m) use ($collector) {
                $m['collector'] = $collector->getName();
                return $m;
            }, $collector->getMessages());
            $messages = array_merge($messages, $msgs);
        }*/

        // sort messages by their timestamp
        usort($queries, function ($a, $b) {
            if ($a['time'] === $b['time']) {
                return 0;
            }
            return $a['time'] < $b['time'] ? -1 : 1;
        });

        return $queries;
    }

    public function getWidgets()
    {
        $name = $this->getName();
        return array(
            "$name" => array(
                'icon' => 'database',
                "widget" => "PhpDebugBar.Widgets.XormWidget",
                "map" => "$name.queries",
                "default" => "[]"
            ),
            "$name:badge" => array(
                "map" => "$name.count",
                "default" => "null"
            )
        );
    }
}

?>
