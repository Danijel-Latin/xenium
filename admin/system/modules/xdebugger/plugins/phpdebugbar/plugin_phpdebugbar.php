<?php

/**
 * @author Danijel
 * @copyright 2016
 */

 spl_autoload_register(
    function($className)
    {
        if (strpos($className, 'DebugBar') !== false || strpos($className, 'Psr') !== false || strpos($className, 'VarDumper') !== false)
        {
            $className = str_replace("_", "\\", $className);
            $className = ltrim($className, '\\');
            $fileName = '';
            $namespace = '';
            if ($lastNsPos = strripos($className, '\\'))
            {
                $namespace = substr($className, 0, $lastNsPos);
                $className = substr($className, $lastNsPos + 1);
                $fileName = str_replace('\\', '/', $namespace) . '/';
            }
            $fileName .= str_replace('_', '/', $className) . '.php';

            require $fileName;
        }
     }
);

use DebugBar\StandardDebugBar;

class plugin_PHPDebugBar
{
    public function debug()
    {
        global $_DEBUG;

        $debugbar = new StandardDebugBar();
        $debugbarRenderer = $debugbar->getJavascriptRenderer();

        echo $debugbarRenderer->renderHead();

        if (isset($_DEBUG['messages']) && !empty($_DEBUG['messages']))
        {
           foreach ($_DEBUG['messages'] as $message)
           {
               if (!isset($message['label'])) $message['label'] = 'Info';
               if (!isset($message['backtrace'])) $message['backtrace'] = array();
               if (is_string($message['text']))
               {
                   $debugbar["messages"]->addMessage($message['text'], $message['label']);
               }
               else
               {
                   $debugbar["messages"]->addMessage($message['text'], $message['label'], false, $message['backtrace']);
               }
           }
        }

        if (isset($_DEBUG['measures']) && !empty($_DEBUG['measures']))
        {
            foreach ($_DEBUG['measures'] as $measure)
            {
                $debugbar["time"]->addMeasure($measure['label'], $measure['start'], $measure['end'], $measure['params'], $measure['collector']);
            }
        }

        if (isset($_DEBUG['exceptions']) && !empty($_DEBUG['exceptions']))
        {
            foreach ($_DEBUG['exceptions'] as $exception)
            {
                $debugbar["exceptions"]->addException($exception);
            }
        }

        if (isset($_DEBUG['xorm']) && !empty($_DEBUG['xorm']))
        {
            foreach ($_DEBUG['xorm'] as $xorm_query)
            {
                $xorm_values = reset($xorm_query);
                $xorm_label = 'info';
                if (isset($xorm_values['error'])) $xorm_label = 'error';
                $debugbar["xorm"]->addQuery($xorm_query, $xorm_label);
            }
        }

        echo $debugbarRenderer->render();
    }
}

?>
