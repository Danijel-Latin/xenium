<?php

/**
 * @author Danijel
 * @copyright 2016
 *
 * @moduleCategory dynamic_content
 * @moduleName Blog
 * @moduleTextEditor true
 * @moduleTextEditorModal true
 * @prepProcessContentSaveFunction prep_process_blog
 * @postProcessContentViewFunction post_process_blog
 */

class Xdebugger extends Module
{
    private $default_plugin = 'PHPDebugBar';

    public static function addMessage($message, $label = 'Info', $backtrace_info = false)
    {
        global $_DEBUG;

        if ($message)
        {
            if (is_string($message))
            {
                if (!$backtrace_info)
                {
                    // get file and line info
                    $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
                    $backtrace = $backtrace[0];
                    $message = '<span style="font-weight: bold;">' . $message . '</span><br /><span class="calling-file">in file <span style="font-weight: bold;">"' . $backtrace['file'] . '"</span> on line <span style="font-weight: bold;">' . $backtrace['line'] . '</span></span>';
                }

                $_DEBUG['messages'][] = ['text' => $message, 'label' => $label];
            }
            else
            {
                $_DEBUG['messages'][] = ['text' => $message, 'label' => $label, 'backtrace' => $backtrace_info];
            }
        }
    }

    public static function addException($e)
    {
        global $_DEBUG;

        if ($e)
        {
            $_DEBUG['exceptions'][] = $e;
        }
    }

    public static function addMeasure($label, $start, $end, $params = array(), $collector = null)
    {
        global $_DEBUG;
        if ($label && $start && $end)
        {
            $_DEBUG['measures'][] = ['label' => $label, 'start' => $start, 'end' => $end, 'params' => $params, 'collector' => $collector];
        }
    }
}

?>
