<?php

/**
 * @author Danijel
 * @copyright 2013
 */



?>
<h4>Settings</h4>
<hr />
<h5>Main settings</h5>
<ul class="nav nav-sidebar">
    <li><a href="/admin/interface/settings/" class="follow">Settings dashboard</a></li>
    <li class="active"><a href="/admin/interface/settings/storage_settings/" class="follow">Storage settings</a></li>
    <li><a href="/admin/interface/settings/language_settings/" class="follow">Language settings</a></li>
</ul>
<hr />
<h5>Project settings</h5>
<ul class="nav nav-sidebar">
    <li><a href="/admin/interface/contents/" class="follow">Xenium</a></li>
    <li class="active"><a href="/admin/interface/contents/static_contents_default/" class="follow">Other project</a></li>
    <li><a href="/admin/interface/contents/emails_default/" class="follow">Another project</a></li>
</ul>