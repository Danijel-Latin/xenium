<div class="modal fade" id="addBlogModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Inser a blog post in text</h4>
      </div>
      <div class="modal-body">
        <p>Select a text block that you want to add and then select its size.</p>
        <p>
            <label>Select a text block:</label>
            <select id="text_blog_id" class="classic-select form-control" style="width: 400px;">
                <option value="">Please choose</option>
                <?php foreach ($project_data as $project){ ?>
                <optgroup label="<?php echo $project['project_name']; ?>">
                <?php foreach ($project['data'] as $blog){ ?>
                    <option value="<?php echo $blog['id']; ?>"><?php echo $blog['title']; ?></option>
                <?php } ?>
                <?php } ?>
                </optgroup>
            </select>
        </p>
        <p>
            <label>Select block size:</label>
            <!--&nbsp;&nbsp;&nbsp;<input value="1 col-md-3" type="radio" name="text_blog_size" checked="checked" /> 25%
            &nbsp;&nbsp;&nbsp;<input value="2 col-md-6" type="radio" name="text_blog_size" /> 50%
            &nbsp;&nbsp;&nbsp;<input value="3 col-md-9" type="radio" name="text_blog_size" /> 75%
            &nbsp;&nbsp;&nbsp;<input value="4 col-md-12" type="radio" name="text_blog_size" /> 100%
            &nbsp;&nbsp;&nbsp;<input value="-one-third col-md-4" type="radio" name="text_blog_size" /> one third
            &nbsp;&nbsp;&nbsp;<input value="-two-thirds col-md-8" type="radio" name="text_blog_size" /> two thirds-->
            
            <label>Horizontal</label>
            <input id="blog-sliding-size-modal-hor" class="sliding-size-modal-hor" />
            <label>Vertical</label>
            <input id="blog-sliding-size-modal-ver" class="sliding-size-modal-ver" />
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="add_new_blog" onclick="add_new_blog(); return false;" data-dismiss="modal">Insert block</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function add_new_blog(){
    var blog_id = $("#text_blog_id").val();
    //console.log(offer_id);
    //var blog_size = $("input[name='text_blog_size']:checked").val();
    
    //var blog_hor_size = 'col-md-' + window.horModalSlider.getValue();
    //var blog_ver_size = 'col-ver-' + window.verModalSlider.getValue();
    
    var blog_hor_size = 'col-md-' + window['blog-sliding-size-modal-hor'].getValue();
    var blog_ver_size = 'col-ver-' + window['blog-sliding-size-modal-ver'].getValue();
    
    //var blog_hor_size = 'col-md-' + $(".sliding-size-modal-hor").val();
    //var blog_ver_size = 'col-ver-' + $(".sliding-size-modal-ver").val();
    
    var blog_size = blog_hor_size + ' ' + blog_ver_size;
    
    var selected_blog = AdminAction.get_blog_for_text(blog_id, blog_size);
    
    $("#" + window.alohaEditable).prepend(selected_blog);
    //Aloha.execCommand('inserthtml', false, selected_offer);
    $("#" + window.alohaEditable).focus();
    //jQuery('.offer').alohaBlock();
    
    var elem_id = window.alohaEditable.replace("-aloha", "");
    matchit(elem_id);
    
    jQuery('.aloha-editable .alohablock').alohaBlock();
    
    bind_aloha_block_functions();
}
</script>