<div class="modal fade" id="addTestimonialModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Inser a testimonial in text</h4>
      </div>
      <div class="modal-body">
        <p>Select a testimonial block that you want to add and then select its size.</p>
        <p>
            <label>Select a testimonials:</label>
            <select id="text_testimonial_id" class="classic-select form-control" style="width: 400px;">
                <option value="">Please choose</option>
                <?php foreach ($project_data as $project){ ?>
                <optgroup label="<?php echo $project['project_name']; ?>">
                <?php foreach ($project['data'] as $testimonial){ ?>
                    <option value="<?php echo $testimonial['id']; ?>"><?php echo $testimonial['title']; ?></option>
                <?php } ?>
                <?php } ?>
                </optgroup>
            </select>
        </p>
        <p>
            <label>Select block size:</label>
            <!--&nbsp;&nbsp;&nbsp;<input value="1 col-md-3" type="radio" name="text_testimonial_size" checked="checked" /> 25%
            &nbsp;&nbsp;&nbsp;<input value="2 col-md-6" type="radio" name="text_testimonial_size" /> 50%
            &nbsp;&nbsp;&nbsp;<input value="3 col-md-9" type="radio" name="text_testimonial_size" /> 75%
            &nbsp;&nbsp;&nbsp;<input value="4 col-md-12" type="radio" name="text_testimonial_size" /> 100%
            &nbsp;&nbsp;&nbsp;<input value="-one-third col-md-4" type="radio" name="text_testimonial_size" /> one third
            &nbsp;&nbsp;&nbsp;<input value="-two-thirds col-md-8" type="radio" name="text_testimonial_size" /> two thirds-->

            <label>Horizontal</label>
            <input id="testimonial-sliding-size-modal-hor" class="sliding-size-modal-hor" />
            <label>Vertical</label>
            <input id="testimonial-sliding-size-modal-ver" class="sliding-size-modal-ver" />
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="add_new_testimonial" onclick="add_new_testimonial(); return false;" data-dismiss="modal">Insert block</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function add_new_testimonial(){
    var testimonial_id = $("#text_testimonial_id").val();
    //console.log(offer_id);
    //var testimonial_size = $("input[name='text_testimonial_size']:checked").val();

    //var testimonial_hor_size = 'col-md-' + window.horModalSlider.getValue();
    //var testimonial_ver_size = 'col-ver-' + window.verModalSlider.getValue();

    var testimonial_hor_size = 'col-md-' + window['testimonial-sliding-size-modal-hor'].getValue();
    var testimonial_ver_size = 'col-ver-' + window['testimonial-sliding-size-modal-ver'].getValue();

    //var testimonial_hor_size = 'col-md-' + $(".sliding-size-modal-hor").val();
    //var testimonial_ver_size = 'col-ver-' + $(".sliding-size-modal-ver").val();

    var testimonial_size = testimonial_hor_size + ' ' + testimonial_ver_size;

    var selected_testimonial = AdminAction.get_testimonial_for_text(testimonial_id, testimonial_size);

    $("#" + window.alohaEditable).prepend(selected_testimonial);
    //Aloha.execCommand('inserthtml', false, selected_offer);
    $("#" + window.alohaEditable).focus();
    //jQuery('.offer').alohaBlock();

    var elem_id = window.alohaEditable.replace("-aloha", "");
    matchit(elem_id);

    jQuery('.aloha-editable .alohablock').alohaBlock();

    bind_aloha_block_functions();
}
</script>
