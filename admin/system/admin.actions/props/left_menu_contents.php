<?php

/**
 * @author Danijel
 * @copyright 2014
 */

$project_info = Project::getInfo($project_id);

?>
<h5><?php echo $project_info['project_name']; ?></h5>
<h4>Contents</h4>
<hr />
<!--<h5>Static pages and emails</h5>-->
<ul class="nav nav-sidebar">
    <li class="nav-item"><a href="/admin/interface/project-<?php echo $project_id; ?>/interface-contents/" class="nav-link follow">Pages</a></li>
    <li class="nav-item"><a href="/admin/interface/project-<?php echo $project_id; ?>/interface-contents/module-menu/" class="nav-link follow">Menus</a></li>
    <li class="nav-item"><a href="/admin/interface/project-<?php echo $project_id; ?>/interface-contents/module-email/" class="nav-link follow">Emails</a></li>
</ul>
<hr />
<?php if (isset($additional_menu['dynamic_content'])){ ?>
<h5>Dynamic contents</h5>
<ul class="nav nav-sidebar">
    <?php echo $additional_menu['dynamic_content']; ?>
</ul>
<?php } ?>
<hr />
<!--<h5>Translations</h5>
<ul class="nav nav-sidebar">
    <li class="active"><a href="/admin/interface/contents/translations_default/" class="follow">Edit translations</a></li>
</ul>-->
