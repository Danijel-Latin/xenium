<?php

/**
 * @author Danijel
 * @copyright 2013
 */



?>
<h4>Users</h4>
<hr />
<ul class="nav nav-sidebar">
    <li class="nav-item active"><a href="/admin/interface/interface-users/" class="nav-link follow">Users summary</a></li>
    <li class="nav-item"><a href="/admin/interface/users/interface-users/action-usersStats/" class="nav-link follow">Users statistics</a></li>
    <li class="nav-header">Data manipulation</li>
    <li class="nav-item"><a href="#" class="nav-link follow">Export users</a></li>
    <li class="nav-item"><a href="#" class="nav-link follow">Import users</a></li>
    <li class="nav-item"><a href="#" class="nav-link follow">Generate reports</a></li>
    <li class="nav-item"><a href="#" class="nav-link follow">User statistics</a></li>
</ul>
