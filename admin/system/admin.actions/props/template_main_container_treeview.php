<div class="row">
    <div class="col-md12 title">
      <h1 class="page-header">
        <?php echo $title; ?>
        <?php if (isset($title_button_group) && $title_button_group) echo $title_button_group; ?>
      </h1>
    </div>
</div>

<div class="row row-content">
    <div id="content-display" class="col-md-12 main">
        <?php if (is_array($contents) && isset($contents)){ ?>
            <table class="table table-striped table-hover content-table tree-table">
                <thead>
                    <tr>
                        <th class="col-md-11">Title</th>
                        <th class="col-md-1">Actions</th>
                    </tr>
                </thead>
            </table>

            <div id="jstree_div">
            <?php
            $page = new Page;
            //$pages = $page->adminGetTree(1);
            //$render_pages = $page->adminRenderTree($pages);
            $render_pages = $page->adminRenderTree($contents);
            echo $render_pages;
             ?>
             </div>

            <script type="text/javascript">
            $(function () {
                var $treeview = $("#jstree_div");
                $treeview.jstree({
                    'core' : {
                        check_callback : true
                    },
                    "plugins" : [ /*"contextmenu", */"dnd", "search", /*"state", */"types", "wholerow"/*, "unique"*/ ],
                    "check_callback" : function(e,data){
                        console.log(data)
                    }
                }).on('ready.jstree move_node.jstree create_node.jstree rename_node.jstree', function() {
                    $treeview.jstree('open_all');
                    bindTitleEdit();
                    bindActions();
                }).on('move_node.jstree', function(e, data){
                    var project_id = $(document).find("#" + data.node.id).attr("project-id");
                    var content_id = $(document).find("#" + data.node.id).attr("content-id");
                    var class_name = $(document).find("#" + data.node.id).attr("class-name");

                    if (data.parent == "#"){
                        var parent_id = 0;
                        var parent_element = "#jstree_div";
                    } else {
                        var parent_id = $(document).find("#" + data.parent).attr("content-id");
                        var parent_element = "#" + data.parent;
                    }

                    if (data.old_parent != data.parent){
                        AdminAction.updateParent(project_id, class_name, content_id, parent_id);
                    }

                    window.new_order = {};

                    $(document).find(parent_element + " > ul > li").each(function () {
                        var index = $(document).find(parent_element + " > ul > li").index(this);
                        //console.log(index);
                        var content_id = $(this).attr("content-id");

                        window.new_order[content_id] = index;
                    });
                    //console.log(window.new_order);
                    AdminAction.updateOrder(project_id, class_name, JSON.stringify(window.new_order));
                });

                updateInterface();
            });

            function bindActions(){
                $(".trash-content").click(function(){
                    var content_id = $(this).attr('content-id');
                    var title = $(this).closest(".jstree-node").find(".branch-title").html();
                    /*var project_id = $(this).closest(".jstree-node").attr("project-id");
                    var class_name = $(this).closest(".jstree-node").attr("class-name");*/
                    var project_id = $(this).attr("project-id");
                    var class_name = $(this).attr("class-name");
                    var content_element = $(this).closest(".jstree-node");

                    bootbox.dialog({
                      message: 'Are you sure you want to delete <b>"' + title + '"</b>.<br />All child contents will be deleted too.',
                      title: "Content deletion confirmation",
                      buttons: {
                        success: {
                          label: "Delete",
                          className: "btn-danger",
                          callback: function() {
                            //Example.show("great success");
                            bootbox.hideAll();
                            content_element.slideUp('fast');
                            AdminAction.trash(class_name, content_id, project_id, 1);
                          }
                        },
                        danger: {
                          label: "cancel",
                          className: "btn-default",
                          callback: function() {
                              bootbox.hideAll();
                          }
                        }
                      }
                    });


                });

                $(".publish-content").click(function(){
                    var content_id = $(this).attr('content-id');
                    var title = $(this).closest(".jstree-node").find(".branch-title").html();
                    /*var project_id = $(this).closest(".jstree-node").attr("project-id");
                    var class_name = $(this).closest(".jstree-node").attr("class-name");alert(title);*/
                    var project_id = $(this).attr("project-id");
                    var class_name = $(this).attr("class-name");

                    var publish = 0;
                    if (!$(this).hasClass("active"))
                    {
                        publish = 1;
                    }

                    AdminAction.publish(class_name, content_id, project_id, publish);

                    if (publish){
                        $(this).addClass("active");
                    } else {
                        $(this).removeClass("active");
                    }
                });
            }

            function titleSave(){
                //alert('saving document');

                var changed_title = $(document).find(".editing input").val();
                var content_id = $(document).find(".editing").closest(".jstree-node").attr("content-id");
                var project_id = $(document).find(".editing").closest(".jstree-node").attr("project-id");
                var class_name = $(document).find(".editing").closest(".jstree-node").attr("class-name");

                AdminAction.updateTitle(project_id, class_name, content_id, changed_title);

                $(document).find(".editing > .jstree-anchor > .branch-title").html(changed_title);
                $(document).find('.editing .title-edit').remove();
                $(document).find('.editing').removeClass('editing');
            }

            function titleDiscard(){
                //alert('discarding changes');

                $(document).find('.editing .title-edit').remove();
                $(document).find('.editing').removeClass('editing');
            }

            function bindTitleEditActions(){
                $(document).find('.title-edit .fa-save').click(function(){
                    titleSave();
                });

                $(document).find('.title-edit .fa-remove').click(function(){
                    titleDiscard();
                });

                $(document).find(".editing input").keyup(function (e) {
                    if (e.which == 13) { // Enter
                        titleSave();
                    }
                    if (e.which == 27) { // Esc
                        titleDiscard();
                    }
                });
                /*
                $(document).find(".editing input").bind('focusin focus', function(e){
                    e.preventDefault();
                });
                */
                setTimeout(function(){
                    $(document).find(".title-edit input").focus().select();
                }, 100);
            }

            function bindTitleEdit(){
                $(".branch-title, .branch-title-edit").click(function(){
                    //var content = $(this).html();
                    var content = $(this).parent().find('.branch-title').html();
                    var already_editing = $(document).find(".editing").length;
                    if (!already_editing){
                        $(this).closest(".jstree-node").addClass('editing');
                        $(this).closest(".jstree-node").prepend('<div class="title-edit"><input value="' + content + '"><i class="fa fa-save" title="Save (Enter)"></i><i class="fa fa-remove" title="Cancel (Esc)"></i></div>');

                        bindTitleEditActions();
                    } else {
                        var prev_title = $(document).find(".editing .branch-title").html();
                        var changed_title = $(document).find(".editing input").val();

                        if (prev_title != changed_title){
                            bootbox.dialog({
                              message: 'You did not save your changes for <b>"' + prev_title + '"</b>.<br />Would you like to save them?<br /><br />Click <b>"Save"</b> to change the title to <b>"' + changed_title + '"</b>',
                              title: "Save changes?",
                              buttons: {
                                success: {
                                  label: "Save",
                                  className: "btn-success",
                                  callback: function() {
                                    //Example.show("great success");
                                    titleSave();
                                    bootbox.hideAll();
                                  }
                                },
                                danger: {
                                  label: "Discard",
                                  className: "btn-danger",
                                  callback: function() {
                                    //Example.show("uh oh, look out!");
                                    bootbox.hideAll();
                                    $(document).find('.editing .title-edit').remove();
                                    $(document).find('.editing').removeClass('editing');

                                    $(this).closest(".jstree-node").addClass('editing');
                                    $(this).closest(".jstree-node").prepend('<div class="title-edit"><input value="' + content + '"><i class="fa fa-save" title="Save (Enter)"></i><i class="fa fa-remove" title="Cancel (Esc)"></i></div>');

                                    bindTitleEditActions();
                                  }
                                }
                              }
                            });
                        } else {
                            $(document).find('.editing .title-edit').remove();
                            $(document).find('.editing').removeClass('editing');

                            $(this).closest(".jstree-node").addClass('editing');
                            $(this).closest(".jstree-node").prepend('<div class="title-edit"><input value="' + content + '"><i class="fa fa-save" title="Save (Enter)"></i><i class="fa fa-remove" title="Cancel (Esc)"></i></div>');

                            bindTitleEditActions();
                        }
                    }
                });
            }

            $(document).find("#new").click(function(){

                var already_editing = $(document).find(".editing").length;
                var project_id = $(this).attr('project-id');
                var class_name = $(this).attr('class-name');
                var parent_id = 0;

                if (already_editing){
                    var prev_title = $(document).find(".editing .branch-title").html();
                    var changed_title = $(document).find(".editing input").val();

                    if (prev_title != changed_title){
                        bootbox.dialog({
                          message: 'You did not save your changes for <b>"' + prev_title + '"</b>.<br />Would you like to save them?<br /><br />Click <b>"Save"</b> to change the title to <b>"' + changed_title + '"</b>',
                          title: "Save changes?",
                          buttons: {
                            success: {
                              label: "Save",
                              className: "btn-success",
                              callback: function() {
                                //Example.show("great success");
                                titleSave();
                                bootbox.hideAll();
                              }
                            },
                            danger: {
                              label: "Discard",
                              className: "btn-danger",
                              callback: function() {
                                //Example.show("uh oh, look out!");
                                bootbox.hideAll();
                                $(document).find('.editing .title-edit').remove();
                                $(document).find('.editing').removeClass('editing');

                                $(this).closest(".jstree-node").addClass('editing');
                                $(this).closest(".jstree-node").prepend('<div class="title-edit"><input value="' + content + '"><i class="fa fa-save" title="Save (Enter)"></i><i class="fa fa-remove" title="Cancel (Esc)"></i></div>');

                                bindTitleEditActions();
                              }
                            }
                          }
                        });
                    } else {
                        //$(document).find(".editing .title-edit .fa-remove").trigger("click");
                    }
                }

    			var ref = $(document).find('#jstree_div').jstree(true),
    				sel = ref.get_selected();
                    parent = ref.get_selected();

    			if(!sel.length) {
                    parent = "#";
                    parent_id = 0;
                    //sel = ref.create_node("#", {"icon":"fa fa-file-text-o fa-2x"});
                    sel = ref.create_node("#", {"icon":"icon icon-linea-basic-10-101"});
                } else {
        			sel = sel[0];
                    parent_id = sel.replace( /^\D+/g, '');
        			//sel = ref.create_node(sel, {"icon":"fa fa-file-text-o fa-2x"});
                    sel = ref.create_node(sel, {"icon":"icon icon-linea-basic-10-101"});
                }

    			if(sel) {
                    //var insert_data = JSON.parse(AdminAction.insertNew(project_id, class_name, parent_id));
                    //var insert_data = AdminAction.insertNew(project_id, class_name, parent_id);
                    insert_data(project_id, class_name, parent_id).done(function(insert_data){
                        //console.log(insert_data);
                        var title = insert_data['title'];
                        var id = insert_data['id'];
                        //var title = $("#" + sel).text().replace('\u00a0', '');

                        var new_html = '<span class="branch-title">' + title + '</span><span class="fa fa-pencil branch-title-edit"></span>' +
                                        '<div class="actions">' +
                                            '<a href="/admin/interface/project-' + project_id + '/interface-contents/module-page/action-edit/id-' + id + '/" class="btn btn-secondary follow" title="Edit"><i class="icon icon-linea-software-10-59"></i></a> ' +
                                            '<button type="button" class="btn btn-secondary publish-content " content-id="' + id + '" project-id="' + project_id + '" class-name="' + class_name + '" title="Published"><i class="icon icon-linea-basic-10-29"></i></button> ' +
                                            '<button type="button" class="btn btn-secondary trash-content" content-id="' + id + '" project-id="' + project_id + '" class-name="' + class_name + '" title="Remove"><i class="icon icon-linea-arrows-10-57"></i></button>' +
                                        '</div>';
                        ref.rename_node($("#" + sel), new_html);
                        //bindTitleEdit();

                        $("#" + sel + "_anchor").find(".branch-title").trigger("click");

                        if (parent == "#"){
                            ref.deselect_node($("#" + sel));
                        } else {
                            var parent_id = $("#" + sel).parent().parent().attr('id');
                            $(document).find("#" + parent_id + " > a.jstree-anchor").trigger("click");
                        }

                        //bindActions();
                    });
    			}

                /*$('#content-display').animate({
                    scrollTop: $("#" + sel).offset().top
                }, 2000);*/

                return false;
    		});
            function insert_data(project_id, class_name, parent_id){
                return AdminAction.insertNew(project_id, class_name, parent_id);
            }
    		function demo_rename() {
    			var ref = $('#jstree_demo_div').jstree(true),
    				sel = ref.get_selected();
    			if(!sel.length) { return false; }
    			sel = sel[0];
    			ref.edit(sel);
    		};
    		function demo_delete() {
    			var ref = $('#jstree_demo_div').jstree(true),
    				sel = ref.get_selected();
    			if(!sel.length) { return false; }
    			ref.delete_node(sel);
    		};

            <?php if (isset($options['trashed']) && $options['trashed']){ ?>
                $(".title-button-group #new, .title-button-group #newLink").attr('disabled', 'disabled');
            <?php } else { ?>
                $(".title-button-group #new, .title-button-group #newLink").removeAttr('disabled', 'disabled');
            <?php } ?>

            </script>
        <?php } else { ?>
            <h5>There are no contents to display. Try to add some.</h5>
        <?php } ?>
    </div>
</div>
