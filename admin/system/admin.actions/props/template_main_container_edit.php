<?php

/**
 * @author Danijel
 * @copyright 2013
 */

global $additional_text_editor, $additional_text_editor_modal;

?>
<div class="row">
    <div class="col-md-12 title">
      <h1 class="page-header">
        <?php echo 'Editing "' . $content["title"] . '"'; ?>
        <?php if (isset($title_button_group) && $title_button_group) echo $title_button_group; ?>
      </h1>
    </div>
</div>

<div class="row row-content">
    <div id="content-display" class="col-sm-12 col-md-12 main">
        <div class="row">
            <div class="col-md-11 edit-area">
                <?php
                //echo Form::generate_admin_form($fields, $populate_content, $editing, $project_id, $id);
                //print_r($content);
                echo Form::generateAdminForm($class, $content, $project_id, $id);
                ?>
            </div>

            <div class="col-sm-3 col-md-1 right-sidebar">
                <button class="btn btn-primary btn-lg btn-block save-btn" class-name="<?php echo $class; ?>" content-id="<?php echo $id; ?>" project-id="<?php echo $project_id; ?>" title="Save"><i class="icon icon-linea-basic-10-33"></i> &nbsp; <span class="save-btn-txt">Save</span></button>
                <a id="back-button" class="btn btn-secondary btn-sm btn-block back-btn follow" href="/admin/interface/contents/" title="Go back"><i class="fa fa-angle-double-left"></i> &nbsp; Back</a>

                <?php self::loadTextEditor($project_id); ?>
                <button id="upload-image" style="display: none;">upl</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
// Custom example logic
$(function() {
    $('.publish').click(function(){
        var content_id = $(this).attr('content-id');
        var project_id = $(this).attr("project-id");
        var class_name = $(this).attr("class-name");

        var publish = 0;
        if (!$(this).hasClass("active"))
        {
            publish = 1;
        }

        AdminAction.publish(class_name, content_id, project_id, publish);

        if (publish){
            $(this).addClass("active");
        } else {
            $(this).removeClass("active");
        }
    });

    $(".trash").click(function(){
        var content_id = $(this).attr('content-id');
        var title = '<?php echo $content['title']; ?>';
        var project_id = $(this).attr("project-id");
        var class_name = $(this).attr("class-name");

        window.delete_button = $(this);

        if (!$(this).hasClass("active"))
        {
            bootbox.dialog({
              message: 'Are you sure you want to delete <b>"' + title + '"</b>.<br />All child contents will be deleted too.',
              title: "Content deletion confirmation",
              buttons: {
                success: {
                  label: "Delete",
                  className: "btn-danger",
                  callback: function() {
                    //Example.show("great success");
                    bootbox.hideAll();
                    window.delete_button.addClass("active");
                    AdminAction.trash(class_name, content_id, project_id, 1);
                  }
                },
                danger: {
                  label: "cancel",
                  className: "btn-default",
                  callback: function() {
                      bootbox.hideAll();
                  }
                }
              }
            });
        } else {
            bootbox.dialog({
              message: 'Are you sure you want to renew <b>"' + title + '"</b>.<br />Content will be positioned as last.',
              title: "Content renewal confirmation",
              buttons: {
                success: {
                  label: "Renew",
                  className: "btn-primary",
                  callback: function() {
                    //Example.show("great success");
                    bootbox.hideAll();
                    window.delete_button.removeClass("active");
                    AdminAction.trash(class_name, content_id, project_id, 0);
                  }
                },
                danger: {
                  label: "cancel",
                  className: "btn-default",
                  callback: function() {
                      bootbox.hideAll();
                  }
                }
              }
            });
        }

        return false;
    });

    $(".image-block").each(function(){

        var target_textarea_id = $(this).attr('id').replace('-plupload', '');
        var drop_element_id = $(this).attr('id');

    	window['elem-' + drop_element_id] = new plupload.Uploader({
    		runtimes : 'html5',
            browse_button : 'upload-image',
    		//browse_button : 'pickfiles',
            drop_element : drop_element_id,
    		//container : 'container',
    		max_file_size : '10mb',
    		url : '/admin/system/admin.actions/admin.upload.php',
    		//flash_swf_url : '/plupload/js/plupload.flash.swf',
    		//silverlight_xap_url : '/plupload/js/plupload.silverlight.xap',
    		multi_selection:false,
            max_file_count: 1,

            filters : {
                max_file_size : '10mb',
                mime_types: [
                    {title : "Image files", extensions : "jpg,gif,png"},
                    {title : "Zip files", extensions : "zip"}
                ]
            },
            init : {
                PostInit: function() {
                    //alert('inited');
                },
                FilesAdded: function(up, files) {
                    $.each(files, function(i, file) {
            			//$('#image-block').append(
                        $('#' + drop_element_id).html(
            				'<div class="image" id="' + file.id + '">' +
            				//file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                            '<b></b>' +
            			'</div>');
            		});

                    //alert('file added');
                    up.refresh(); // Reposition Flash/Silverlight
                    up.start();
                },
                UploadProgress: function(up, file) {
                    $('#' + file.id + " b").html(file.percent + "%");
                },
                FileUploaded: function(up, file, info) {
                    var images = {"image_files" : []};

            		var obj = jQuery.parseJSON(info.response);
                    console.log(obj);
                    if (typeof obj.fileUrl != 'undefined') {
                        //$("#element-" + window.random_id + " .image-preview").css('background-image', 'url(' + obj.fileUrl + ')');
                        $('#' + file.id).html('<div class="img" style="background: url(' + obj.fileUrl + ')"></div>');
                        //$("#element-" + window.random_id + " .settings").text(JSON.stringify({ 'image_file': obj.fileUrl, 'id': obj.id, 'ext': obj.ext, 'provider': obj.provider }));
                        images.image_files.push({ 'fileUrl': obj.fileUrl, 'id': obj.id, 'ext': obj.ext, 'provider': obj.provider });
                    } else {
                        $('#' + file.id).html('<div class="img" style="background: url(/images/450x350/' + obj.fileYear + '/' + obj.fileMonth + '/' + obj.fileName + ')"></div>');
                        $('#' + file.id).attr('year', obj.fileYear);
                        $('#' + file.id).attr('month', obj.fileMonth);
                        $('#' + file.id).attr('image', obj.fileName);
                        images.image_files.push( { "id":file.id, "fileName":obj.fileName, "fileYear":obj.fileYear, "fileMonth":obj.fileMonth } );
                    }

                    var new_images = JSON.stringify(images.image_files);
                    $("#" + target_textarea_id).val(new_images);
                },
                Error: function(up, err) {
                    /*document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;*/
                    console.log("\nError #" + err.code + ": " + err.message);
                }
            }
    	});

        window['elem-' + drop_element_id].init();

        /*
    	window['blog_uploader'].bind('Init', function(up, params) {
    		//$('#image-block').html("<div>Current runtime: " + params.runtime + "</div>");
    	});
    	window['blog_uploader'].init();

    	window['blog_uploader'].bind('FilesAdded', function(up, files) {
    		$.each(files, function(i, file) {
    			//$('#image-block').append(
                $('#' + drop_element).html(
    				'<div class="image" id="' + file.id + '">' +
    				//file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                    '<b></b>' +
    			'</div>');
    		});

    		up.refresh(); // Reposition Flash/Silverlight
            up.start();
    	});

    	window['blog_uploader'].bind('UploadProgress', function(up, file) {
    		$('#' + file.id + " b").html(file.percent + "%");
    	});

    	window['blog_uploader'].bind('Error', function(up, err) {
    		$('#filelist').append("<div>Error: " + err.code +
    			", Message: " + err.message +
    			(err.file ? ", File: " + err.file.name : "") +
    			"</div>"
    		);

    		up.refresh(); // Reposition Flash/Silverlight
    	});

    	window['blog_uploader'].bind('FileUploaded', function(up, file, response) {

           var images = {"image_files" : []};

    		var obj = jQuery.parseJSON(response.response);
            $('#' + file.id).html('<div class="img" style="background: url(/images/1000x200/' + obj.fileYear + '/' + obj.fileMonth + '/' + obj.fileName + ')"></div>');
            $('#' + file.id).attr('year', obj.fileYear);
            $('#' + file.id).attr('month', obj.fileMonth);
            $('#' + file.id).attr('image', obj.fileName);
            images.image_files.push( { "id":file.id, "fileName":obj.fileName, "fileYear":obj.fileYear, "fileMonth":obj.fileMonth } );
            var new_images = JSON.stringify(images.image_files);
            $("#" + target_textarea_id).val(new_images);
    	});*/

    });
});

</script>
