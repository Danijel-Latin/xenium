<?php

/**
 * @author Danijel
 * @copyright 2014
 */



?>
<!--
<div class="custom-container title-container">
    <h3>Main dashboard</h3>
    <p>Here you can view, add or edit the displaying of plugins on main dashboard and create your own custom dashboards.</p>
</div>
-->

<div class="row">
    <div class="col-md-12 title">
      <h1 class="page-header">Main dashboard</h1>
    </div>
</div>
<div class="row row-content">
    <div id="content-display" class="col-md-12 content main">
    <div class="grid-stack row">
        <div class="col-md-6 grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="6" data-gs-height="2">
            <div class="chart-wrapper" func="getPageviewsToday">
              <div class="chart-title">
                <h5>Visits over time</h5>
              </div>
              <div class="chart-stage col-ver-100">
                <!--<img data-src="holder.js/100%x350/white">-->
                <!--<div id="widgetIframe"><iframe id="widgetIframeI" width="100%" height="350" src="http://piwik-xenium.rhcloud.com/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=day&date=today&disableLink=1&widget=1&language=en" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>
                -->
                <canvas id="visits-graph" class="full-chart">

                </canvas>
                <?php
                    $get_json = json_decode(file_get_contents('http://piwik-xenium.rhcloud.com/index.php?module=API&method=VisitsSummary.getVisits&idSite=3&period=day&date=today&format=JSON&token_auth=f182c6e72a4ea868da68a7f1aee1d896&date=last10'), true);

                    $graph_data = array();
                    $graph_labels = array();

                    foreach ($get_json as $key => $data)
                    {
                        $graph_data[] = $data;
                        $graph_labels[] = date('d.m.Y', strtotime($key));
                    }
                ?>

                <script type="text/javascript">
                var lineChartData = {
    "datasets": [{
        "data": <?php echo json_encode($graph_data); ?>,
            "pointStrokeColor": "#fff",
            "fillColor": "rgba(220,220,220,0.5)",
            "pointColor": "rgba(220,220,220,1)",
            "strokeColor": "rgba(220,220,220,1)"
    }],
        "labels": <?php echo json_encode($graph_labels); ?>
    };



    var myLine = new Chart(document.getElementById("visits-graph").getContext("2d")).Line(lineChartData, {
    responsive: true,
    maintainAspectRatio: true,
    scaleShowGridLines : false,
    });
                </script>
              </div>
              <div class="chart-notes">
                Viewing users per day
              </div>
            </div>
        </div>

        <div class="col-md-6 grid-stack-item" data-gs-x="6" data-gs-y="0" data-gs-width="6" data-gs-height="2">
            <div class="chart-wrapper" func="getPageviews5Days">
              <div class="chart-title">
                <h5>Visits overview</h5>
              </div>
              <div class="chart-stage col-ver-100">
                <!--<img data-src="holder.js/100%x350/white">-->
                <?php
                    $visits_overview = json_decode(file_get_contents('http://piwik-xenium.rhcloud.com/index.php?module=API&method=VisitsSummary.get&idSite=3&period=day&date=today&format=JSON&token_auth=f182c6e72a4ea868da68a7f1aee1d896&date=last10'), true);
                    //print_r($visits_overview);

                    $visits_per_day = array();
                    $unique_visits_per_day = array();
                    $avg_time_on_site = array();
                    $bounce_rate = array();
                    $nb_actions_per_visit = array();
                    $max_actions = array();

                    foreach ($visits_overview as $key => $array_value)
                    {
                        if (!isset($array_value['nb_visits']))
                        {
                            $visits_per_day[] = 0;
                        }
                        else
                        {
                            $visits_per_day[] = $array_value['nb_visits'];
                        }

                        if (!isset($array_value['nb_uniq_visitors']))
                        {
                            $unique_visits_per_day[] = 0;
                        }
                        else
                        {
                            $unique_visits_per_day[] = $array_value['nb_uniq_visitors'];
                        }

                        if (!isset($array_value['avg_time_on_site']))
                        {
                            $avg_time_on_site[] = 0;
                        }
                        else
                        {
                            $avg_time_on_site[] = $array_value['avg_time_on_site'];
                        }

                        if (!isset($array_value['bounce_rate']))
                        {
                            $bounce_rate[] = 0;
                        }
                        else
                        {
                            $bounce_rate[] = $array_value['bounce_rate'];
                        }

                        if (!isset($array_value['nb_actions_per_visit']))
                        {
                            $nb_actions_per_visit[] = 0;
                        }
                        else
                        {
                            $nb_actions_per_visit[] = $array_value['nb_actions_per_visit'];
                        }

                        if (!isset($array_value['max_actions']))
                        {
                            $max_actions[] = 0;
                        }
                        else
                        {
                            $max_actions[] = $array_value['max_actions'];
                        }
                    }
                ?>
                <p>
                    <span class="bar"><?php echo implode(',', $visits_per_day); ?></span> &nbsp; <strong><?php echo end($visits_per_day); ?></strong> visitors<br />
                </p>
                <p>
                    <span class="bar"><?php echo implode(',', $unique_visits_per_day); ?></span> &nbsp; <strong><?php echo end($unique_visits_per_day); ?></strong> unique visitors<br />
                </p>
                <p>
                    <span class="bar"><?php echo implode(',', $avg_time_on_site); ?></span> &nbsp; <strong><?php echo end($unique_visits_per_day); ?>s</strong> average time on site<br />
                </p>
                <p>
                    <span class="bar"><?php echo implode(',', $bounce_rate); ?></span> &nbsp; <strong><?php echo end($bounce_rate); ?>%</strong> visits have bounced (left the website after one page)<br />
                </p>
                <p>
                    <span class="bar"><?php echo implode(',', $nb_actions_per_visit); ?></span> &nbsp; <strong><?php echo end($nb_actions_per_visit); ?></strong> actions (page views, downloads, outlinks and internal site searches) per visit<br />
                </p>
                <p>
                    <span class="bar"><?php echo implode(',', $max_actions); ?></span> &nbsp; <strong><?php echo end($max_actions); ?></strong> max actions (page views, downloads, outlinks and internal site searches)<br />
                </p>

                <script type="text/javascript">
                    $(".bar").peity("bar");
                </script>
                <!--
                <div id="widgetIframe"><iframe width="100%" height="470" src="http://piwik-xenium.rhcloud.com/index.php?module=Widgetize&action=iframe&widget=1&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=3&period=day&date=today&disableLink=1&widget=1&language=en" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>
                -->
              </div>
              <div class="chart-notes">
                Viewing actions per day.
              </div>
            </div>
          </div>
      </div>
      <!--<h1 class="page-header">Dashboard</h1>-->
    <!--
      <div class="row placeholders">
        <div class="col-xs-6 col-sm-3 placeholder">
          <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Label</h4>
          <span class="text-muted">Something else</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Label</h4>
          <span class="text-muted">Something else</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Label</h4>
          <span class="text-muted">Something else</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Label</h4>
          <span class="text-muted">Something else</span>
        </div>
      </div>

      <textarea class="aloha-editable col-md-12">
      neki tko tam
      </textarea>

      <h2 class="sub-header">Section title</h2>
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Header</th>
              <th>Header</th>
              <th>Header</th>
              <th>Header</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1,001</td>
              <td>Lorem</td>
              <td>ipsum</td>
              <td>dolor</td>
              <td>sit</td>
            </tr>
            <tr>
              <td>1,002</td>
              <td>amet</td>
              <td>consectetur</td>
              <td>adipiscing</td>
              <td>elit</td>
            </tr>
            <tr>
              <td>1,003</td>
              <td>Integer</td>
              <td>nec</td>
              <td>odio</td>
              <td>Praesent</td>
            </tr>
            <tr>
              <td>1,003</td>
              <td>libero</td>
              <td>Sed</td>
              <td>cursus</td>
              <td>ante</td>
            </tr>
            <tr>
              <td>1,004</td>
              <td>dapibus</td>
              <td>diam</td>
              <td>Sed</td>
              <td>nisi</td>
            </tr>
            <tr>
              <td>1,005</td>
              <td>Nulla</td>
              <td>quis</td>
              <td>sem</td>
              <td>at</td>
            </tr>
            <tr>
              <td>1,006</td>
              <td>nibh</td>
              <td>elementum</td>
              <td>imperdiet</td>
              <td>Duis</td>
            </tr>
            <tr>
              <td>1,007</td>
              <td>sagittis</td>
              <td>ipsum</td>
              <td>Praesent</td>
              <td>mauris</td>
            </tr>
            <tr>
              <td>1,008</td>
              <td>Fusce</td>
              <td>nec</td>
              <td>tellus</td>
              <td>sed</td>
            </tr>
            <tr>
              <td>1,009</td>
              <td>augue</td>
              <td>semper</td>
              <td>porta</td>
              <td>Mauris</td>
            </tr>
            <tr>
              <td>1,010</td>
              <td>massa</td>
              <td>Vestibulum</td>
              <td>lacinia</td>
              <td>arcu</td>
            </tr>
            <tr>
              <td>1,011</td>
              <td>eget</td>
              <td>nulla</td>
              <td>Class</td>
              <td>aptent</td>
            </tr>
            <tr>
              <td>1,012</td>
              <td>taciti</td>
              <td>sociosqu</td>
              <td>ad</td>
              <td>litora</td>
            </tr>
            <tr>
              <td>1,013</td>
              <td>torquent</td>
              <td>per</td>
              <td>conubia</td>
              <td>nostra</td>
            </tr>
            <tr>
              <td>1,014</td>
              <td>per</td>
              <td>inceptos</td>
              <td>himenaeos</td>
              <td>Curabitur</td>
            </tr>
            <tr>
              <td>1,015</td>
              <td>sodales</td>
              <td>ligula</td>
              <td>in</td>
              <td>libero</td>
            </tr>
          </tbody>
        </table>
    </div>-->
    </div>
</div>
