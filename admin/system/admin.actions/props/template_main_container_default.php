<div class="col-sm-12 col-md-10 title">
  <h1 class="page-header">
    <?php echo $title; ?>
    <?php if (isset($title_button_group) && $title_button_group) echo $title_button_group; ?>
  </h1>
</div>

<div id="content-display" class="col-sm-12 col-md-12 main">
    <?php if (is_array($contents) && isset($contents)){ ?>
        <table class="table table-striped table-hover content-table">
            <thead>
                <tr>
                    <th class="col-md-11">Title</th>
                    <th class="col-md-1">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($contents as $content ){ ?>
                <tr id="<?php echo $content['id']; ?>">
                    <td><?php echo $content['title']; ?></td>
                    <td>
                        <a href="/admin/interface/contents/edit_blog/<?php echo $content['id']; ?>_<?php echo $content['project_id']; ?>/" class="btn btn-default follow" title="Edit"><i class="fa fa-edit"></i></a>
                        <button class="btn btn-default" onclick="hide_content_row('<?php echo $content['id']; ?>', 'trash_blog')" title="Remove"><i class="fa fa-remove"></i></button>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
        <h5>There are no contents to display. Try to add some.</h5>
    <?php } ?>
</div>
