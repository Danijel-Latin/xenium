<?php

/**
 * @author Danijel
 * @copyright 2014
 */

global $projects;

?>

<div class="col-sm-6 col-sm-offset-9 col-md-10 col-md-offset-2 title">
    <h1 class="page-header">
        Pages
        <span class="title-button-group">
            <a class="btn btn-default follow" href="/admin/interface/contents/static_contents_default/" title="Show all static contents"><i class="fa fa-list-alt fa-2x"></i></a>
            <a class="btn btn-default" href="#" data-target="#addStaticContent" data-toggle="modal" no-follow="true" title="Create a new static content"><i class="fa fa-file-text-o fa-2x"></i></a>
            <a class="btn btn-default follow" title="Show rejected static contents"><i class="fa fa-trash fa-2x"></i></a>
            <a class="btn btn-default follow" title="Show editing history"><i class="fa fa-history fa-2x"></i></a>
        </span>
    </h1>
</div>

<div id="html_content" class="col-sm-6 col-sm-offset-12 col-md-10 col-md-offset-2 main">
<?php if (is_array($projects) && isset($projects)){ ?>
    <table class="table table-striped table-hover content-table">
        <thead>
            <tr>
                <th class="col-md-11">Title</th>
                <th class="col-md-1">Actions</th>
            </tr>
        </thead>
        <!--<tbody>
            <?php foreach ($projects as $project ){ ?>
            <tr id="<?php echo $project['id']; ?>">
                <td>First page for <strong><?php echo $project['project_name']; ?></strong></td>
                <td>
                    <a href="/admin/interface/contents/edit_first_page/<?php echo $project['id']; ?>/" class="btn btn-default follow" title="Edit"><i class="fa fa-edit"></i></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>-->
    </table>
<?php } else { ?>
    <h5>There are no first pages to display. Try to add a project.</h5>
<?php } ?>

<div id="jstree_demo_div">
    <ul>
      <li data-jstree='{"icon":"fa fa-th-list fa-2x"}'>
          Root node 1
          <div class="actions">
              <a href="/admin/interface/contents/edit_first_page/1/" class="btn btn-default follow" title="Edit"><i class="fa fa-edit"></i></a>
          </div>
        <ul>
          <li data-jstree='{"icon":"fa fa-file-text fa-2x"}' id="child_node_1">
              Child node 1
              <div class="actions">
                  <a href="/admin/interface/contents/edit_first_page/1/" class="btn btn-default follow" title="Edit"><i class="fa fa-edit"></i></a>
              </div>
          </li>
          <li data-jstree='{"icon":"fa fa-file-text fa-2x"}'>
              Child node 2
              <div class="actions">
                  <a href="/admin/interface/contents/edit_first_page/1/" class="btn btn-default follow" title="Edit"><i class="fa fa-edit"></i></a>
              </div>
          </li>
        </ul>
      </li>
      <li data-jstree='{"icon":"fa fa-file-text fa-2x"}'>
          Root node 2
          <div class="actions">
              <a href="/admin/interface/contents/edit_first_page/1/" class="btn btn-default follow" title="Edit"><i class="fa fa-edit"></i></a>
          </div>
      </li>
    </ul>
</div>
<script type="text/javascript">
$(function () {
    var $treeview = $("#jstree_demo_div");
    $treeview.jstree({
    "plugins" : [ "wholerow" ]
}).on('ready.jstree', function() {
    $treeview.jstree('open_all');
  });;
});
</script>

</div>
<!--
<div class="row-fluid">
    <div class="span11">
        <div class="custom-container">
            <h2>Showing all projects</h2>
            <?php if (is_array($projects) && isset($projects)){ ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th class="span11">Title</th>
                        <th class="span1">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($projects as $project ){ ?>
                    <tr id="<?php echo $project['id']; ?>">
                        <td>First page for <strong><?php echo $project['project_name']; ?></strong></td>
                        <td>
                            <a href="/admin/interface/contents/edit_first_page/<?php echo $project['id']; ?>/" class="btn follow"><i class="icon-edit"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } else { ?>
            <h5>There are no first pages to display. Try to add a project.</h5>
            <?php } ?>
        </div>
    </div>
    <div class="span1">
        <div class="right-toolbar-positioner">
            <div class="right-toolbar-included">
                <div class="custom-container right-toolbar">
                    <p class="toolbar-title">Tools:</p>
                    <hr />

                    <p><a class="btn follow" title="Show editing history"><i class="icon-time icon-2x"></i></a></p>
                </div>
            </div>
        </div>
    </div>
</div>
-->
