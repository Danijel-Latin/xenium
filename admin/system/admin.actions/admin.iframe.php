<?php

/**
 * @author Danijel
 * @copyright 2014
 */

$video_type = $_GET['video_type'];
$video_url = $_GET['video_url'];

?>



<!DOCTYPE html>
<html>
<head>
    
    <style>
    html{
        height: 100%;
    }
    
    body{
        height: 100%;
        margin: 0;
        padding: 0;
        background: #ffffff;
        overflow: hidden;
    }
    
    video{
        width: 100%;
        height: 100%;
    }
    </style>
    
  <!-- video.js -->
    <link href="/admin/theme/js/video-js/video-js.css" rel="stylesheet">
    <script src="/admin/theme/js/video-js/video.js"></script>
    <script>
        videojs.options.flash.swf = "/admin/theme/js/video-js/video-js.swf"
    </script>
    <script src="/admin/theme/js/video-js/youtube.js"></script>
    <script src="/admin/theme/js/video-js/vjs.vimeo.js"></script>
    <!-- video.js end -->
</head>
<body>
  <video id="vid<?php echo rand(); ?>" src="" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="100%" height="100%" data-setup='{ "techOrder": ["youtube"], "src": "<?php echo $video_url; ?>" }'>
  </video>
</body>
</html>
