<?php

/**
 * @author Danijel
 * @copyright 2013
 */

require($_SERVER['DOCUMENT_ROOT'] . '/admin/system/settings/config.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/theme/js/jAPI-CORE.php');

// before
/*if (!isset($_SESSION['admin_user']))
{
    die('<script type="text/javascript"> window.location = "/admin/login/"; </script>');
}*/
$XeniumAPI = new API('set_session');
$session_result = json_decode($XeniumAPI->get(), true);
if (!isset($session_result['status']) || $session_result['status'] == 'loggedOut' || !$session_result['user_info']['user_id'])
{
    //header('Location: /admin/login/');
    die('<script type="text/javascript"> window.location = "/admin/login/"; </script>');
    //die();
}

if (isset($_POST['ArgOne'])){
    $id = $_POST['ArgOne'];
    //echo $project_id;
}

if (isset($_POST['ArgTwo'])){
    $project_id = $_POST['ArgTwo'];
    //setcookie('project_id', $project_id, 0, '/');
}
else
{
    //unset($_COOKIE['project_id']);
    //setcookie('project_id', null, -1, '/');
}

if (isset($_COOKIE['project_id']))
{
    $project_id = $_COOKIE['project_id'];
}

//echo json_encode($_POST);
//$project_id = $_POST['ArgTwo'];


class AdminAction
{
    private $methods = array();
    private $plugins = array();

    public $prop;
    public $projects = array();
    //public $projects = Projects::getAll();

    public function __construct()
    {
        //$this->prop = 'neki';
        //$this->projects = Project::getAll();
        $this->load_plugins();
    }

    protected function load_plugins()
    {
        $base = $_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/plugins/';
        $plugins = glob($base . 'AdminAction_plugin_*.php');
        foreach($plugins as $plugin)
        {
            include_once $plugin;
            $name = basename($plugin, '.php');
            $className = $name;
            $obj = new $className();
            $this->plugins[$name] = $obj;

            foreach (get_class_methods($obj) as $method )
                 $this->methods[$method] = $name;

            foreach (get_object_vars($obj) as $var_name => $var_value )
            {
                if (property_exists(get_class($this), $var_name))
                {
                    if (is_array($this->{$var_name}))
                    {
                        $this->{$var_name} = array_merge_recursive($this->{$var_name}, $var_value);
                    }
                    else
                    {
                        $this->{$var_name} = $this->{$var_name} . $var_value;
                    }

                }
                else
                {
                    $this->{$var_name} = $var_value;
                }
            }
        }
    }

    public function __call($method, $args)
    {
        if(! key_exists($method, $this->methods))
           throw new Exception ("Call to undefined method: " . $method);

            // Prepends Object class - removed for the moment
           //array_unshift($args, $this);
           return call_user_func_array(array($this->plugins[$this->methods[$method]], $method), $args);
    }

    public function loadInterface($interface_data)
    {
        // default value
        $load_array = ['interface' => 'dashboard', 'action' => 'default'];
        $project_id = 0;

        if ($interface_data && $interface_data != ',')
        {
            $interface_data = trim($interface_data, ',');
            $interface_array = explode(',', $interface_data);
            foreach ($interface_array as $int_val)
            {
                $int_array = explode('-', $int_val);
                $load_array[$int_array[0]] = $int_array[1];
            }
        }

        if (isset($load_array['project']) && $load_array['project'])
        {
            $project_id = intval($load_array['project']);
        }

        // load left menu
        $menu = self::loadMenuProp('left_menu_' . $load_array['interface'], $project_id);
        if (!$project_id)
        {
            $content = self::loadMainProp('main_container_' . $load_array['interface'] . '_' . $load_array['action'], $project_id);
        }
        else
        {
            if (!isset($load_array['module']) || !$load_array['module']) $load_array['module'] = 'page';
            if (!isset($load_array['action']) || !$load_array['action'] || $load_array['action'] == 'default') $load_array['action'] = 'viewAll';
            if (!isset($load_array['id']) || !$load_array['id']) $load_array['id'] = 0;

            $content = self::loadProp($project_id, $load_array['module'], $load_array['action'], [], intval($load_array['id'])); //public function loadProp($project_id, $class, $function, $options = array(), $id = 0)
        }
        echo json_encode(['answer' => 'success', 'menu' => $menu, 'content' => $content]);
    }

    public function loadMainProp($prop_name, $project_id = 0)
    {
        //global $project_id, $additional_menu, $additional_text_editor, $additional_text_editor_modal;
        global $additional_menu, $additional_text_editor, $additional_text_editor_modal;
        //$projects = Projects::getAll();
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }


    public function loadMenuProp($prop_name, $project_id = 0){
        global $additional_menu, $additional_text_editor, $additional_text_editor_modal;
        //$projects = Project::getAll();
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }

    public function load_text_editor_prop($prop_name, $project_id)
    {
        global $additional_menu, $additional_text_editor, $additional_text_editor_modal;
        $projects = Project::getAll();
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }
    /*
    public function load_modal_prop($prop_name, $project_id)
    {
        global $additional_menu, $additional_text_editor, $additional_text_editor_modal;
        $projects = Projects::getAll();
        $current_project_offers = Offer::offers_get_by_project($project_id);
        $project_info = Projects::get_project_info_by_id($project_id);
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }
    */
    public function load_modal_prop($prop_name, $project_data)
    {
        global $additional_menu, $additional_text_editor, $additional_text_editor_modal;

        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }

    public function load_first_page_prop($prop_name, $project_id)
    {
        $fp = new FirstPage;
        $editing = 'FirstPage';
        $first_page = $fp->first_page_get_by_project_id($project_id);
        //$first_page[0] = $first_page[1]; //print_r($first_page);
        //print_r($first_page);
        $first_page_id = $first_page[0]['id'];
        $fields = $fp->fields;
        $populate_content = $fp->first_page_get_by_project_id($project_id); //print_r($populate_content);
        $project_info = Projects::get_project_info_by_id($project_id);
        $save_function = 'AdminAction.save_first_page';
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }

    public function load_project_prop($prop_name, $project_id)
    {
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }

    public function load_static_content_prop($prop_name, $id, $project_id)
    {
        $GLOBALS['project_id'] = $project_id;

        $sc = new StaticContent;
        $editing = 'StaticContent';
        $fields = $sc->fields;
        $populate_content = $sc->static_contents_get_by_id($id);
        $content_title = $populate_content[0]['title'];
        $project_info = Projects::get_project_info_by_id($project_id);
        $save_function = 'AdminAction.save_static_content';
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }

    public function load_testimonials_prop($prop_name, $id, $project_id)
    {
        $GLOBALS['project_id'] = $project_id;

        $testimonials = new Testimonials;
        $editing = 'Testimonial';
        $fields = $testimonials->fields;
        $populate_content = $testimonials->get_by_id($id);
        $content_title = $populate_content[0]['title'];
        $project_info = Projects::get_project_info_by_id($project_id);
        $save_function = 'AdminAction.save_testimonial';
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }

    public function load_blog_prop($prop_name, $id, $project_id)
    {
        $GLOBALS['project_id'] = $project_id;

        $blog = new Blog;
        $editing = 'Blog';
        $fields = $blog->fields;
        $populate_content = $blog->get_by_id($id);
        $content_title = $populate_content[0]['title'];
        $project_info = Projects::get_project_info_by_id($project_id);
        $save_function = 'AdminAction.save_blog';
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }

    public function load_text_block_prop($prop_name, $id, $project_id)
    {
        $GLOBALS['project_id'] = $project_id;

        $tb = new TextBlock;
        $editing = 'TextBlock';
        $fields = $tb->fields;
        $populate_content = $tb->get_by_id($id);
        $content_title = $populate_content[0]['title'];
        $project_info = Projects::get_project_info_by_id($project_id);
        $save_function = 'AdminAction.save_text_block';
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }

    public function load_email_prop($prop_name, $id, $project_id)
    {
        $xMail_id = new Emails;
        $editing = 'Emails';
        $fields = $xMail_id->emails_fields;
        $populate_content = $xMail_id->email_get_by_id($id);
        $content_title = $populate_content[0]['title'];
        $project_info = Projects::get_project_info_by_id($project_id);
        $save_function = 'AdminAction.save_email';
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }

    public function loadTextEditor($project_id = 'all')
    {
        global $additional_text_editor, $additional_text_editor_modal;
        $additional_text_editor = array();
        $additional_text_editor_modal = array();
        $filename = $_SERVER['DOCUMENT_ROOT'] . '/admin/system/classes.modules/class.*.php';
        foreach (glob($filename) as $filefound)
        {
            //echo "$filefound size " . filesize($filefound) . "\n";
            $tokens = token_get_all(file_get_contents($filefound));
            $comments = array();
            $counter = 0;
            foreach($tokens as $token) {
                if($token[0] == T_COMMENT || $token[0] == T_DOC_COMMENT) {
                    $comments[] = $token[1];
                    $module_name_pattern = "/@moduleName (.*?)\n/";
                    $module_text_editor_pattern = "/@moduleTextEditor (.*?)\n/";
                    $module_text_editor_modal_pattern = "/@moduleTextEditorModal (.*?)\n/";
                    //preg_match($module_category_pattern, $token[1], $category_matches);
                    preg_match($module_name_pattern, $token[1], $name_matches);
                    preg_match($module_text_editor_pattern, $token[1], $text_editor_matches);
                    preg_match($module_text_editor_modal_pattern, $token[1], $text_editor_modal_matches);
                    //print_r($name_matches);
                    //print_r($text_editor_matches);



                    if (isset($text_editor_matches[1]) && preg_replace('/\s+/', '', $text_editor_matches[1]) == 'true')
                    {
                        $class_name = preg_replace('/\s+/', '', $name_matches[1]);

                        if ((double)phpversion() >= 5.3)
                        {
                            $additional_text_editor[$class_name] = call_user_func($class_name .'::' . preg_replace('/\s+/', '', strtolower($name_matches[1])) . '_text_editor_add');
                        }
                        else
                        {
                            $additional_text_editor[$counter] = call_user_func(array($class_name, preg_replace('/\s+/', '', strtolower($name_matches[1])) . '_text_editor_add'));
                        }
                    }

                    if (isset($text_editor_modal_matches[1]) && preg_replace('/\s+/', '', $text_editor_modal_matches[1]) == 'true')
                    {
                        if ((double)phpversion() >= 5.3)
                        {
                            $additional_text_editor_modal[$class_name] = call_user_func_array($class_name .'::' . preg_replace('/\s+/', '', strtolower($name_matches[1])) . '_text_editor_modal_add', array($project_id));
                        }
                        else
                        {
                            $additional_text_editor_modal[$counter] = call_user_func_array(array($class_name, preg_replace('/\s+/', '', strtolower($name_matches[1])) . '_text_editor_modal_add'), array($project_id));
                        }
                    }

                    $counter++;
                }
            }
        }
        //print_r($additional_text_editor);
        echo self::load_text_editor_prop('text_editor', $project_id);
    }

    /*public function load_left_menu($menu_name, $project_id)
    {
        // bof get all dynamic contents
        global $additional_menu;
        $additional_menu = array();
        $filename = $_SERVER['DOCUMENT_ROOT'] . '/admin/system/classes.modules/class.*.php';
        foreach (glob($filename) as $filefound)
        {
            //echo "$filefound size " . filesize($filefound) . "\n";
            $tokens = token_get_all(file_get_contents($filefound));
            $comments = array();
            foreach($tokens as $token) {
                if($token[0] == T_COMMENT || $token[0] == T_DOC_COMMENT) {
                    $comments[] = $token[1];
                    $module_category_pattern = "/@moduleCategory (.*?)\n/";
                    $module_name_pattern = "/@moduleName (.*?)\n/";
                    preg_match($module_category_pattern, $token[1], $category_matches);
                    preg_match($module_name_pattern, $token[1], $name_matches);
                    //print_r($matches);
                    if (isset($category_matches[1])){
                        if (isset($name_matches[1])) $left_menu_additional[$category_matches[1]] = array($name_matches[1]);
                        //print_r($left_menu_additional);
                        if (!isset($additional_menu[preg_replace('/\s+/', '', $category_matches[1])]))
                        {
                            if ((double)phpversion() >= 5.3)
                            {
                                $additional_menu[preg_replace('/\s+/', '', $category_matches[1])] = call_user_func(preg_replace('/\s+/', '', $name_matches[1]) .'::' . preg_replace('/\s+/', '', strtolower($name_matches[1])) . '_menu_add');
                            }
                            else
                            {
                                $additional_menu[preg_replace('/\s+/', '', $category_matches[1])] = call_user_func(array(preg_replace('/\s+/', '', $name_matches[1]), preg_replace('/\s+/', '', strtolower($name_matches[1])) . '_menu_add'));
                            }
                            //$additional_menu[preg_replace('/\s+/', '', $category_matches[1])] = call_user_func(preg_replace('/\s+/', '', $name_matches[1]) .'::' . preg_replace('/\s+/', '', strtolower($name_matches[1])) . '_menu_add');

                        }
                        else
                        {
                            if ((double)phpversion() >= 5.3)
                            {
                                $additional_menu[preg_replace('/\s+/', '', $category_matches[1])] .= call_user_func(preg_replace('/\s+/', '', $name_matches[1]) .'::' . preg_replace('/\s+/', '', strtolower($name_matches[1])) . '_menu_add');
                            }
                            else
                            {
                                $additional_menu[preg_replace('/\s+/', '', $category_matches[1])] .= call_user_func(array(preg_replace('/\s+/', '', $name_matches[1]), preg_replace('/\s+/', '', strtolower($name_matches[1])) . '_menu_add'));
                            }
                        }

                        //$additional_menu[preg_replace('/\s+/', '', $category_matches[1])] = '';
                        //$new_class = new preg_replace('/\s+/', '', $name_matches[1]);
                        //$additional_menu[preg_replace('/\s+/', '', $category_matches[1])] .= call_user_func(array($offer, preg_replace('/\s+/', '', strtolower($name_matches[1])) . '_menu_add'));
                        //echo str_replace(' ', '', $category_matches[1]);
                        //echo $additional_menu[preg_replace('/\s+/', '', $category_matches[1])];
                    }
                    //if (isset($name_matches[1])) echo str_replace(' ', '', $name_matches[1]);
                }
            }
            //print_r($comments);
        }
        // eof get all dynamic contents

        echo self::loadMenuProp('left_menu_' . $menu_name, $project_id);
    }*/

    public function viewAll($project_id, $class)
    {
        echo self::loadProp($project_id, $class, __FUNCTION__);
    }

    public function viewTrashed($project_id, $class)
    {
        $options = array('trashed' => 1);
        echo self::loadProp($project_id, $class, __FUNCTION__, $options);
    }

    public function viewHistory($project_id, $class)
    {
        echo self::loadProp($project_id, $class, __FUNCTION__);
    }

    public function edit($project_id, $class, $id)
    {
        $options = array();
        echo self::loadProp($project_id, $class, __FUNCTION__, $options, $id);
    }

    public function publish($class_name, $id, $project_id, $published)
    {
        $class = new $class_name;
        $class->publish($id, $project_id, $published);
    }

    public function trash($class_name, $id, $project_id, $trashed)
    {
        $class = new $class_name;
        $class->trash($id, $project_id, $trashed);
    }

    public function save($class_name, $id, $project_id, $new_values)
    {
        $class = new $class_name;
        echo $class->save($id, $project_id, $new_values);
    }

    public function insertNew($project_id, $class, $parent)
    {
        $class_object = new $class;
        $class_object->insertNew($project_id, $parent);
    }

    public function updateTitle($project_id, $class, $id, $title)
    {
        $class_object = new $class;
        $class_object->updateTitle($project_id, $id, $title);
    }

    public function updateParent($project_id, $class, $id, $parent_id)
    {
        $class_object = new $class;
        $class_object->updateParent($project_id, $id, $parent_id);
    }

    public function updateOrder($project_id, $class, $new_order)
    {
        $class_object = new $class;
        $class_object->updateOrder($project_id, $new_order);
    }

    public function loadProp($project_id, $class, $function, $options = array(), $id = 0)
    {
        //if ($class != 'undefined')
        //{
            $content_class = new $class;
            $title = $content_class->module_name;

            if (isset($options['trashed']) && $options['trashed']) $title .= ' (trashed)';

            if ($function == 'edit' && $id)
            {
                $content = $content_class->adminGetContent($project_id, $id, $options); // get single content
                $title_button_group = $content_class->adminGetModuleEditMenu($project_id, $content);
                $load_prop = 'template_main_container_edit';
            }
            else
            {
                $contents = $content_class->adminGetContents($project_id, $options);
                $title_button_group = $content_class->adminGetModuleMenu($project_id);
                $load_prop = $content_class->module_menu_structure[$function]['load_prop'];
            }

            ob_start();
            include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $load_prop . '.php');
            $prop = ob_get_contents();
            ob_end_clean();
            return $prop;
        //}
    }

    public function saveCondition($project_id, $class_name, $condition_id, $condition_name, $condition_json, $condition_layout)
    {
        $project = new Project;
        $project_info = $project->getById($project_id);
        $project_slug = $project_info['project_slug'];
        //echo $condition_json;
        $conditions_url = DIR_PROJECTS . 'project_' . $project_slug . '/conditions/';
        //echo $conditions_url;

        $return_array = [
            'new' => false,
            'project_id' => $project_id,
            'class_name' => $class_name,
            'condition_id' => $condition_id,
            'condition_name' => $condition_name,
            'condition_json' => $condition_json,
            'condition_layout' => $condition_layout
        ];

        if (!$condition_id)
        {
            $condition_id = sha1(randomPassword() . '<' . rand() . '>' . randomSerial());
            //echo json_encode(['condition_id' => $condition_id]);
            $return_array['new'] = true;
            $return_array['condition_id'] = $condition_id;

            $return_array['new_html'] = '<tr class="condition" id="condition-' . condition_id . '">
              <td class="condition-activation">
                <i class="icon icon-linea-arrows-10-92"></i>
                <input type="hidden" value="' . $condition_id . '" />
                <textarea class="invisible">' . $condition_json . '</textarea>
              </td>
              <td class="condition-name">
                ' . $condition_name . '
              </td>
              <td>
                  <div class="btn-group">
                      <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                      </button>
                      <div class="dropdown-menu">
                          <a class="dropdown-item" href="#">Action <span class="layout-actions"><i class="fa fa-edit"></i> <i class="fa fa-close"></i></span></a>
                          <a class="dropdown-item" href="#">Another action <span class="layout-actions"><i class="fa fa-edit"></i> <i class="fa fa-close"></i></span></a>
                          <a class="dropdown-item" href="#">Something else here <span class="layout-actions"><i class="fa fa-edit"></i> <i class="fa fa-close"></i></span></a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="#">Separated link</a>
                      </div>
                  </div>
              </td>
              <td class="actions">
                  <a href="#" role="button" class="btn btn-secondary edit-condition" title="Edit" data-toggle="modal" data-target="#conditionsModal"><i class="icon icon-linea-software-10-59"></i></a>
                  <a href="#" role="button" class="btn btn-secondary" title="Remove"><i class="icon icon-linea-arrows-10-57"></i></a>
              </td>
            </tr>';
        }

        $condition_content = '<?php
        $project_id = \'' . $project_id . '\';
        $class_name = \'' . $class_name . '\';
        $condition_id = \'' . $condition_id . '\';
        $condition_name = \'' . $condition_name . '\';
        $condition_json = \'' . $condition_json . '\';
        $condition_layout = \'' . $condition_layout . '\';

        ' . Condition::generate($condition_json, $condition_layout) . '

        ?>';
        //$condition_content = 'neki tle';

        if (file_exists($conditions_url . $condition_id . '_' . $class_name . '.php'))
        {
            unlink($conditions_url . $condition_id . '_' . $class_name . '.php');
        }

        file_put_contents($conditions_url . $condition_id . '_' . $class_name . '.php', $condition_content);

        echo json_encode($return_array);
    }

    public function deleteCondition($project_id, $class_name, $condition_id)
    {
        $project = new Project;
        $project_info = $project->getById($project_id);
        $project_slug = $project_info['project_slug'];
        //echo $condition_json;
        $conditions_url = DIR_PROJECTS . 'project_' . $project_slug . '/conditions/';

        unlink($conditions_url . $condition_id . '_' . $class_name . '.php');

        echo json_encode(['answer' => 'success']);
    }

    public function saveLayout($project_id, $layout_name, $layout_html)
    {
        $project = new Project;
        $project_info = $project->getById($project_id);
        $project_slug = $project_info['project_slug'];
        //echo $condition_json;
        $layouts_url = DIR_PROJECTS . 'project_' . $project_slug . '/layouts/';

        if (file_exists($layouts_url . $layout_name . '.html'))
        {
            unlink($layouts_url . $layout_name . '.html');
        }
        file_put_contents($layouts_url . $layout_name . '.html', $layout_html);

        $layouts = Layout::getAll($project_id);
        $layout_dropdown = Layout::generateDropdown($layouts);

        echo json_encode(['answer' => 'success', 'layout_name' => $layout_name, 'layout_html' => $layout_html, 'layout_dropdown' => $layout_dropdown]);
    }

    public function deleteLayout($project_id, $layout_name)
    {
        $project = new Project;
        $project_info = $project->getById($project_id);
        $project_slug = $project_info['project_slug'];
        //echo $condition_json;
        $layouts_url = DIR_PROJECTS . 'project_' . $project_slug . '/layouts/';

        if (file_exists($layouts_url . $layout_name . '.html'))
        {
            unlink($layouts_url . $layout_name . '.html');
        }

        $layouts = Layout::getAll($project_id);
        $layout_dropdown = Layout::generateDropdown($layouts);

        echo json_encode(['answer' => 'success', 'layout_dropdown' => $layout_dropdown]);
    }

    public function getLayout($project_id, $layout_name)
    {
        $project = new Project;
        $project_info = $project->getById($project_id);
        $project_slug = $project_info['project_slug'];
        //echo $condition_json;
        $layouts_url = DIR_PROJECTS . 'project_' . $project_slug . '/layouts/';

        if (file_exists($layouts_url . $layout_name . '.html'))
        {
            $layout = file_get_contents($layouts_url . $layout_name . '.html');
            echo json_encode(['answer' => 'success', 'layout' => $layout]);
        }
        else
        {
            echo json_encode(['answer' => 'error']);
        }
    }

    public function installProject($project_id, $project_name, $project_slug, $repo_url)
    {
        $result = Git::installProject($project_id, $project_name, $project_slug, $repo_url);
        echo json_encode($result);
    }

    public function commitProject($repo_folder)
    {
        $repo = new Git($repo_folder);
        $repo->addAllChanges();
        $repo->commit('Xenium - commiting new changes');
        $remote_name = $repo->getRemoteInfo();
        $remote_name = $remote_name[0];
        $remote_branch = $repo->getCurrentBranchName();
        $repo->push($remote_name . ' ' . $remote_branch);

        echo json_encode(['answer' => 'success', 'message' => 'Changes commited successfully.']);
    }

    public function checkoutProject($repo_folder, $branch)
    {
        $repo = new Git($repo_folder);
        $repo->checkout($branch);
        $version = str_replace('release/', 'v.', $branch);

        echo json_encode(['answer' => 'success', 'message' => 'Switched successfully to ' . $version . '.']);
    }

    public function checkForUpdates()
    {
        global $UPDATE_INFO;
        $answer = array();

        $api = new API('interface/update_check');
        $UPDATE_INFO = json_decode($api->get('project_ids[]=1&project_ids[]=2'), true); print_r($update_info);

        $answer = Git::checkForUpdates(DIR_WWW);

        foreach (glob(DIR_WWW.'projects/project_*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir_project) {
            //$dir_array = explode('/', $dir);
            //$dir_project = end($dir_array);
            $answer = Git::checkForUpdates($dir_project, $answer);
        }

        //$changes = $repo->status(); //print_r($changes);

        echo json_encode($answer);
    }
/*
    public function loadProp($prop_name, $title, $title_button_group, $contents)
    {
        //global $project_id, $additional_menu, $additional_text_editor, $additional_text_editor_modal;
        global $additional_menu, $additional_text_editor, $additional_text_editor_modal;
        $projects = Projects::getAll();
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/admin/system/admin.actions/props/' . $prop_name . '.php');
        $prop = ob_get_contents();
        ob_end_clean();
        return $prop;
    }
*/
    public function dashboard_default()
    {
        echo self::loadMainProp('main_container_dashboard_default');
        //echo self::loadProp($project_id, $class, __FUNCTION__);
    }

    public function applications_default($period)
    {
        global $set_period, $payment_stats, $today_count, $today_amount, $week_count, $week_amount, $month_count, $month_amount;

        if ($period == 'undefined')
        {
            $set_period = 'day';
        }
        else
        {
            $set_period = $period;
        }

        $payment_stats = Payment::get_payment_stats($set_period);

        echo self::loadProp('main_container_applications_default');
    }

    public function users_default($limit_start, $limit, $search_type, $search_term)
    {
        global $users, $projects, $items_count, $page_limit_start, $page_limit, $search_type_load, $search_term_load;

        $search_type_load = $search_type;
        $search_term_load = $search_term;

        if ($limit_start == 'undefined')
        {
            $limit_start = 0;
        }
        if ($limit == 'undefined')
        {
            $limit = 10;
        }

        $page_limit_start = $limit_start;
        $page_limit = $limit;

        //$offers = Offer::offers_get_all();
        if ($search_type == 'undefined')
        {
            $items_count = Users::users_get_count();
        }
        elseif ($search_type == 'search-simple')
        {
            $items_count = Users::users_get_simple_search_count($search_term);
        }
        elseif ($search_type == 'search-advanced')
        {
            $items_count = Users::users_get_advanced_search_count($search_term);
        }

        $users = Users::users_get_all_limit_search($limit_start, $limit, $search_type, $search_term);
        $projects = Project::getAll();
        echo self::loadProp('main_container_users_default');
    }

    public function users_stats($period)
    {
        global $set_period, $users_stats, $today_count, $today_activated, $week_count, $week_activated, $month_count, $month_activated;

        if ($period == 'undefined')
        {
            $set_period = 'day';
        }
        else
        {
            $set_period = $period;
        }

        $users_stats = Users::get_users_stats($set_period);

        echo self::loadProp('main_container_users_stats');
    }

    public function multimedia_default()
    {
        echo self::loadProp('main_container_multimedia_default');
    }

    public function settings_default()
    {
        echo self::loadProp('main_container_settings_default');
    }

    public function storage_settings()
    {
        echo self::loadProp('main_container_settings_storage');
    }

    public function emails_default()
    {
        global $emails;
        $emails = Emails::emails_get_all();
        echo self::loadProp('main_container_emails_default');
    }

    public function add_new_email($title, $project_id)
    {
        $new_id = Emails::email_insert_new($title, $project_id);
        echo '<a id="redirect" class="follow" style="display: none;" href="/admin/interface/contents/edit_email/' . $new_id . '_' . $project_id . '/"></a><script type="text/javascript"> $("#redirect").trigger("click"); </script>';
    }

    public function edit_email($id, $project_id)
    {
        echo self::load_email_prop('default_email_edit_form', $id, $project_id);
    }

    public function trash_email($id)
    {
        Emails::emails_to_trash($id);
    }

    public function save_email($id, $project_id, $new_values)
    {
        Emails::emails_save($id, $project_id, $new_values);
    }

    public function translations_default($language_from, $language_to)
    {
        global $translations, $language_from_code, $language_to_code, $language_from_name, $language_to_name, $all_languages;

        $all_languages = Languages::get_all_www_languages_list();

        if (isset($language_from) && $language_from != 'undefined')
        {
            $language_from_code = $language_from;
        }
        else
        {
            $language_from_code = 'en';
        }

        if (!isset($language_to) || $language_to == 'undefined')
        {
            foreach ($all_languages as $language)
            {
                if ($language['code'] != 'en')
                {
                    $language_to_code = $language['code'];
                    break;
                }
            }
        }
        else
        {
            $language_to_code = $language_to;
        }
        $language_from_name = Languages::get_language_name($language_from_code);
        $language_to_name = Languages::get_language_name($language_to_code);
        $translations = Translations::get_all_translations_admin();
        echo self::loadProp('main_container_translations_default');
    }

    public function translations_save($language_to, $new_values)
    {
        $new_values = str_replace('ANDPARAMETER', '&', $new_values);
        $new_values = str_replace('%3Cbr+style%3D%22%22%3E', '', $new_values);
        $new_values = str_replace('%0D%0A%3C%2Fp%3E%3Cbr+style%3D%22%22+class%3D%22aloha-cleanme%22%3E', '', $new_values);
        $new_values = str_replace('<br style="">', '', $new_values);
        $new_values = str_replace('<br style="" class="aloha-cleanme">', '', $new_values);
        //echo $new_values;
        //Translations::translations_save($language_to, $new_values);
        $translations = new Translations;
        $translations->translations_save($language_to, $new_values);
    }

    public function confirm_payment($id)
    {
        $result = Payment::confirm_payment_offline($id);
        if ($result)
        {
            $answer = json_encode(array('result' => 1));
        }
        else
        {
            $answer = json_encode(array('result' => 0));
        }

        echo $answer;
    }

    public function applications_exports()
    {
        echo self::loadProp('main_container_applications_exports');
    }

    public function user_activate($user_id)
    {
        echo Users::admin_activate_user($user_id);
    }

    public function logoff_user()
    {
        unset($_SESSION['admin_user']);
    }


}


//echo $project_id;


/*if (isset($project_id))
{
    //echo $project_id;
}*/
//$project_id = 1;



new jAPIBaseClass('AdminAction');
//xMemcache::ajax_write_memcache_settings();

/*xMemcache::get_memcache_settings();

if (isset($project_id))
{
    //echo $project_id;
    xMemcache::write_memcache_settings();
} else {*/
    /*
    $projects = Projects::getAll();

    foreach ($projects as $project)
    {
        $project_id = $project['id'];
        xMemcache::write_memcache_settings();
    }
    */
//}

//echo $_SERVER['REQUEST_URI'];

?>
