<?php

spl_autoload_register(function($class_name)
{
    global $project_slug;

    if (isset($project_slug) && $project_slug && file_exists(DIR_WWW . 'projects/' . $project_slug . '/settings/modules/' . strtolower($class_name) . '/class.' . strtolower($class_name) . '.php'))
    {//echo $class_name . ' found!';
        require_once(DIR_WWW . 'projects/' . $project_slug . '/settings/modules/' . strtolower($class_name) . '/class.' . strtolower($class_name) . '.php');

        /*if (method_exists($class_name, 'loadCacheSetting') && call_user_func_array(array($class_name, 'loadCacheSetting'), array($class_name)))
        {
            $memcache = new xMemcache;
            $memcache->get_memcache_group(strtolower($class_name), 'group');
            $memcache->get_memcache_group(strtolower($class_name), 'single');
        }*/

        return;
    }
    elseif (file_exists(DIR_SETTINGS . '/../modules/' . strtolower($class_name) . '/class.' . strtolower($class_name) . '.php'))
    {
        require_once(DIR_SETTINGS . '/../modules/' . strtolower($class_name) . '/class.' . strtolower($class_name) . '.php');

        /*if (method_exists($class_name, 'loadCacheSetting') && call_user_func_array(array($class_name, 'loadCacheSetting'), array($class_name)))
        {
            $memcache = new xMemcache;
            $memcache->get_memcache_group(strtolower($class_name), 'group');
            $memcache->get_memcache_group(strtolower($class_name), 'single');
        }*/

        return;
    }
    else
    {
        //echo 'Class file for ' . $class_name . ' does not exist';
    }
});

function class_autoloader($class_name)
{
    global $project_slug;

    if (isset($project_slug) && $project_slug && file_exists(DIR_WWW . 'projects/' . $project_slug . '/settings/modules/' . strtolower($class_name) . '/class.' . strtolower($class_name) . '.php'))
    {
        require_once(DIR_WWW . 'projects/' . $project_slug . '/settings/modules/' . strtolower($class_name) . '/class.' . strtolower($class_name) . '.php');

        /*if (method_exists($class_name, 'loadCacheSetting') && call_user_func_array(array($class_name, 'loadCacheSetting'), array($class_name)))
        {
            $memcache = new xMemcache;
            $memcache->get_memcache_group(strtolower($class_name), 'group');
            $memcache->get_memcache_group(strtolower($class_name), 'single');
        }*/

        return;
    }
    elseif (file_exists(DIR_SETTINGS . '/../modules/' . strtolower($class_name) . '/class.' . strtolower($class_name) . '.php'))
    {
        require_once(DIR_SETTINGS . '/../modules/' . strtolower($class_name) . '/class.' . strtolower($class_name) . '.php');

        /*if (method_exists($class_name, 'loadCacheSetting') && call_user_func_array(array($class_name, 'loadCacheSetting'), array($class_name)))
        {
            $memcache = new xMemcache;
            $memcache->get_memcache_group(strtolower($class_name), 'group');
            $memcache->get_memcache_group(strtolower($class_name), 'single');
        }*/

        return;
    }
    else
    {
        //echo 'Class file for ' . $class_name . ' does not exist';
    }
}

function getServerOS(){
    return strtoupper(substr(PHP_OS, 0, 3));
}

function getDirs()
{
    global $project_slug;

    $server_OS = getServerOS();

    if ($server_OS == 'WIN')
    {
        define( 'DIR_SETTINGS', str_replace("\\", "/", dirname(__FILE__) ));
    }
    else
    {
        define( 'DIR_SETTINGS', dirname(__FILE__) );
    }

    define( 'DIR_WWW', DIR_SETTINGS . '/../../../' );
    define( 'DIR_ROOT', DIR_SETTINGS . '/../../../' );
    define( 'DIR_PROJECTS', DIR_SETTINGS . '/../../../projects/' );
    define( 'DIR_MODULES', DIR_SETTINGS . '/../modules/' );
    //define( 'DIR_PROJECT', DIR_SETTINGS . '/../../projects/' . $project_slug );
}

function sanitize($value)
{
    htmlentities( $value, ENT_QUOTES, 'utf-8');

    $search=array("\\","\0","\n","\r","\x1a","'",'"');
    $replace=array("\\\\","\\0","\\n","\\r","\Z","\'",'\"');

    return str_replace($search, $replace, $value);
}

function randomPassword()
{
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++)
    {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function randomSerial($limit_chars = 10, $division_every = 5)
{
    $chars = array(0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    $serial = '';
    $max = count($chars)-1;
    for($i=0;$i<$limit_chars;$i++){
        $serial .= (!($i % $division_every) && $i ? '-' : '').$chars[rand(0, $max)];
    }
    return $serial;
}

function var_debug($variable,$strlen=100,$width=25,$depth=10,$i=0,&$objects = array())
{
  $search = array("\0", "\a", "\b", "\f", "\n", "\r", "\t", "\v");
  $replace = array('\0', '\a', '\b', '\f', '\n', '\r', '\t', '\v');

  $string = '';

  switch(gettype($variable)) {
    case 'boolean':      $string.= '<small>boolean</small> <font color="#cc0000">' . $variable?'true':'false' . '</font>'; break;
    case 'integer':      $string.= '<small>integer</small> <font color="#cc0000">' . $variable . '</font>';                break;
    case 'double':       $string.= '<small>integer</small> <font color="#cc0000">' . $variable . '</font>';                break;
    case 'resource':     $string.= '<small>[resource]</small>';             break;
    case 'NULL':         $string.= '<font color="#cc0000">null</font>';                   break;
    case 'unknown type': $string.= '<font color="#cc0000">???</font>';                    break;
    case 'string':
      $len = strlen($variable);
      $variable = str_replace($search,$replace,substr($variable,0,$strlen),$count);
      $variable = substr($variable,0,$strlen);
      $string .= '<small>string</small> <font color="#cc0000">';
      $string .= "'";
      $string.= $variable;
      if ($len>$strlen) $string .= '...';
      $string .= "'";
      //if ($len<$strlen) $string.= "'".$variable."'";
      //else $string.= 'string('.$len.'): "'.$variable.'"...';
      $string .= '</font> <i>(length=' . $len . ')</i>';
      break;
    case 'array':
      $len = count($variable);
      /*if ($i==$depth) $string.= 'array('.$len.') {...}';
      elseif(!$len) $string.= 'array(0) {}';
      else {
        $keys = array_keys($variable);
        $spaces = str_repeat(' ',$i*2);
        $string.= "array($len)\n".$spaces.'{';
        $count=0;
        foreach($keys as $key) {
          if ($count==$width) {
            $string.= "\n".$spaces."  ...";
            break;
          }
          $string.= "\n".$spaces."  [$key] => ";
          $string.= var_debug($variable[$key],$strlen,$width,$depth,$i+1,$objects);
          $count++;
      }
        $string.="\n".$spaces.'}';
      }*/
      $string = '<small>array</small> <i>(size=' . $len . ')</i>';
      break;
    case 'object':
      $id = array_search($variable,$objects,true);
      if ($id!==false)
        $string.=get_class($variable).'#'.($id+1).' {...}';
      else if($i==$depth)
        $string.=get_class($variable).' {...}';
      else {
        $id = array_push($objects,$variable);
        $array = (array)$variable;
        $spaces = str_repeat(' ',$i*2);
        $string.= get_class($variable)."#$id\n".$spaces.'{';
        $properties = array_keys($array);
        foreach($properties as $property) {
          $name = str_replace("\0",':',trim($property));
          $string.= "\n".$spaces."  [$name] => ";
          $string.= var_debug($array[$property],$strlen,$width,$depth,$i+1,$objects);
        }
        $string.= "\n".$spaces.'}';
      }
      break;
  }

  if ($i>0) return $string;

  //$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
  //do $caller = array_shift($backtrace); while ($caller && !isset($caller['file']));
  //if ($caller) $string = $caller['file'].':'.$caller['line']."\n".$string;

  return '<pre>' . $string . '</pre>';
}

function convertMemory($size)
{
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function convertTime($time)
{
    $time = floatval($time);
    if ($time >= 1)
    {
        return number_format($time, 2) . ' s';
    }
    else
    {
        return ($time * 1000) . ' ms';
    }
}

function trim_text($input, $length, $ellipses = true, $strip_html = true)
{
	//strip tags, if desired
	if ($strip_html) {
		$input = strip_tags($input);
	}

	//no need to trim, already shorter than trim length
	if (strlen($input) <= $length) {
		return $input;
	}

	//find last space within length
	$last_space = strrpos(substr($input, 0, $length), ' ');
	$trimmed_text = substr($input, 0, $last_space);

	//add ellipses (...)
	if ($ellipses) {
		$trimmed_text .= '...';
	}

	return $trimmed_text;
}

set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext)
{
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }

    switch ($errno)
    {
        case E_ERROR:
            $label = 'Fatal error';
            break;

        case E_WARNING:
            $label = 'Warning';
            break;

        case E_PARSE:
            $label = 'Parse error';
            break;

        case E_NOTICE:
            $label = 'Notice';
            break;

        case E_CORE_ERROR:
            $label = 'Fatal core error';
            break;

        case E_CORE_WARNING:
            $label = 'Core warning';
            break;

        case E_COMPILE_ERROR:
            $label = 'Fatal compile error';
            break;

        case E_COMPILE_WARNING:
            $label = 'Compile warning';
            break;

        case E_USER_ERROR:
            $label = 'Fatal error';
            break;

        case E_USER_WARNING:
            $label = 'Warning';
            break;

        case E_USER_NOTICE:
            $label = 'Notice';
            break;

        case E_STRICT:
            $label = 'Strict';
            break;

        case E_RECOVERABLE_ERROR:
            $label = 'Fatal recoverable error';
            break;

        case E_DEPRECATED:
            $label = 'Deprecated';
            break;

        case E_USER_DEPRECATED:
            $label = 'Deprecated';
            break;

        default:
            $label = 'Unknown error';
            break;
    }

    //throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    //$errors[] = [$errstr, 0, $errno, $errfile, $errline];
    $error_message = '<span style="font-weight: bold;">' . $errstr . '</span> <br /><span class="calling-file">in file <span style="font-weight: bold;">"' . $errfile . '"</span> on line <span style="font-weight: bold;">' . $errline . '</span></span>';
    Xdebugger::addMessage(trim($error_message), $label, true);
});

register_shutdown_function("shutdownHandler");

function shutdownHandler() //will be called when php script ends.
{
    $lasterror = error_get_last();
    $fatal_error = false;
    switch ($lasterror['type'])
    {
        case E_ERROR:
            $label = 'Fatal error';
            $fatal_error = true;
            break;
        case E_CORE_ERROR:
            $label = 'Fatal core error';
            $fatal_error = true;
            break;
        case E_COMPILE_ERROR:
            $label = 'Fatal compile error';
            $fatal_error = true;
            break;
        case E_USER_ERROR:
            $label = 'Fatal error';
            $fatal_error = true;
            break;
        case E_RECOVERABLE_ERROR:
            $label = 'Fatal recoverable error';
            $fatal_error = true;
            break;
        case E_CORE_WARNING:
            $label = 'Core warning';
            $fatal_error = true;
            break;
        case E_COMPILE_WARNING:
            $label = 'Compile warning';
            $fatal_error = true;
            break;
        case E_PARSE:
            $label = 'Parse error';
            $fatal_error = true;
            break;
        /*default:
            $label = 'Unknown error';
            $fatal_error = true;
            break;*/
    }

    if ($fatal_error)
    {
        $error = '<span style="font-weight: bold;">[SHUTDOWN] ' . $lasterror['message'] . '</span><br /><span class="calling-file">in file <span style="font-weight: bold;">"' . $lasterror['file'] . '"</span> on line <span style="font-weight: bold;">' . $lasterror['line'] . '</span></span>';

        Xdebugger::addMessage(trim($error), $label, true);
        $debugger = new Xdebugger;
        $debugger->debug();
    }
}

// add info message to debugger
function d($message)
{
    // get file and line info
    $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    $backtrace = $backtrace[0];
    if (is_string($message))
    {
        $message = '<span style="font-weight: bold;">' . $message . '</span><br /><span class="calling-file">in file <span style="font-weight: bold;">"' . $backtrace['file'] . '"</span> on line <span style="font-weight: bold;">' . $backtrace['line'] . '</span></span>';
    }
    /*else
    {
        $message = '<span style="font-weight: bold;">' . var_debug($message) . '</span><br /><span class="calling-file">in file <span style="font-weight: bold;">"' . $backtrace['file'] . '"</span> on line <span style="font-weight: bold;">' . $backtrace['line'] . '</span></span>';
    }*/
    //Xdebugger::addMessage($message, 'Info', true);
    Xdebugger::addMessage($message, 'Info', $backtrace);
}

// stop the script, show debugger, optionally add a message
function dd($message = '')
{
    if ($message)
    {
        // get file and line info
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $backtrace = $backtrace[0];
        if (is_string($message))
        {
            $message = '<span style="font-weight: bold;">' . $message . '</span><br /><span class="calling-file">in file <span style="font-weight: bold;">"' . $backtrace['file'] . '"</span> on line <span style="font-weight: bold;">' . $backtrace['line'] . '</span></span>';
        }
        /*else
        {
            $message = '<span style="font-weight: bold;">' . var_debug($message) . '</span><br /><span class="calling-file">in file <span style="font-weight: bold;">"' . $backtrace['file'] . '"</span> on line <span style="font-weight: bold;">' . $backtrace['line'] . '</span></span>';
        }*/
        Xdebugger::addMessage($message, 'Info', $backtrace);
    }

    echo '<span></span>'; // ghost element, otherwise debugbar is not displayed
    $debugger = new Xdebugger;
    $debugger->debug();
    die();
}

function getInterfaceId()
{
    if (!defined('INTERFACE_TOKEN'))
    {
        foreach (glob(DIR_SETTINGS . '/interface/*.id.xinfo') as $filename)
        {
            $interface_array = unserialize(file_get_contents($filename));
            define('INTERFACE_TOKEN', $interface_array['id']);
            define('INTERFACE_TYPE', $interface_array['type']);
            break;
        }
    }
}

?>
