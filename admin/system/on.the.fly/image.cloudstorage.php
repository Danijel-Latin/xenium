<?php

/**
 * @author Danijel
 * @copyright 2014
 */

session_cache_limiter('');
header('Content-type:image/' . strtolower(substr(strrchr($_GET['ext'],"."),1)));
require($_SERVER['DOCUMENT_ROOT'] . '/admin/system/settings/config.php');

$longExpiryOffset = 315360000;
header ( "Cache-Control: public, max-age=" . $longExpiryOffset );
header ( "Expires: " . gmdate ( "D, d M Y H:i:s", time () + $longExpiryOffset ) . " GMT" );

$provider = sanitize($_GET['provider']);
$id = sanitize($_GET['id']);

$tmp_folder = $_SERVER['DOCUMENT_ROOT'] . '/uploads/image/tmp/';
$tmp_file = $tmp_folder . $id . '.' . sanitize($_GET['ext']);
if (file_exists($tmp_file))
{
    $redirect = 'http://' . $_SERVER['HTTP_HOST'] . '/uploads/image/tmp/' . $id . '.' . sanitize($_GET['ext']);
    header( 'Location: ' . $redirect ) ;
    die();
}

// get crop width and height if set
if (isset($_GET['width']))
{
    $resize_x = sanitize($_GET['width']);
}

if (isset($_GET['height']))
{
    $resize_y = sanitize($_GET['height']);
}

if (!isset($_GET['resize_mode']))
{
    $resize_mode = 6;
    $resize_mode_name = 'ZEBRA_IMAGE_CROP_CENTER';
}
else
{
    $resize_mode = $_GET['resize_mode'];
    if ($resize_mode == 0)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_BOXED';
    }
    elseif ($resize_mode == 1)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_NOT_BOXED';
    }
    elseif ($resize_mode == 2)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_CROP_TOPLEFT';
    }
    elseif ($resize_mode == 3)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_CROP_TOPCENTER';
    }
    elseif ($resize_mode == 4)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_CROP_TOPRIGHT';
    }
    elseif ($resize_mode == 5)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_CROP_MIDDLELEFT';
    }
    elseif ($resize_mode == 6)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_CROP_CENTER';
    }
    elseif ($resize_mode == 7)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_CROP_MIDDLERIGHT';
    }
    elseif ($resize_mode == 8)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_CROP_BOTTOMLEFT';
    }
    elseif ($resize_mode == 9)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_CROP_BOTTOMCENTER';
    }
    elseif ($resize_mode == 10)
    {
        $resize_mode_name = 'ZEBRA_IMAGE_CROP_BOTTOMRIGHT';
    }
}

// get global project id
$project_info = Projects::get_domain_project_info();
$project_id = $project_info['project_id'];

$params = array();
$params['id'] = $id;
$method_name = $provider . '_get_file_url';

$file = new File;
$redirect_url = call_user_func_array(array($file, $method_name), $params);
//echo $redirect_url;
//header( 'Location: ' . $redirect_url ) ;

/*
function file_get_contents_curl($url) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

echo file_get_contents_curl($redirect_url);
*/
/*
TRANSFERRED TO BASIC FUNCTIONS
function file_get_contents_curl($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
*/

if (!isset($resize_x) || !isset($resize_y))
{
    if ((double)phpversion() < 5.6)
    {
        $contents = file_get_contents_curl($redirect_url);
    }
    else
    {
        $file_class = new File;
        $contents = call_user_func_array(array($file_class, $provider . '_get_file_contents'), array($id));
    }

    if (!file_exists($tmp_folder))
    {
        File::create_dir($tmp_folder);
    }
    file_put_contents($tmp_file, $contents);
    echo $contents;
}
else
{
    $tmp_folder = $_SERVER['DOCUMENT_ROOT'] . '/uploads/image/tmp/';

    if (!file_exists($tmp_folder))
    {
        File::create_dir($tmp_folder);
    }

    $tmp_file = $tmp_folder . $id . '.' . sanitize($_GET['ext']);

    if ((double)phpversion() < 5.6)
    {
        $file_contents = file_get_contents_curl($redirect_url);
    }
    else
    {
        $file_class = new File;
        $contents = call_user_func_array(array($file_class, $provider . '_get_file_contents'), array($id));
    }

    //file_put_contents($tmp_file, $file_contents);
    file_put_contents($tmp_file, $contents);

    $image = new Zebra_Image();
    $image->source_path = $tmp_file;

    $resized_folder = $tmp_folder . $resize_x . 'x' . $resize_y . '_' . $resize_mode_name . '/';

    if (!file_exists($tmp_folder))
    {
        File::create_dir($resized_folder);
    }

    $resized_file = $resized_folder  . $id . sanitize($_GET['ext']);

    $image->target_path = $resized_file;
    if (!$image->resize($resize_x, $resize_y, $resize_mode)) show_error($image->error, $image->source_path, $image->target_path);
    //readfile($resized_file);
    print file_get_contents($resized_file);
}


?>
